package com.epam.greencyclegoods.utility;

import com.epam.greencyclegoods.dto.company.CompanyOverview;
import com.epam.greencyclegoods.dto.company.CreateCompanyDto;
import com.epam.greencyclegoods.dto.company.EditCompanyDto;
import com.epam.greencyclegoods.dto.companyCategory.CompanyCategoryOverview;
import com.epam.greencyclegoods.dto.companyCategory.CreateCompanyCategoryDto;
import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.dto.event.CreateEventDto;
import com.epam.greencyclegoods.dto.event.EditEventDto;
import com.epam.greencyclegoods.dto.event.EventOverview;
import com.epam.greencyclegoods.dto.order.OrderOverview;
import com.epam.greencyclegoods.dto.payment.EditPaymentDto;
import com.epam.greencyclegoods.dto.payment.PaymentOverview;
import com.epam.greencyclegoods.dto.product.CreateProductDto;
import com.epam.greencyclegoods.dto.product.EditProductDto;
import com.epam.greencyclegoods.dto.product.ProductOverview;
import com.epam.greencyclegoods.dto.productCategory.CreateProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.EditProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.ProductCategoryOverview;
import com.epam.greencyclegoods.dto.registration.CreateRegistrationDto;
import com.epam.greencyclegoods.dto.registration.EditRegistrationDto;
import com.epam.greencyclegoods.dto.registration.RegistrationOverview;
import com.epam.greencyclegoods.dto.user.ChangePasswordDto;
import com.epam.greencyclegoods.dto.user.CreateUserDto;
import com.epam.greencyclegoods.dto.user.EditUserDto;
import com.epam.greencyclegoods.dto.user.UserOverview;
import com.epam.greencyclegoods.entity.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 10.11.2023
 */

public class MockDataTestUtil {

    public static User getUser() {
        return User.builder()
                .id(1)
                .firstName("FirstName")
                .lastName("LastName")
                .email("email.@gmail.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.CUSTOMER)
                .enabled(true)
                .build();
    }

    public static User getUserForTokenWithDisabled() {
        return User.builder()
                .id(1)
                .firstName("FirstName")
                .lastName("LastName")
                .email("email.@gmail.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.CUSTOMER)
                .enabled(false)
                .build();
    }

    public static User getUserForTokenWithEnabled() {
        return User.builder()
                .id(1)
                .firstName("FirstName")
                .lastName("LastName")
                .email("email.@gmail.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.CUSTOMER)
                .enabled(true)
                .build();
    }

    public static User getOwnerUser() {
        return User.builder()
                .id(1)
                .firstName("FirstName")
                .lastName("LastName")
                .email("email.@gmail.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.OWNER)
                .enabled(true)
                .build();
    }

    public static User getAdminUser() {
        return User.builder()
                .id(1)
                .firstName("FirstName")
                .lastName("LastName")
                .email("email.@gmail.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
    }

    public static ChangePasswordDto getChangePasswordDto() {
        return ChangePasswordDto.builder()
                .oldPassword("Password1!")
                .newPassword1("Password1@")
                .newPassword2("Password1@")
                .build();
    }

    public static ChangePasswordDto getNonMatchingChangePasswordDto() {
        return ChangePasswordDto.builder()
                .oldPassword("Password1!")
                .newPassword1("Password1@")
                .newPassword2("Password2@")
                .build();
    }

    public static CreateUserDto getCreateUserDto() {
        return CreateUserDto.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email("email.@gmail.com")
                .password("Password1!")
                .enabled(true)
                .build();
    }

    public static UserOverview getUserOverview() {
        return UserOverview.builder()
                .id(1)
                .firstName("FirstName")
                .lastName("LastName")
                .email("email.@gmail.com")
                .role(Role.CUSTOMER)
                .build();
    }

    public static EditUserDto getEditUserDto() {
        return EditUserDto.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .build();
    }

    public static VerificationToken getVerificationToken() {
        return VerificationToken.builder()
                .id(1)
                .user(getUser())
                .plainToken("sampleToken")
                .build();
    }

    public static VerificationToken getVerificationTokenWithExpiry() {
        return VerificationToken.builder()
                .id(1)
                .user(getUserForTokenWithDisabled())
                .expiresAt(LocalDateTime.now().plusDays(1))
                .plainToken("sampleToken")
                .build();
    }

    public static VerificationToken getTokenWithoutUser() {
        return VerificationToken.builder()
                .id(1)
                .expiresAt(LocalDateTime.now().plusDays(1))
                .plainToken("sampleToken")
                .build();
    }

    public static Company getCompany() {
        return Company.builder()
                .id(1)
                .user(getUser())
                .name("Company")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("companyEmail@gmail.com")
                .companyCategory(getCompanyCategory())
                .build();
    }

    public static CompanyOverview getCompanyOverview() {
        return CompanyOverview.builder()
                .id(1)
                .userOverview(getUserOverview())
                .name("Company")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .companyCategoryOverview(getCompanyCategoryOverview())
                .email("companyEmail@gmail.com")
                .build();
    }

    public static CreateCompanyDto getCreateCompanyDto() {
        return CreateCompanyDto.builder()
                .name("Company")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("companyEmail@gmail.com")
                .companyCategoryId(getCompanyCategory().getId())
                .build();
    }

    public static EditCompanyDto getEditCompanyDto() {
        return EditCompanyDto.builder()
                .name("Company")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("companyEmail@gmail.com")
                .companyCategoryId(getCompanyCategory().getId())
                .build();
    }

    public static CompanyCategory getCompanyCategory() {
        return CompanyCategory.builder()
                .id(1)
                .name("InteriorFurnishings")
                .build();
    }

    public static CreateCompanyCategoryDto getCreateCompanyCategoryDto() {
        return CreateCompanyCategoryDto.builder()
                .name("InteriorFurnishings")
                .build();
    }

    public static CompanyCategoryOverview getCompanyCategoryOverview() {
        return CompanyCategoryOverview.builder()
                .id(1)
                .name("InteriorFurnishings")
                .build();
    }

    public static Event getEvent() {
        return Event.builder()
                .id(1)
                .name("Hrazdan River Cleaning")
                .company(getCompany())
                .price(0)
                .description("Join Us in Restoring the Beauty of Hrazdan River.")
                .build();
    }

    public static CreateEventDto getCreateEventDto() {
        return CreateEventDto.builder()
                .name("Hrazdan River Cleaning")
                .companyId(getCompany().getId())
                .price((double) 0)
                .description("Join Us in Restoring the Beauty of Hrazdan River.")
                .build();
    }

    public static EventOverview getEventOverview() {
        return EventOverview.builder()
                .id(1)
                .name("Hrazdan River Cleaning")
                .companyOverview(getCompanyOverview())
                .price(0)
                .description("Join Us in Restoring the Beauty of Hrazdan River.")
                .build();
    }

    public static EditEventDto getEditEventDto() {
        return EditEventDto.builder()
                .name("Hrazdan River Cleaning")
                .companyId(getCompany().getId())
                .price((double) 0)
                .description("Join Us in Restoring the Beauty of Hrazdan River.")
                .build();
    }

    public static Registration getRegistration() {
        return Registration.builder()
                .id(1)
                .user(getUser())
                .peopleCount(2)
                .event(getEvent())
                .status(RegistrationStatus.PENDING)
                .build();
    }

    public static EditRegistrationDto getEditRegistrationDto() {
        return EditRegistrationDto.builder()
                .status("PENDING")
                .peopleCount(2)
                .build();
    }

    public static CreateRegistrationDto getCreateRegistrationDto() {
        return CreateRegistrationDto.builder()
                .peopleCount(2)
                .eventId(getEvent().getId())
                .build();
    }

    public static RegistrationOverview getRegistrationOverview() {
        return RegistrationOverview.builder()
                .id(1)
                .userOverview(getUserOverview())
                .peopleCount(2)
                .userOverview(getUserOverview())
                .status("PENDING")
                .build();
    }

    public static ProductCategory getProductCategory() {
        return ProductCategory.builder()
                .id(1)
                .name("Furniture")
                .build();
    }

    public static ProductCategoryOverview getProductCategoryOverview() {
        return ProductCategoryOverview.builder()
                .id(1)
                .name("Furniture")
                .build();
    }

    public static CreateProductCategoryDto getCreateProductCategoryDto() {
        return CreateProductCategoryDto.builder()
                .name("Furniture")
                .build();
    }

    public static EditProductCategoryDto getEditProductCategoryDto() {
        return EditProductCategoryDto.builder()
                .name("Dishes")
                .build();
    }

    public static Product getProduct() {
        return Product.builder()
                .id(1)
                .name("Table")
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home.")
                .productCategory(getProductCategory())
                .company(getCompany())
                .productCount(3)
                .build();
    }

    public static Product getProductForOwner() {
        return Product.builder()
                .id(1)
                .name("Table")
                .user(getOwnerUser())
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home. ")
                .productCategory(getProductCategory())
                .company(getCompany())
                .build();
    }

    public static Product getProductForAdmin() {
        return Product.builder()
                .id(1)
                .name("Table")
                .user(getAdminUser())
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home. ")
                .productCategory(getProductCategory())
                .company(getCompany())
                .build();
    }

    public static Product getProductForBasket() {
        return Product.builder()
                .id(2)
                .name("Table")
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home.")
                .productCategory(getProductCategory())
                .company(getCompany())
                .build();
    }

    public static EditProductDto getEditProductDto() {
        return EditProductDto.builder()
                .name("Table")
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home.")
                .productCategoryId(getProductCategory().getId())
                .companyId(getCompany().getId())
                .build();
    }

    public static ProductOverview getProductOverview() {
        return ProductOverview.builder()
                .id(1)
                .name("Table")
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home.")
                .productCategoryOverview(getProductCategoryOverview())
                .companyOverview(getCompanyOverview())
                .build();
    }

    public static CreateProductDto getCreateProductDto() {
        return CreateProductDto.builder()
                .name("Table")
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home.")
                .productCategoryId(getProductCategory().getId())
                .companyId(getCompany().getId())
                .build();
    }

    public static Basket getBasket() {
        return Basket.builder()
                .id(1)
                .user(getUser())
                .product(getProduct())
                .itemQuantity(1)
                .build();
    }

    public static Basket getBasketWithQuantity() {
        return Basket.builder()
                .id(1)
                .user(getUser())
                .product(getProductForBasket())
                .itemQuantity(10)
                .build();
    }

    public static CreditCard getCreditCard() {
        return CreditCard.builder()
                .id(1)
                .user(getUser())
                .cardHolder("FirstName LastName")
                .cardNumber("4111111111111111")
                .cardExpiresAt(LocalDate.now().plusDays(50))
                .cvv("111")
                .build();
    }

    public static CreateCreditCardDto getCreateCreditCardDto() {
        return CreateCreditCardDto.builder()
                .cardHolder("FirstName LastName")
                .cardNumber("4111111111111111")
                .cardExpiresAt(LocalDate.now().plusDays(50))
                .cvv("111")
                .build();
    }

    public static Payment getPayment() {
        return Payment.builder()
                .id(0)
                .paymentCreateDate(LocalDateTime.now())
                .user(getUser())
                .status(PaymentStatus.PROCESSING)
                .order(getOrderForPayment())
                .totalAmount(getOrder().getTotalPrice())
                .build();
    }

    public static PaymentOverview getPaymentOverview() {
        return PaymentOverview.builder()
                .id(1)
                .userOverview(getUserOverview())
                .orderOverview(getOrderOverview())
                .build();
    }

    public static EditPaymentDto getEditPaymentDto() {
        return EditPaymentDto.builder()
                .paymentDate("2023-11-10T12:00:00")
                .status("PROCESSING")
                .build();
    }

    public static Order getOrder() {
        return Order.builder()
                .id(1)
                .additionalAddress("additionalAddress")
                .status(OrderStatus.NEW)
                .paymentCompleted(true)
                .additionalPhone("+37499112233")
//                .paymentOption(PaymentOption.CASH)
                .products(List.of(getProduct(), getProductForBasket()))
                .totalPrice(200)
                .user(getUser())
                .build();
    }

    public static Order getOwnerOrder() {
        return Order.builder()
                .id(1)
                .additionalAddress("additionalAddress")
                .status(OrderStatus.NEW)
                .paymentCompleted(true)
                .additionalPhone("+37499112233")
//                .paymentOption(PaymentOption.CASH)
                .products(List.of(getProduct(), getProductForBasket()))
                .totalPrice(200)
                .user(getOwnerUser())
                .build();
    }

    public static Order getOrderForPayment() {
        return Order.builder()
                .id(1)
                .additionalAddress("additionalAddress")
                .status(OrderStatus.NEW)
                .paymentCompleted(true)
                .additionalPhone("+37499112233")
//                .paymentOption(PaymentOption.CREDIT_CARD)
                .products(List.of(getProduct(), getProduct()))
                .totalPrice(200.0)
                .user(getUser())
                .build();
    }

    public static OrderOverview getOrderOverview() {
        return OrderOverview.builder()
                .id(1)
                .additionalAddress("additionalAddress")
                .status("NEW")
                .paymentCompleted(true)
                .additionalPhone("+37499112233")
                .productOverviews(List.of(getProductOverview(), getProductOverview()))
                .totalPrice(200.0)
                .userOverview(getUserOverview())
                .build();
    }
}