package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Arpi Khachatryan
 * Date: 09.12.2023
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RegistrationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private CompanyCategoryRepository companyCategoryRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EventRepository eventRepository;

    private Registration registration;

    @BeforeEach
    void setUp() {
        User user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        CompanyCategory companyCategory = CompanyCategory.builder()
                .name("defaultCategory")
                .build();
        companyCategoryRepository.save(companyCategory);

        Company company = Company.builder()
                .user(user)
                .name("defaultCompany")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("defaultCompanyEmail@test.com")
                .companyCategory(companyCategory)
                .build();
        companyRepository.save(company);

        Event event = Event.builder()
                .name("defaultEvent")
                .company(company)
                .price(0)
                .description("Join Us in Restoring the Beauty of Hrazdan River.")
                .build();
        eventRepository.save(event);

        registration = Registration.builder()
                .user(user)
                .peopleCount(2)
                .event(event)
                .status(RegistrationStatus.PENDING)
                .build();
        registrationRepository.save(registration);
    }

    @AfterEach
    void tearDown() {
        registrationRepository.deleteAll();
        eventRepository.deleteAll();
        companyRepository.deleteAll();
        companyCategoryRepository.deleteAll();
    }

    @Test
    void registrations() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/registrations"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("registration/registrations"));
    }

    @Test
    void userRegistrations() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/registrations/user-registrations"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("registration/registrations"));
    }

    @Test
    void getEditRegistrationPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/registrations/edit/{id}", registration.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("registration/editRegistration"));
    }

    @Test
    void getEditRegistrationPageWithErrors() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/registrations/edit/{id}", registration.getId() + 1)
                        .param("errorMessageEdit", "No registrations found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/registrations"));
    }

    @Test
    void editRegistrationShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/registrations/edit/{id}", registration.getId())
                        .param("peopleCount", "3")
                        .param("status", String.valueOf(RegistrationStatus.PENDING)))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/registrations/edit/" + registration.getId()));
    }

    @Test
    void editRegistrationWithValidationErrors() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/registrations/edit/{id}", registration.getId())
                        .param("peopleCount", "0")
                        .param("status", String.valueOf(RegistrationStatus.PENDING)))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/registrations/edit/" + registration.getId()))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("registrationErrors"));
    }

    @Test
    void editRegistrationWithRegistrationNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/registrations/edit/{id}", -1)
                        .param("peopleCount", "3")
                        .param("status", String.valueOf(RegistrationStatus.PENDING)))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/registrations/edit/" + -1))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("errorMessageEdit"));
    }

    @Test
    void deleteRegistrationShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/registrations/delete/{id}", registration.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/registrations"));
    }

    @Test
    void deleteRegistrationShouldFailWhenRegistrationNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/registrations/delete/{id}", -1))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/registrations"))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("errorMessageDelete"));
    }
}