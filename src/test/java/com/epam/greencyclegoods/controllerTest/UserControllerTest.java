package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.entity.VerificationToken;
import com.epam.greencyclegoods.repository.UserRepository;
import com.epam.greencyclegoods.repository.VerificationTokenRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Arpi Khachatryan
 * Date: 02.12.2023
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    private User user;

    @BeforeEach
    void setUp() {
        user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @AfterEach
    void tearDown() {
        verificationTokenRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void getUserProfileAsAdmin() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/home"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("admin/admin"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessagePassword"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessageEdit"));
    }

    @Test
    void getUserProfileAsOwner() throws Exception {
        user.setRole(Role.OWNER);
        userRepository.save(user);
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/home"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("user/customer"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessagePassword"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessageEdit"));
    }

    @Test
    void getUserProfileAsCustomer() throws Exception {
        user.setRole(Role.CUSTOMER);
        userRepository.save(user);
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/home"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("user/customer"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessagePassword"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessageEdit"));
    }

    @Test
    void getUserProfileShouldFail() throws Exception {
        userRepository.deleteAll();
        mockMvc.perform(MockMvcRequestBuilders.get("/users/home"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("main/error"));

    }

    @Test
    void userProfileAsAdmin() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/profile"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("admin/adminProfile"));
    }

    @Test
    void userProfileAsOwner() throws Exception {
        user.setRole(Role.OWNER);
        userRepository.save(user);
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/profile"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("user/userProfile"));
    }

    @Test
    void users() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("users"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("totalPages"))
                .andExpect(MockMvcResultMatchers.view().name("user/users"));
    }

    @Test
    void getUserRegistrationPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("user/addUser"));
    }

    @Test
    void registerUserWithValidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/users/add")
                        .param("firstName", "NewFirstName")
                        .param("lastName", "NewLastName")
                        .param("email", "firstName.lastName@test.com")
                        .param("password", "Password1!"))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/loginPage"));
    }

    @Test
    void registerUserWithInvalidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/users/add")
                        .param("firstName", "")
                        .param("lastName", "")
                        .param("email", "")
                        .param("password", ""))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("user/addUser"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("userErrors"));
    }

    @Test
    void verifyUserByTokenShouldSucceed() throws Exception {
        user.setEnabled(false);
        userRepository.save(user);

        VerificationToken verificationToken = VerificationToken.builder()
                .plainToken("token")
                .expiresAt(LocalDateTime.now().plusDays(2))
                .user(user)
                .build();
        verificationTokenRepository.save(verificationToken);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/verify")
                        .param("token", "token"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("main/loginPage"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("verificationError"));
    }

    @Test
    void verifyUserByTokenShouldFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/verify")
                        .param("token", "token")
                        .param("user", String.valueOf(user)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("user/addUser"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("user"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("verificationMessage"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("verificationError"));
    }

    @Test
    void getEditPage() throws Exception {
        userRepository.save(user);
        mockMvc.perform(MockMvcRequestBuilders.get("/users/edit/{id}", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("user/customerEdit"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("user"));
    }

    @Test
    void getEditPageWithUserNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/edit/{id}", -1))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/users/home"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("user"))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("errorMessageEdit"));
    }

    @Test
    void editUserWithValidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/users/edit/{id}", user.getId())
                        .param("firstName", "EditFirstName")
                        .param("lastName", "EditLastName")
                        .param("email", "new.email@test.com"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/users/home"));
    }

    @Test
    void editUserWithUserNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/users/edit/{id}", -1)
                        .param("firstName", "EditFirstName")
                        .param("lastName", "EditLastName")
                        .param("email", "new.email@test.com"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/users/home"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("userErrors"))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("errorMessageEdit"));
    }

    @Test
    void editUserWithInvalidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/users/edit/{id}", user.getId())
                        .param("firstName", "")
                        .param("lastName", "")
                        .param("email", "invalid-email"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/users/edit/" + user.getId()))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("userErrors"));
    }

    @Test
    void changePasswordWithValidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/users/changePassword/{id}", user.getId())
                        .param("oldPassword", "Password1!")
                        .param("newPassword1", "NewPassword1!")
                        .param("newPassword2", "NewPassword1!"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/users/home"));
    }

    @Test
    void changePasswordWithInvalidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/users/changePassword/{id}", user.getId())
                        .param("oldPassword", "InvalidPassword")
                        .param("newPassword1", "ShortPassword")
                        .param("newPassword2", "MismatchedPassword"))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/users/edit/" + user.getId()))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("userErrors"));
    }

    @Test
    void deleteUserShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/delete/{id}", user.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/users"));
    }

    @Test
    void deleteUserShouldFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/delete/{id}", -1))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/users"))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("errorMessageDelete"));
    }
}