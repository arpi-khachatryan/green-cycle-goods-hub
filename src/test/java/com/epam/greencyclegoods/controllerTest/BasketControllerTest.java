package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Arpi Khachatryan
 * Date: 02.12.2023
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BasketControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BasketRepository basketRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CompanyCategoryRepository companyCategoryRepository;

    @Autowired
    private CompanyRepository companyRepository;

    private Company company;

    private ProductCategory productCategory;

    private Product product;

    private Basket basket;

    private User user;

    @BeforeEach
    void setUp() {
        user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        CompanyCategory companyCategory = CompanyCategory.builder()
                .name("defaultCategory")
                .build();
        companyCategoryRepository.save(companyCategory);

        company = Company.builder()
                .user(user)
                .name("defaultCompany")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("defaultCompanyEmail@test.com")
                .companyCategory(companyCategory)
                .build();
        companyRepository.save(company);

        productCategory = ProductCategory.builder()
                .name(UUID.randomUUID().toString())
                .build();
        productCategoryRepository.save(productCategory);

        product = Product.builder()
                .name("defaultProduct")
                .user(user)
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home. ")
                .productCategory(productCategory)
                .company(company)
                .productCount(10)
                .build();
        productRepository.save(product);

        basket = Basket.builder()
                .id(1)
                .user(user)
                .product(product)
                .itemQuantity(1)
                .build();
        basketRepository.save(basket);
    }

    @AfterEach
    void tearDown() {
        basketRepository.deleteAll();
        productRepository.deleteAll();
        companyRepository.deleteAll();
        companyCategoryRepository.deleteAll();
        productCategoryRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void baskets() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/baskets"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("basket/basket"));
    }

    @Test
    void allBasketsAsAdmin() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/baskets/all-baskets"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("admin/basket"));
    }

    @Test
    void allBasketsAsOwner() throws Exception {
        user.setRole(Role.OWNER);
        userRepository.save(user);
        mockMvc.perform(MockMvcRequestBuilders.get("/baskets/all-baskets"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("admin/basket"));
    }

    @Test
    void addToBasketShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/baskets/add/{id}", product.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/products"));
    }

    @Test
    void addToBasketFailsWhenProductCountIsNotEnough() throws Exception {
        Product product = Product.builder()
                .name("newProduct")
                .user(user)
                .price(150.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Chair – a sustainable and stylish addition to your home.")
                .productCategory(productCategory)
                .company(company)
                .productCount(0)
                .build();
        productRepository.save(product);

        mockMvc.perform(MockMvcRequestBuilders.get("/baskets/add/{id}", product.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/products"));
    }

    @Test
    void deleteBasketShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/baskets/delete/{id}", basket.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void deleteBasketShouldFailWhenProductNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/baskets/delete/{id}", 999))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("basket/basket"));
    }

    @Test
    void productsInStock() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/baskets/products-in-stock"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}