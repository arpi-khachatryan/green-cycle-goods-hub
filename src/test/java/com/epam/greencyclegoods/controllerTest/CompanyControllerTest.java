package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

/**
 * @author Arpi Khachatryan
 * Date: 09.12.2023
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyCategoryRepository companyCategoryRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private EventRepository eventRepository;

    private Company company;

    private CompanyCategory companyCategory;

    @BeforeEach
    void setUp() {
        User user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        companyCategory = CompanyCategory.builder()
                .name("defaultCategory")
                .build();
        companyCategoryRepository.save(companyCategory);

        company = Company.builder()
                .user(user)
                .name("defaultCompany")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("defaultCompanyEmail@test.com")
                .companyCategory(companyCategory)
                .build();
        companyRepository.save(company);

        ProductCategory productCategory = ProductCategory.builder()
                .name(UUID.randomUUID().toString())
                .build();
        productCategoryRepository.save(productCategory);

        Product product = Product.builder()
                .name("defaultProduct")
                .user(user)
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home. ")
                .productCategory(productCategory)
                .company(company)
                .build();
        productRepository.save(product);
    }

    @AfterEach
    void tearDown() {
        eventRepository.deleteAll();
        productRepository.deleteAll();
        companyRepository.deleteAll();
        companyCategoryRepository.deleteAll();
        productCategoryRepository.deleteAll();

    }

    @Test
    void companies() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("company/companies"));
    }

    @Test
    void userCompanies() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/user-companies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("company/companies"));
    }

    @Test
    void userCompaniesRedirectOnError() throws Exception {
        productRepository.deleteAll();
        companyRepository.deleteAll();
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/user-companies"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies"));
    }

    @Test
    void company() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{id}", company.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("company/company"));
    }

    @Test
    void registrationTerms() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/registration-terms"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("categories"))
                .andExpect(MockMvcResultMatchers.view().name("company/companyTerms"));
    }

    @Test
    void companyRedirectOnError() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{id}", -1))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies"));
    }

    @Test
    void getAddCompanyPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("company/addCompany"))
                .andExpect(model().attributeExists("categories"));
    }

    @Test
    void addCompanyShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/companies/add")
                        .file(new MockMultipartFile("companyImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("name", "newCompany")
                        .param("email", "newCompanyEmail@test.com")
                        .param("phone", "+37499887766")
                        .param("companyCategoryId", String.valueOf(companyCategory.getId()))
                        .param("deliveryPrice", "200.0"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies"));
    }

    @Test
    void addCompanyImageUploadFailure() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/companies/add")
                        .file(new MockMultipartFile("companyImage", "test.txt", "text/plain", "Company files".getBytes()))
                        .param("name", "newCompany")
                        .param("email", "newCompanyEmail@test.com")
                        .param("phone", "+37499887766")
                        .param("companyCategoryId", String.valueOf(companyCategory.getId()))
                        .param("deliveryPrice", "200.0"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies/add"));
    }

    @Test
    void addCompanyValidationError() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/companies/add")
                        .file(new MockMultipartFile("companyImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .param("name", "newCompany")
                        .param("email", "")
                        .param("phone", "+3749988776")
                        .param("companyCategoryId", "")
                        .param("deliveryPrice", ""))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies/add"));
    }

    @Test
    void getEditCompanyPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/edit/{id}", company.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("company/editCompanies"));
    }

    @Test
    void getEditCompanyPageWithException() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/edit/{id}", -1)
                        .param("errorMessageEdit", "Company not found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies/-1"));
    }

    @Test
    void editCompanyShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/companies/edit/{id}", company.getId())
                        .file(new MockMultipartFile("companyImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("name", "newCompanyName")
                        .param("email", "new.CompanyEmail@test.com")
                        .param("phone", "+37499887766")
                        .param("companyCategoryId", String.valueOf(companyCategory.getId()))
                        .param("deliveryPrice", "200.0"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies/edit/" + company.getId()));
    }

    @Test
    void editCompanyShouldFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/companies/edit/{id}", 1)
                        .file(new MockMultipartFile("companyImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("name", "newCompanyName")
                        .param("email", "new.CompanyEmail@test.com")
                        .param("phone", "+37499887766")
                        .param("companyCategoryId", "2")
                        .param("deliveryPrice", "200.0"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies/edit/1?errorMessageEdit=Failed+to+update+the+company+information."));
    }

    @Test
    void deleteCompanyShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/delete/{id}", company.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies/" + company.getId()));
    }

    @Test
    void deleteCompanyShouldFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/delete/{id}", -1)
                        .param("errorMessageDelete", "Company not found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companies/-1"));
    }

    @Test
    void companyEvents() throws Exception {
        Event event = Event.builder().company(company).build();
        eventRepository.save(event);

        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{id}/events", company.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("event/eventCatalog"));

        event.setCompany(null);
        eventRepository.deleteAll();
    }

    @Test
    void companyEventsRedirectOnError() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{id}/events", -1))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/events"));
    }
}