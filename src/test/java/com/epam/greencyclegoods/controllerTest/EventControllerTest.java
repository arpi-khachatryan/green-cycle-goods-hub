package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

/**
 * @author Arpi Khachatryan
 * Date: 10.12.2023
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EventControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private CompanyCategoryRepository companyCategoryRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    private Company company;

    private Event event;

    @BeforeEach
    void setUp() {
        User user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        CompanyCategory companyCategory = CompanyCategory.builder()
                .name("defaultCategory")
                .build();
        companyCategoryRepository.save(companyCategory);

        company = Company.builder()
                .user(user)
                .name("defaultCompany")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("defaultCompanyEmail@test.com")
                .companyCategory(companyCategory)
                .build();
        companyRepository.save(company);

        event = Event.builder()
                .name("defaultEvent")
                .company(company)
                .price(0)
                .description("Join Us in Restoring the Beauty of Hrazdan River.")
                .build();
        eventRepository.save(event);
    }

    @AfterEach
    void tearDown() {
        eventRepository.deleteAll();
        companyRepository.deleteAll();
        companyCategoryRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void events() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/events"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("events"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("companies"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("eventsByCompany"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("totalPages"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("event/events"));
    }

    @Test
    void userEvents() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/events/events"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("events"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("totalPages"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("admin/events"));
    }

    @Test
    void event() throws Exception {
        eventRepository.save(event);
        mockMvc.perform(MockMvcRequestBuilders.get("/events/{id}", event.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("event/event"));
    }

    @Test
    void getEventShouldFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/events/{id}", -1)
                        .param("errorMessageEvent", "Event not found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/events"));
    }

    @Test
    void getAddEventPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/events/add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("event/addEvent"))
                .andExpect(model().attributeExists("companies"));
    }

    @Test
    void addEventShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/events/add")
                        .file(new MockMultipartFile("eventImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("name", "newEvent")
                        .param("description", "Join Us in Restoring the Beauty of Azat River.")
                        .param("price", "0")
                        .param("eventDateTime", "2025-12-10T12:31")
                        .param("companyId", String.valueOf(company.getId())))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/events/events"));
    }

    @Test
    void addEventValidationError() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/events/add")
                        .file(new MockMultipartFile("eventImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("name", "newEvent")
                        .param("description", "")
                        .param("price", "0")
                        .param("eventDateTime", "2025-12-10T12:31")
                        .param("companyId", String.valueOf(company.getId())))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("event/addEvent"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("eventErrors"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("companies"));
    }

    @Test
    void getEditEventPage() throws Exception {
        eventRepository.save(event);
        mockMvc.perform(MockMvcRequestBuilders.get("/events/edit/{id}", event.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("event/editEvent"));
    }

    @Test
    void getEditEventPageWithEventNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/events/edit/{id}", -1)
                        .param("errorMessageEdit", "Event not found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/events/events"));
    }

    @Test
    void editEventShouldSucceed() throws Exception {
        eventRepository.save(event);
        mockMvc.perform(MockMvcRequestBuilders.multipart("/events/edit/{id}", event.getId())
                        .file(new MockMultipartFile("eventImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("name", "newEvent")
                        .param("description", "")
                        .param("price", "0")
                        .param("eventDateTime", "2025-12-10T12:31")
                        .param("companyId", String.valueOf(company.getId())))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/events/events"));
    }


    @Test
    void editEventShouldFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/events/edit/{id}", event.getId())
                        .file(new MockMultipartFile("eventImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("name", "newEvent")
                        .param("price", "")
                        .param("eventDateTime", "2025-12-10T12:31")
                        .param("companyId", String.valueOf(company.getId())))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/events/edit/" + event.getId()));
    }

    @Test
    void editEventShouldFailWithEventNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/events/edit/{id}", -1)
                        .file(new MockMultipartFile("eventImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("name", "newEvent")
                        .param("price", "0")
                        .param("eventDateTime", "2025-12-10T12:31")
                        .param("companyId", String.valueOf(company.getId())))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/events/edit/" + (-1)))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("errorMessageEdit"));
    }

    @Test
    void deleteEventShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/events/delete/{id}", event.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/events/events"));
    }

    @Test
    void deleteEventShouldFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/events/delete/{id}", -1)
                        .param("errorMessageDelete", "Event not found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/events/events"));
    }

    @Test
    void getEventRegistrationPage() throws Exception {
        Event eventForRegistration = Event.builder()
                .name("exampleEvent")
                .company(company)
                .price(0)
                .description("Join Us in Restoring the Beauty of Hrazdan River.")
                .build();
        eventRepository.save(eventForRegistration);
        Registration registration = Registration.builder()
                .peopleCount(2)
                .event(eventForRegistration)
                .status(RegistrationStatus.PENDING)
                .build();
        registrationRepository.save(registration);

        mockMvc.perform(MockMvcRequestBuilders.get("/events/{id}/registration", eventForRegistration.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("registration/addRegistration"));

        registrationRepository.deleteAll();
        eventRepository.deleteAll();
    }

    @Test
    void getEventRegistrationPageShouldFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/events/{id}/registration", -1))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/events/" + (-1)))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("errorMessageEvent"));
    }

    @Test
    void addRegistrationShouldFail() throws Exception {
        eventRepository.deleteAll();
        mockMvc.perform(MockMvcRequestBuilders.post("/events/{id}/registration", -1)
                        .param("peopleCount", "3"))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/events/" + (-1)));
    }
}