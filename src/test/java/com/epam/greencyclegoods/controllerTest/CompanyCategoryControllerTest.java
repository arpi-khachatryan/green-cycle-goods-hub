package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.CompanyCategory;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.repository.CompanyCategoryRepository;
import com.epam.greencyclegoods.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Arpi Khachatryan
 * Date: 02.12.2023
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CompanyCategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CompanyCategoryRepository companyCategoryRepository;

    private CompanyCategory companyCategory;

    @BeforeEach
    void setUp() {
        User user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        companyCategory = CompanyCategory.builder()
                .id(1)
                .name("defaultCategory")
                .build();
        companyCategoryRepository.save(companyCategory);
    }

    @AfterEach
    void tearDown() {
        companyCategoryRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void companyCategories() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companyCategories"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("companyCategory/companyCategory"));
    }

    @Test
    void getAddCategoryPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companyCategories/add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("companyCategory/addCompanyCategory"));
    }

    @Test
    void addCategoryWithValidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/companyCategories/add")
                        .param("id", String.valueOf(companyCategory.getId()))
                        .param("name", "newCategory"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companyCategories"));
    }

    @Test
    void addCategoryWithInvalidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/companyCategories/add")
                        .param("name", ""))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("companyCategory/addCompanyCategory"));
    }

    @Test
    void deleteCategoryShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companyCategories/delete/{id}", companyCategory.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companyCategories"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessageDeleteCategory"));
    }

    @Test
    void deleteCategoryShouldFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/companyCategories/delete/{id}", 999))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/companyCategories"))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("errorMessageDeleteCategory"));
    }
}