package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * @author Arpi Khachatryan
 * Date: 09.12.2023
 */

@ActiveProfiles("test")
@Transactional
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyCategoryRepository companyCategoryRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BasketRepository basketRepository;

    private User user;

    private Order order;

    private Product product;

    @BeforeEach
    void setUp() {
        user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        CompanyCategory companyCategory = CompanyCategory.builder()
                .name("defaultCategory")
                .build();
        companyCategoryRepository.save(companyCategory);

        Company company = Company.builder()
                .user(user)
                .name("defaultCompany")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("defaultCompanyEmail@test.com")
                .companyCategory(companyCategory)
                .build();
        companyRepository.save(company);

        ProductCategory productCategory = ProductCategory.builder()
                .name("defaultCategory")
                .build();
        productCategoryRepository.save(productCategory);

        product = Product.builder()
                .name("defaultProduct")
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home.")
                .productCategory(productCategory)
                .company(company)
                .productCount(3)
                .build();
        productRepository.save(product);

        order = Order.builder()
                .id(3)
                .additionalAddress("additionalAddress")
                .status(OrderStatus.NEW)
                .paymentCompleted(true)
                .additionalPhone("+37499112233")
                .products(List.of(product))
                .totalPrice(200)
                .user(user)
                .build();
        orderRepository.save(order);
    }

    @AfterEach
    void tearDown() {
        basketRepository.deleteAll();
        orderRepository.deleteAll();
        productRepository.deleteAll();
        companyRepository.deleteAll();
        productCategoryRepository.deleteAll();
        companyCategoryRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void ordersAsAdmin() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/orders"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("order/orders"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("orders"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("totalPages"));
    }

    @Test
    void ordersAsOwner() throws Exception {
        user.setRole(Role.OWNER);
        userRepository.save(user);
        mockMvc.perform(MockMvcRequestBuilders.get("/orders"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("order/orders"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("orders"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("totalPages"));
    }

    @Test
    void ordersAsUser() throws Exception {
        user.setRole(Role.CUSTOMER);
        userRepository.save(user);
        mockMvc.perform(MockMvcRequestBuilders.get("/orders"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("order/orders"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("orders"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("totalPages"));
    }

    @Test
    void userOrders() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/orders/user-orders"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("order/orders"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("orders"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("totalPages"));
    }

    @Test
    void order() throws Exception {
        Order orderInstance = Order.builder()
                .additionalAddress("newAdditionalAddress")
                .status(OrderStatus.NEW)
                .paymentCompleted(true)
                .additionalPhone("+37499112233")
                .products(List.of(product))
                .totalPrice(200)
                .user(user)
                .build();
        orderRepository.save(orderInstance);
        mockMvc.perform(MockMvcRequestBuilders.get("/orders/{id}", orderInstance.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("order/order"));
    }

    @Test
    void orderWithException() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/orders/{id}", -1)
                        .param("errorMessageOrder", "Order not found."))
                .andExpect(status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/orders"));
    }

    @Test
    void getAddOrderPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/orders/add"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/baskets"));
    }

    @Test
    void getAddOrderPageWithBasket() throws Exception {
        basketRepository.save(Basket.builder()
                .product(product)
                .user(user).build());

        mockMvc.perform(MockMvcRequestBuilders.get("/orders/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("order/addOrder"));

        basketRepository.deleteAll();
    }

    @Test
    void getAddOrderPageWithBasketNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/orders/add")
                        .param("errorMessageAddOrder", "Basket not found."))
                .andExpect(status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/baskets"));
    }

    @Test
    void addOrder() throws Exception {
        basketRepository.save(Basket.builder()
                .user(user)
                .product(product).build());

        mockMvc.perform(post("/orders/add")
                        .param("additionalAddress", "Additional Address")
                        .param("additionalPhone", "+37499303030")
                        .param("paymentOption", "CASH")
                        .param("cardNumber", "4111111111111111")
                        .param("cardHolder", "FirstName LastName")
                        .param("cardExpiresAt", String.valueOf(LocalDate.now().plusDays(50)))
                        .param("cvv", "111"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("order/confirmOrder"));
    }

    @Test
    void addOrderWithValidationErrors() throws Exception {
        mockMvc.perform(post("/orders/add")
                        .param("paymentOption", "CREDIT_CARD")
                        .param("additionalAddress", "")
                        .param("additionalPhone", " "))
                .andExpect(status().isOk())
                .andExpect(view().name("order/addOrder"));
    }

    @Test
    void getEditOrderPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/orders/edit/{id}", order.getId()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/orders"));
    }

    @Test
    void getEditOrderPageWithOrderNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/orders/edit/{id}", -1)
                        .param("errorMessageEditOrder", "Order not found."))
                .andExpect(status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/orders"));
    }

    @Test
    void editOrderShouldSucceed() throws Exception {
        mockMvc.perform(post("/orders/edit/{id}", order.getId())
                        .param("additionalAddress", "New Address")
                        .param("additionalPhone", "+37499887766")
                        .param("status", "IN_PROGRESS")
                        .param("paymentCompleted", "true"))
                .andExpect(status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/orders/edit/" + order.getId()));
    }

    @Test
    void editOrderWithValidationErrors() throws Exception {
        mockMvc.perform(post("/orders/edit/{id}", order.getId())
                        .param("additionalAddress", "")
                        .param("additionalPhone", "+374")
                        .param("status", " ")
                        .param("paymentCompleted", "true"))
                .andExpect(status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/orders/edit/" + order.getId()));
    }

    @Test
    void deleteOrderShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/orders/delete/{id}", order.getId()))
                .andExpect(status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/orders"));
    }

    @Test
    void deleteOrderWithOrderNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/orders/delete/{id}", -1)
                        .param("errorMessageDeleteOrder", "Order not found"))
                .andExpect(status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/orders"));
    }
}