package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Arpi Khachatryan
 * Date: 09.12.2023
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyCategoryRepository companyCategoryRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BasketRepository basketRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    private Payment payment;

    @BeforeEach
    void setUp() {
        User user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        CompanyCategory companyCategory = CompanyCategory.builder()
                .name("defaultCategory")
                .build();
        companyCategoryRepository.save(companyCategory);

        Company company = Company.builder()
                .user(user)
                .name("defaultCompany")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("defaultCompanyEmail@test.com")
                .companyCategory(companyCategory)
                .build();
        companyRepository.save(company);

        ProductCategory productCategory = ProductCategory.builder()
                .name("defaultCategory")
                .build();
        productCategoryRepository.save(productCategory);

        Product product = Product.builder()
                .name("defaultProduct")
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home.")
                .productCategory(productCategory)
                .company(company)
                .productCount(1)
                .build();
        productRepository.save(product);

        Basket basket = Basket.builder()
                .user(user)
                .product(product)
                .itemQuantity(1)
                .build();
        basketRepository.save(basket);

        Order order = Order.builder()
                .additionalAddress("defaultAdditionalAddress")
                .status(OrderStatus.NEW)
                .paymentCompleted(true)
                .additionalPhone("+37499112233")
                .products(List.of(product))
                .totalPrice(200.0)
                .user(user)
                .build();
        orderRepository.save(order);

        payment = Payment.builder()
                .paymentCreateDate(LocalDateTime.now())
                .user(user)
                .status(PaymentStatus.PROCESSING)
                .order(order)
                .totalAmount(order.getTotalPrice())
                .build();
        paymentRepository.save(payment);
    }

    @AfterEach
    void tearDown() {
        paymentRepository.deleteAll();
        orderRepository.deleteAll();
        basketRepository.deleteAll();
        productRepository.deleteAll();
        companyRepository.deleteAll();
        companyCategoryRepository.deleteAll();
        productCategoryRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void payments() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/payments"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("payment/payments"));
    }

    @Test
    void userPayments() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/payments/user-payments"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("payment/payments"));
    }

    @Test
    void payment() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/payments/{id}", payment.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("payment"))
                .andExpect(MockMvcResultMatchers.view().name("payment/payment"));
    }

    @Test
    void paymentWithPaymentNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/payments/{id}", -1)
                        .param("errorMessagePayment", "Payment not found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/payments"));
    }

    @Test
    void getEditPaymentPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/payments/edit/{id}", payment.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("payment/payment"));
    }

    @Test
    void getEditPaymentPageWithPaymentNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/payments/edit/{id}", -1)
                        .param("errorMessagePayment", "Payment not found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/payments"));
    }

    @Test
    void editPaymentShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/payments/edit/{id}", payment.getId())
                        .param("paymentDate", "2023-11-10T12:00:00")
                        .param("status", "PROCESSING"))
                .andExpect(status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/payments/" + payment.getId()));
    }

    @Test
    void editPaymentWithValidationErrors() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/payments/edit/{id}", payment.getId()))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/payments/" + payment.getId()));
    }

    @Test
    void deletePaymentShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/payments/delete/{id}", payment.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/payments"));
    }

    @Test
    void deletePaymentWithPaymentNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/payments/delete/{id}", -1))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/payments"));
    }
}