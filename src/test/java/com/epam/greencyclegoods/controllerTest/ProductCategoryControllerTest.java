package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.ProductCategory;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.repository.ProductCategoryRepository;
import com.epam.greencyclegoods.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Arpi Khachatryan
 * Date: 09.12.2023
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductCategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    private ProductCategory productCategory;

    @BeforeEach
    void setUp() {
        User user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        productCategory = ProductCategory.builder()
                .name("defaultCategory")
                .build();
        productCategoryRepository.save(productCategory);
    }

    @AfterEach
    void tearDown() {
        productCategoryRepository.deleteAll();
    }

    @Test
    void categories() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/productCategories"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("productCategory/productCategories"));
    }

    @Test
    void getAddCategoryPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/productCategories/add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("productCategory/addProductCategory"));
    }

    @Test
    void addCategoryWithValidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/productCategories/add")
                        .param("name", "newCategory"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/productCategories"));
    }

    @Test
    void addCategoryWithInvalidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/productCategories/add")
                        .param("name", ""))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("productCategory/addProductCategory"));
    }

    @Test
    void getEditCategoryPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/productCategories/edit/{id}", productCategory.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("category"))
                .andExpect(MockMvcResultMatchers.view().name("productCategory/editProductCategory"));
    }

    @Test
    void getEditCategoryPageWithCategoryNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/productCategories/edit/{id}", 999))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/productCategories"));
    }

    @Test
    void editCategoryWithValidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/productCategories/edit/{id}", productCategory.getId())
                        .param("name", "exampleCategory"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/productCategories"));
    }

    @Test
    void editCategoryWithInvalidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/productCategories/edit/{id}", 999)
                        .param("name", ""))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/productCategories/edit/999"))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("nonNullCategoryMessage"));
    }

    @Test
    void deleteCategoryShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/productCategories/delete/{id}", productCategory.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/productCategories"));
    }

    @Test
    void deleteCategoryShouldFailWithCategoryNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/productCategories/delete/{id}", 999))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/productCategories"))
                .andExpect(MockMvcResultMatchers.flash().attributeExists("errorMessageDelete"));
    }
}