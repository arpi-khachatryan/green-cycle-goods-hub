package com.epam.greencyclegoods.controllerTest;

import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Arpi Khachatryan
 * Date: 02.12.2023
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CompanyCategoryRepository companyCategoryRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private BasketRepository basketRepository;

    private Company company;

    private ProductCategory productCategory;

    private Product product;

    @BeforeEach
    void setUp() {
        User user = User.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .email(UUID.randomUUID() + "email.@test.com")
                .password("Password1!")
                .createdAt(LocalDateTime.now())
                .role(Role.ADMIN)
                .enabled(true)
                .build();
        userRepository.save(user);

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(user.getEmail());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        CompanyCategory companyCategory = CompanyCategory.builder()
                .name("defaultCategory")
                .build();
        companyCategoryRepository.save(companyCategory);

        company = Company.builder()
                .user(user)
                .name("defaultCompany")
                .phone("+37499112233")
                .address("address")
                .deliveryPrice(1.0)
                .email("defaultCompanyEmail@test.com")
                .companyCategory(companyCategory)
                .build();
        companyRepository.save(company);

        productCategory = ProductCategory.builder()
                .name("defaultCategory")
                .build();
        productCategoryRepository.save(productCategory);

        product = Product.builder()
                .name("defaultProduct")
                .user(user)
                .price(200.0)
                .description("Introducing our eco-friendly Recycled Wood Dining Table – a sustainable and stylish addition to your home. ")
                .productCategory(productCategory)
                .company(company)
                .productCount(8)
                .build();
        productRepository.save(product);
    }

    @AfterEach
    void tearDown() {
        productRepository.deleteAll();
        productCategoryRepository.deleteAll();
        companyRepository.deleteAll();
        companyCategoryRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void products() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/products")
                        .param("sort", "some-sort")
                        .param("page", "1")
                        .param("size", "15"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("product/products"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categories"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("products"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("totalPages"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessageAddToBasket"));
    }

    @Test
    void userProducts() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/products/myproducts")
                        .param("sort", "some-sort")
                        .param("page", "1")
                        .param("size", "15"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("admin/products"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categories"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("totalPages"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessageProduct"));
    }

    @Test
    void getAddProductPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/products/add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("product/addProduct"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("companies"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categories"))
                .andExpect(MockMvcResultMatchers.model().attributeDoesNotExist("errorMessageProduct"));
    }

    @Test
    void addProductWithValidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/products/add")
                        .file(new MockMultipartFile("productImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .param("name", "newProduct")
                        .param("description", "Introducing our eco-friendly newProduct.")
                        .param("price", "200.0")
                        .param("productCategoryId", String.valueOf(productCategory.getId()))
                        .param("companyId", String.valueOf(company.getId()))
                        .param("productCount", "3"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/products/myproducts"));
    }

    @Test
    void addProductWithInvalidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/products/add")
                        .file(new MockMultipartFile("productImage", "test.txt", "text/plain", "Product files".getBytes()))
                        .param("name", "newProduct")
                        .param("description", "Introducing our eco-friendly Recycled Wood newProduct – a sustainable and stylish addition to your home.")
                        .param("price", "")
                        .param("productCategoryId", String.valueOf(productCategory.getId()))
                        .param("companyId", String.valueOf(company.getId()))
                        .param("productCount", "3"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.view().name("product/addProduct"));
    }

    @Test
    void getEditProductPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/products/edit/{id}", product.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("product/editProduct"));
    }

    @Test
    void getEditProductPageShouldFailWithProductNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/products/edit/{id}", 999)
                        .param("errorMessageProduct", "No product found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/products/myproducts"));
    }

    @Test
    void editProductWithValidData() throws Exception {
        Basket basket = Basket.builder()
                .itemQuantity(3)
                .build();
        basketRepository.save(basket);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/products/edit/{id}", product.getId())
                        .file(new MockMultipartFile("productImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .param("name", "exampleProduct")
                        .param("price", "200.0")
                        .param("productCount", "3"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/products/myproducts"));

        basketRepository.deleteById(basket.getId());
    }

    @Test
    void editProductWithInvalidData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/products/edit/{id}", product.getId())
                        .file(new MockMultipartFile("productImage", "test.txt", "text/plain", "Product files".getBytes()))
                        .param("name", "newProduct")
                        .param("price", "200.0")
                        .param("productCount", "0"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/products/edit/" + product.getId()));
    }

    @Test
    void editProductWithProductNotFoundException() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/products/edit/{id}", -1)
                        .file(new MockMultipartFile("productImage", "test.txt", "text/plain", InputStream.nullInputStream()))
                        .param("name", "newProduct")
                        .param("price", "200.0")
                        .param("productCount", "3")
                        .param("errorMessageProduct", "No product found."))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/products/edit/" + (-1)));
    }

    @Test
    void deleteProductShouldSucceed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/products/delete/{id}", product.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/products/myproducts"));
    }

    @Test
    void deleteProductShouldFailWhenProductNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/products/delete/{id}", -1))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/products/myproducts"))
                .andExpect(MockMvcResultMatchers.flash().attribute("errorMessageProduct", "No product found."));
    }
}