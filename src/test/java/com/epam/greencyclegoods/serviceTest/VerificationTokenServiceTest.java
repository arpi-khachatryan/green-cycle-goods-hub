package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.entity.VerificationToken;
import com.epam.greencyclegoods.exception.VerificationException;
import com.epam.greencyclegoods.repository.VerificationTokenRepository;
import com.epam.greencyclegoods.service.impl.VerificationTokenServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 02.12.2023
 */

@ExtendWith(MockitoExtension.class)
public class VerificationTokenServiceTest {

    @InjectMocks
    VerificationTokenServiceImpl verificationTokenService;

    @Mock
    private VerificationTokenRepository tokenRepository;

    @Test
    public void testCreate() {
        // when
        when(tokenRepository.save(any())).thenReturn(getVerificationToken());
        VerificationToken actual = verificationTokenService.create(getUser());

        // then
        assertNotNull(actual);
        verify(tokenRepository, times(1)).save(any());
    }

    @Test
    public void testFindValidToken() throws VerificationException {
        // given
        VerificationToken token = getVerificationTokenWithExpiry();
        String validToken = token.getPlainToken();

        // when
        when(tokenRepository.findByPlainToken(validToken)).thenReturn(Optional.of(token));
        VerificationToken actual = verificationTokenService.find(validToken);

        // then
        assertNotNull(actual);
        assertEquals(token, actual);
        verify(tokenRepository, times(1)).findByPlainToken(validToken);
        verify(tokenRepository, times(0)).delete(any());
    }

    @Test
    public void testFindExpiredToken() {
        // given
        VerificationToken token = getVerificationToken();
        String expiredToken = token.getPlainToken();
        token.setExpiresAt(LocalDateTime.now().minusDays(1));

        // when
        when(tokenRepository.findByPlainToken(expiredToken)).thenReturn(Optional.of(token));
        assertThrows(VerificationException.class, () -> verificationTokenService.find(expiredToken));

        // then
        verify(tokenRepository, times(1)).findByPlainToken(expiredToken);
        verify(tokenRepository, times(1)).delete(any());
    }

    @Test
    public void testFindNonexistentToken() {
        // when
        when(tokenRepository.findByPlainToken(anyString())).thenReturn(Optional.empty());
        assertThrows(VerificationException.class, () -> verificationTokenService.find(anyString()));

        // then
        verify(tokenRepository, times(1)).findByPlainToken(anyString());
        verify(tokenRepository, times(0)).delete(any());
    }

    @Test
    public void testDelete() {
        // given
        VerificationToken token = getVerificationToken();

        // when
        doNothing().when(tokenRepository).delete(token);
        verificationTokenService.delete(token);

        // then
        verify(tokenRepository, times(1)).delete(token);
    }
}