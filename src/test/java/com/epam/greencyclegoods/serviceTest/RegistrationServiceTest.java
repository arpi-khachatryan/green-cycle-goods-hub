package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.registration.CreateRegistrationDto;
import com.epam.greencyclegoods.dto.registration.EditRegistrationDto;
import com.epam.greencyclegoods.dto.registration.RegistrationOverview;
import com.epam.greencyclegoods.entity.Registration;
import com.epam.greencyclegoods.entity.RegistrationStatus;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.RegistrationCreationException;
import com.epam.greencyclegoods.exception.RegistrationNotFoundException;
import com.epam.greencyclegoods.mapper.RegistrationMapper;
import com.epam.greencyclegoods.repository.RegistrationRepository;
import com.epam.greencyclegoods.service.impl.RegistrationServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
class RegistrationServiceTest {

    @InjectMocks
    RegistrationServiceImpl registrationService;

    @Mock
    RegistrationMapper registrationMapper;

    @Mock
    RegistrationRepository registrationRepository;

    @Test
    void testRegistrations() {
        // given
        Pageable pageable = PageRequest.of(0, 10);
        Page<Registration> registrationPage = new PageImpl<>(List.of(getRegistration(), getRegistration(), getRegistration()));

        // when
        when(registrationRepository.findAll(pageable)).thenReturn(registrationPage);
        Page<RegistrationOverview> actual = registrationService.registrations(pageable);

        // then
        assertNotNull(actual);
    }

    @Test
    void testRegistrationsByUser() {
        // given
        Page<Registration> registrationPage = new PageImpl<>(List.of(getRegistration(), getRegistration(), getRegistration()));
        Pageable pageable = PageRequest.of(0, 10);

        // when
        when(registrationRepository.findByUserId(getUser().getId(), pageable)).thenReturn(registrationPage);
        Page<RegistrationOverview> result = registrationService.registrationsByUser(getUser().getId(), pageable);

        // then
        assertNotNull(result);
    }

    @Test
    void testRegistrationsByOwner() {
        // given
        Pageable pageable = PageRequest.of(0, 10);
        User owner = getOwnerUser();
        Page<Registration> registrationPage = new PageImpl<>(List.of(getRegistration(), getRegistration(), getRegistration()));

        // when
        when(registrationRepository.findRegistrationsByCompanyOwner(owner.getId(), pageable)).thenReturn(registrationPage);

        // then
        assertDoesNotThrow(() -> registrationService.registrationsByOwner(owner, pageable));
    }

    @Test
    void testRegistrationById() {
        // when
        when(registrationRepository.findById(anyInt())).thenReturn(Optional.of(getRegistration()));
        when(registrationMapper.mapToDto(any())).thenReturn(getRegistrationOverview());

        // then
        assertDoesNotThrow(() -> registrationService.registrationById(anyInt()));
    }

    @Test
    void testEdit() throws RegistrationNotFoundException {
        // given
        EditRegistrationDto editRegistrationDto = getEditRegistrationDto();
        Registration existingRegistration = getRegistration();

        // when
        when(registrationRepository.findById(anyInt())).thenReturn(Optional.of(existingRegistration));
        registrationService.edit(editRegistrationDto, existingRegistration.getId());

        // then
        verify(registrationRepository, times(1)).findById(anyInt());
        verify(registrationRepository, times(1)).save(existingRegistration);
    }

    @Test
    void testEditThrowsRegistrationNotFoundException() {
        // given
        int invalidRegistrationId = -1;
        EditRegistrationDto editDto = getEditRegistrationDto();

        // when
        when(registrationRepository.findById(invalidRegistrationId)).thenReturn(Optional.empty());
        RegistrationNotFoundException exception = assertThrows(RegistrationNotFoundException.class, () -> registrationService.edit(editDto, invalidRegistrationId));

        // then
        verify(registrationRepository, times(1)).findById(invalidRegistrationId);
        verify(registrationRepository, never()).save(any());
    }

    @Test
    void testDelete() throws RegistrationNotFoundException {
        // when
        when(registrationRepository.existsById(anyInt())).thenReturn(true);
        registrationService.delete(anyInt());

        // then
        verify(registrationRepository, times(1)).deleteById(anyInt());
    }

    @Test
    void testDeleteThrowsRegistrationNotFoundException() {
        // given
        int nonExistingRegistrationId = 2;

        // when
        when(registrationRepository.existsById(nonExistingRegistrationId)).thenReturn(false);
        assertThrows(RegistrationNotFoundException.class, () -> registrationService.delete(nonExistingRegistrationId));

        // then
        verify(registrationRepository, never()).deleteById(anyInt());
    }

    @Test
    void testAdd() throws RegistrationCreationException {
        // given
        CreateRegistrationDto createRegistrationDto = getCreateRegistrationDto();
        User user = getUser();
        Registration registration = getRegistration();

        // when
        when(registrationMapper.mapToEntity(createRegistrationDto)).thenReturn(registration);
        when(registrationRepository.findRegistrationByUserAndEvent(user, createRegistrationDto.getEventId())).thenReturn(Optional.empty());
        registrationService.addRegistration(createRegistrationDto, user);

        // then
        verify(registrationRepository, times(1)).save(registration);
        assertEquals(RegistrationStatus.PENDING, registration.getStatus());
    }

    @Test
    void testAddThrowsRegistrationCreationException() {
        // given
        CreateRegistrationDto createRegistrationDto = getCreateRegistrationDto();
        User user = getUser();

        // when
        when(registrationRepository.findRegistrationByUserAndEvent(user, createRegistrationDto.getEventId())).thenReturn(Optional.of(new Registration()));
        assertThrows(RegistrationCreationException.class, () -> registrationService.addRegistration(createRegistrationDto, user));

        // then
        verify(registrationRepository, never()).save(any());
    }

    @Test
    void testAddRegistrationWithNullUser() {
        // given
        CreateRegistrationDto createRegistrationDto = getCreateRegistrationDto();

        // when
        assertThrows(RegistrationCreationException.class, () -> registrationService.addRegistration(createRegistrationDto, null));

        // then
        verify(registrationRepository, never()).save(any());
    }
}