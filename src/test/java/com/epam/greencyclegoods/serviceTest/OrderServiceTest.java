package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.dto.order.OrderOverview;
import com.epam.greencyclegoods.entity.Basket;
import com.epam.greencyclegoods.entity.Order;
import com.epam.greencyclegoods.entity.Product;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.mapper.OrderMapper;
import com.epam.greencyclegoods.repository.BasketRepository;
import com.epam.greencyclegoods.repository.OrderRepository;
import com.epam.greencyclegoods.repository.ProductRepository;
import com.epam.greencyclegoods.service.BasketService;
import com.epam.greencyclegoods.service.PaymentService;
import com.epam.greencyclegoods.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @InjectMocks
    OrderServiceImpl orderService;

    @Mock
    OrderRepository orderRepository;

    @Mock
    OrderMapper orderMapper;

    @Mock
    BasketService basketService;

    @Mock
    PaymentService paymentService;

    @Mock
    ProductRepository productRepository;

    @Mock
    BasketRepository basketRepository;

    @Test
    void testOrders() {
        // given
        Pageable pageable = PageRequest.of(0, 10);
        Page<Order> orders = new PageImpl<>(List.of(getOrder(), getOrder(), getOrder()));

        // when
        when(orderRepository.findAll(pageable)).thenReturn(orders);
        Page<OrderOverview> actual = orderService.orders(pageable);

        // then
        assertFalse(actual.isEmpty());
    }

    @Test
    void testOwnerOrders() {
        // given
        Pageable pageable = PageRequest.of(0, 10);
        Page<Order> orders = new PageImpl<>(List.of(getOwnerOrder(), getOwnerOrder(), getOwnerOrder()));

        // when
        when(orderRepository.findOrdersByCompanyOwner(1, pageable)).thenReturn(orders);
        Page<OrderOverview> actual = orderService.ownerOrders(1, pageable);

        // then
        assertFalse(actual.isEmpty());
    }

    @Test
    void testUserOrders() {
        // given
        User user = getUser();
        Pageable pageable = PageRequest.of(0, 10);
        Page<Order> orders = new PageImpl<>(List.of(getOrder(), getOrder(), getOrder()));

        // when
        when(orderRepository.findByUserId(user.getId(), pageable)).thenReturn(orders);
        Page<OrderOverview> actual = orderService.userOrders(user.getId(), pageable);

        // then
        assertFalse(actual.isEmpty());
    }

    @Test
    void testOrder() throws OrderNotFoundException {
        // given
        Order order = getOrder();

        // when
        when(orderRepository.findById(order.getId())).thenReturn(Optional.of(order));
        when(orderMapper.mapToDto(order)).thenReturn(new OrderOverview());
        OrderOverview actual = orderService.order(order.getId());

        // then
        assertNotNull(actual);
    }

    @Test
    void testOrderNotFound() {
        //when
        when(orderRepository.findById(getOrder().getId())).thenReturn(Optional.empty());
        assertThrows(OrderNotFoundException.class, () -> orderService.order(getOrder().getId()));

        // then
        verify(orderMapper, never()).mapToDto(any(Order.class));
    }

    @Test
    void testProductQuantity() throws OrderNotFoundException {
        // given
        Order order = getOrder();
        Product product1 = getProduct();
        Product product2 = getProduct();
        List<Product> products = List.of(product1, product1, product2, product2, product2);
        order.setProducts(products);

        // when
        when(orderRepository.findById(anyInt())).thenReturn(Optional.of(order));
        Map<Product, Double> result = orderService.productQuantity(anyInt());

        // then
        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.containsKey(product1));
        assertTrue(result.containsKey(product2));
        assertEquals(2.0, result.get(product1));
        assertEquals(3.0, result.get(product2));
    }

    @Test
    void testProductQuantityThrowsOrderNotFoundException() {
        // when
        when(orderRepository.findById(anyInt())).thenReturn(Optional.empty());

        // then
        assertThrows(OrderNotFoundException.class, () -> orderService.productQuantity(anyInt()));
    }

    @Test
    void testAddOrderSuccessfully() throws BasketNotFoundException, OrderNotFoundException, UserNotFoundException, CreditCardNotFoundException, ProductNotFoundException {
        // given
        Order order = getOrder();
        CreateCreditCardDto cardDto = new CreateCreditCardDto();
        User user = getUser();
        Product product = getProduct();
        Basket basket = getBasket();
        basket.setItemQuantity(2.0);
        List<Basket> basketList = List.of(basket);

        // when
        when(basketService.calculateTotalBasketPrice(user)).thenReturn(10.0);
        when(basketRepository.findBasketByUser(user)).thenReturn(basketList);
        basketRepository.findBasketByUser(user);
        basketService.calculateTotalBasketPrice(user);
        basketService.delete(1, user);
        paymentService.addPayment(order, new CreateCreditCardDto(), user);
        when(productRepository.save(any(Product.class))).thenAnswer(invocation -> {
            Product savedProduct = invocation.getArgument(0);
            savedProduct.setId(product.getId());
            return savedProduct;
        });
        productRepository.save(getProduct());
        orderRepository.save(getOrder());

        // then
        verify(paymentService, times(1)).addPayment(eq(order), eq(new CreateCreditCardDto()), eq(user));
        verify(orderRepository, times(1)).save(any(Order.class));
        verify(paymentService, times(1)).addPayment(any(Order.class), eq(cardDto), eq(user));
        verify(basketService, times(1)).calculateTotalBasketPrice(user);
        verify(basketRepository, times(1)).findBasketByUser(user);
        verify(basketService, times(1)).delete(1, user);
        verify(productRepository, times(1)).save(any(Product.class));
    }

    @Test
    void testDelete() {
        // when
        when(orderRepository.existsById(anyInt())).thenReturn(true);
        assertDoesNotThrow(() -> orderService.delete(anyInt()));

        // then
        verify(orderRepository, times(1)).existsById(anyInt());
        verify(orderRepository, times(1)).deleteById(anyInt());
    }

    @Test
    void testDeleteThrowsOrderNotFoundException() {
        // when
        when(orderRepository.existsById(anyInt())).thenReturn(false);
        assertThrows(OrderNotFoundException.class, () -> orderService.delete(anyInt()));

        // then
        verify(orderRepository, never()).deleteById(anyInt());
    }
}
