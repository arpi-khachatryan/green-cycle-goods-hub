package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.company.CompanyOverview;
import com.epam.greencyclegoods.dto.company.CreateCompanyDto;
import com.epam.greencyclegoods.dto.company.EditCompanyDto;
import com.epam.greencyclegoods.entity.Company;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.mapper.CompanyMapper;
import com.epam.greencyclegoods.repository.*;
import com.epam.greencyclegoods.service.impl.CompanyServiceImpl;
import com.epam.greencyclegoods.utility.ImageUtility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
public class CompanyServiceTest {

    @InjectMocks
    private CompanyServiceImpl companyService;

    @Mock
    private ImageUtility imageUtility;

    @Mock
    private CompanyMapper companyMapper;

    @Mock
    CompanyRepository companyRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    CompanyCategoryRepository companyCategoryRepository;

    @Mock
    ProductRepository productRepository;

    @Mock
    EventRepository eventRepository;

    @Test
    void testCompanies() {
        // given
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        List<Company> companyList = List.of(getCompany(), getCompany(), getCompany());
        List<CompanyOverview> companyOverviewList = List.of(getCompanyOverview(), getCompanyOverview(), getCompanyOverview());
        Page<Company> companyPage = new PageImpl<>(companyList, pageable, companyList.size());

        // when
        when(companyRepository.findAll(pageable)).thenReturn(companyPage);
        when(companyMapper.mapToDto(any())).thenReturn(getCompanyOverview());
        Page<CompanyOverview> result = companyService.companies(pageable);

        // then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(companyOverviewList.size(), result.getContent().size());
        verify(companyRepository, times(1)).findAll(pageable);
        verify(companyMapper, times(companyList.size())).mapToDto(any());
    }

    @Test
    void testCompaniesByUser() throws CompanyNotFoundException {
        // given
        User user = getUser();
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        List<Company> companyList = List.of(getCompany(), getCompany(), getCompany());
        List<CompanyOverview> companyOverviewList = List.of(getCompanyOverview(), getCompanyOverview(), getCompanyOverview());
        Page<Company> companyPage = new PageImpl<>(companyList, pageable, companyList.size());

        // when
        when(companyRepository.findCompanyByUserId(user.getId(), pageable)).thenReturn(companyPage);
        when(companyMapper.mapToDto(any())).thenReturn(getCompanyOverview());
        Page<CompanyOverview> result = companyService.companiesByUser(user, pageable);

        // then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(companyOverviewList.size(), result.getContent().size());
        verify(companyRepository, times(1)).findCompanyByUserId(user.getId(), pageable);
        verify(companyMapper, times(companyList.size())).mapToDto(any());
    }

    @Test
    void testCompany() throws CompanyNotFoundException {
        // given
        Company company = getCompany();
        CompanyOverview companyOverview = getCompanyOverview();

        // when
        when(companyRepository.findById(anyInt())).thenReturn(Optional.of(company));
        when(companyMapper.mapToDto(company)).thenReturn(companyOverview);
        CompanyOverview result = companyService.company(anyInt());

        // then
        assertNotNull(result);
        assertEquals(companyOverview, result);
        verify(companyRepository, times(1)).findById(anyInt());
        verify(companyMapper, times(1)).mapToDto(company);
    }

    @Test
    void testCompaniesList() {
        // given
        List<Company> companyList = List.of(getCompany(), getCompany(), getCompany());
        List<CompanyOverview> companyOverviewList = List.of(getCompanyOverview(), getCompanyOverview(), getCompanyOverview());

        // when
        when(companyRepository.findAll()).thenReturn(companyList);
        when(companyMapper.mapToDtoList(companyList)).thenReturn(companyOverviewList);
        List<CompanyOverview> result = companyService.companies();

        // then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(companyOverviewList.size(), result.size());
        verify(companyRepository, times(1)).findAll();
        verify(companyMapper, times(1)).mapToDtoList(companyList);
    }

    @Test
    void testEdit() throws CompanyNotFoundException, ImageUploadException, IOException {
        // given
        EditCompanyDto editCompanyDto = getEditCompanyDto();
        MultipartFile[] files = new MultipartFile[]{new MockMultipartFile("file", "test.txt", "text/plain", "Companies files".getBytes())};
        Company company = getCompany();

        // when
        when(companyRepository.findById(anyInt())).thenReturn(Optional.of(company));
        when(companyCategoryRepository.getReferenceById(editCompanyDto.getCompanyCategoryId())).thenReturn(getCompanyCategory());
        when(imageUtility.uploadImages(files)).thenReturn(Collections.singletonList("image_path"));
        companyService.edit(editCompanyDto, anyInt(), files);

        // then
        verify(companyRepository, times(1)).findById(anyInt());
        verify(companyCategoryRepository, times(1)).getReferenceById(editCompanyDto.getCompanyCategoryId());
        verify(imageUtility, times(1)).uploadImages(files);
        verify(companyRepository, times(1)).save(company);
    }

    @Test
    void testEditCompanyNotFoundException() {
        // given
        EditCompanyDto editCompanyDto = getEditCompanyDto();
        MultipartFile[] files = null;

        // when
        when(companyRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(CompanyNotFoundException.class, () -> companyService.edit(editCompanyDto, anyInt(), files));

        // then
        verify(companyRepository, times(1)).findById(anyInt());
        verifyNoMoreInteractions(companyRepository, companyCategoryRepository, imageUtility);
    }

    @Test
    void testEditImageUploadException() throws IOException {
        // given
        EditCompanyDto editCompanyDto = getEditCompanyDto();
        MultipartFile[] files = new MultipartFile[]{new MockMultipartFile("file", "test.txt", "text/plain", "Companies files.".getBytes())};
        Company company = getCompany();

        // when
        when(companyRepository.findById(anyInt())).thenReturn(Optional.of(company));
        when(companyCategoryRepository.getReferenceById(editCompanyDto.getCompanyCategoryId())).thenReturn(getCompanyCategory());
        when(imageUtility.uploadImages(files)).thenThrow(new IOException());
        assertThrows(CompanyNotFoundException.class, () -> companyService.edit(editCompanyDto, anyInt(), files));

        // then
        verify(companyRepository, times(1)).findById(anyInt());
        verify(companyCategoryRepository, times(1)).getReferenceById(editCompanyDto.getCompanyCategoryId());
        verify(imageUtility, times(1)).uploadImages(files);
        verifyNoMoreInteractions(companyRepository, companyCategoryRepository);
    }

    @Test
    void testAdd() throws UserUpdateException, ImageUploadException, CompanyCreationException, IOException {
        // given
        CreateCompanyDto createCompanyDto = getCreateCompanyDto();
        MultipartFile[] files = new MultipartFile[]{new MockMultipartFile("file", "test.txt", "text/plain", "Companies files".getBytes())};
        User user = getUser();
        Company company = getCompany();
        company.setUser(user);

        // when
        when(companyRepository.existsByEmailIgnoreCase(createCompanyDto.getEmail())).thenReturn(false);
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        when(companyMapper.mapToEntity(createCompanyDto)).thenReturn(company);
        when(imageUtility.uploadImages(files)).thenReturn(Collections.singletonList("image_path"));
        companyService.add(createCompanyDto, files, user);

        // then
        verify(companyRepository, times(1)).existsByEmailIgnoreCase(createCompanyDto.getEmail());
        verify(userRepository, times(1)).findById(user.getId());
        verify(userRepository, times(1)).save(user);
        verify(imageUtility, times(1)).uploadImages(files);
        verify(companyRepository, times(1)).save(any());
    }

    @Test
    void testAddCompanyExistsByEmail() {
        // given
        CreateCompanyDto createCompanyDto = getCreateCompanyDto();
        MultipartFile[] files = new MultipartFile[]{new MockMultipartFile("file", "test.txt", "text/plain", "Companies files".getBytes())};
        User user = getUser();

        // when
        when(companyRepository.existsByEmailIgnoreCase(createCompanyDto.getEmail())).thenReturn(true);
        assertThrows(CompanyCreationException.class, () -> companyService.add(createCompanyDto, files, user));

        // then
        verify(companyRepository, times(1)).existsByEmailIgnoreCase(createCompanyDto.getEmail());
        verifyNoMoreInteractions(userRepository, companyRepository, imageUtility);
    }

    @Test
    void testDelete() {
        // when
        when(companyRepository.findById(anyInt())).thenReturn(Optional.of(new Company()));
        when(productRepository.existsByCompanyId(anyInt())).thenReturn(false);
        when(eventRepository.existsByCompanyId(anyInt())).thenReturn(false);
        assertDoesNotThrow(() -> companyService.delete(anyInt()));

        // then
        verify(companyRepository, times(1)).deleteById(anyInt());
    }

    @Test
    void testDeleteThrowsCompanyDeleteException() {
        // when
        when(companyRepository.findById(anyInt())).thenReturn(Optional.of(new Company()));
        when(productRepository.existsByCompanyId(anyInt())).thenReturn(true);
        when(eventRepository.existsByCompanyId(anyInt())).thenReturn(false);
        assertThrows(CompanyDeleteException.class, () -> companyService.delete(anyInt()));

        // then
        verify(companyRepository, never()).deleteById(anyInt());
    }

    @Test
    void testDeleteCompanyThrowsCompanyNotFoundException() {
        // when
        when(companyRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(CompanyNotFoundException.class, () -> companyService.delete(anyInt()));

        // then
        verify(companyRepository, never()).deleteById(anyInt());
    }
}