package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.productCategory.CreateProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.EditProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.ProductCategoryOverview;
import com.epam.greencyclegoods.entity.ProductCategory;
import com.epam.greencyclegoods.exception.CategoryDeleteException;
import com.epam.greencyclegoods.exception.CategoryNotFoundException;
import com.epam.greencyclegoods.exception.CategoryUpdateException;
import com.epam.greencyclegoods.exception.ProductCategorySaveException;
import com.epam.greencyclegoods.mapper.ProductCategoryMapper;
import com.epam.greencyclegoods.repository.ProductCategoryRepository;
import com.epam.greencyclegoods.repository.ProductRepository;
import com.epam.greencyclegoods.service.impl.ProductCategoryServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
public class ProductCategoryServiceTest {

    @InjectMocks
    ProductCategoryServiceImpl productCategoryService;

    @Mock
    ProductCategoryMapper categoryMapper;

    @Mock
    ProductCategoryRepository productCategoryRepository;

    @Mock
    ProductRepository productRepository;

    @Test
    public void testCategoriesPageable() {
        // given
        Pageable pageable = PageRequest.of(0, 10);
        List<ProductCategory> categoryList = List.of(getProductCategory(), getProductCategory(), getProductCategory());
        Page<ProductCategory> page = new PageImpl<>(categoryList, pageable, categoryList.size());

        // when
        when(productCategoryRepository.findAll(pageable)).thenReturn(page);
        Page<ProductCategoryOverview> actual = productCategoryService.categories(pageable);

        // then
        assertNotNull(actual);
        assertFalse(actual.isEmpty());
    }

    @Test
    public void testGetAllCategoriesPageableEmpty() {
        // given
        Pageable pageable = PageRequest.of(0, 10);
        when(productCategoryRepository.findAll(pageable)).thenReturn(Page.empty());

        // when
        Page<ProductCategoryOverview> result = productCategoryService.categories(pageable);

        // then
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void testCategories() {
        // given
        List<ProductCategory> categoryList = List.of(getProductCategory(), getProductCategory(), getProductCategory());
        List<ProductCategoryOverview> productCategoryOverviews = List.of(getProductCategoryOverview(), getProductCategoryOverview(), getProductCategoryOverview());

        // when
        when(productCategoryRepository.findAll()).thenReturn(categoryList);
        when(categoryMapper.mapToDtoList(categoryList)).thenReturn(productCategoryOverviews);
        List<ProductCategoryOverview> result = productCategoryService.categories();

        // then
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    public void testCategoriesEmptyList() {
        // when
        when(productCategoryRepository.findAll()).thenReturn(Collections.emptyList());
        List<ProductCategoryOverview> result = productCategoryService.categories();

        // then
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void testSave() throws ProductCategorySaveException {
        // given
        CreateProductCategoryDto createDto = getCreateProductCategoryDto();
        ProductCategory productCategory = getProductCategory();

        // when
        when(categoryMapper.mapToEntity(createDto)).thenReturn(productCategory);
        when(productCategoryRepository.save(productCategory)).thenReturn(productCategory);
        productCategoryService.save(createDto);

        // then
        verify(productCategoryRepository, times(1)).save(productCategory);
    }

    @Test
    public void testSaveThrowsProductCategorySaveException() throws ProductCategorySaveException {
        // given
        CreateProductCategoryDto createDto = new CreateProductCategoryDto();

        // when
        productCategoryService.save(createDto);

        // then
        verifyNoInteractions(productCategoryRepository);
    }

    @Test
    public void testEdit() throws CategoryUpdateException, CategoryNotFoundException {
        // given
        EditProductCategoryDto editDto = getEditProductCategoryDto();
        ProductCategory existingCategory = getProductCategory();

        // when
        when(productCategoryRepository.findById(anyInt())).thenReturn(Optional.of(existingCategory));
        when(productCategoryRepository.existsByNameIgnoreCase("Dishes")).thenReturn(false);
        productCategoryService.edit(editDto, anyInt());

        // then
        verify(productCategoryRepository).save(existingCategory);
    }

    @Test
    public void testEditProductCategoryInvalidName() {
        // given
        EditProductCategoryDto editDto = getEditProductCategoryDto();
        ProductCategory existingCategory = getProductCategory();

        // when
        when(productCategoryRepository.findById(anyInt())).thenReturn(Optional.of(existingCategory));
        when(productCategoryRepository.existsByNameIgnoreCase(editDto.getName())).thenReturn(true);

        // then
        assertThrows(CategoryUpdateException.class, () -> productCategoryService.edit(editDto, anyInt()));
    }

    @Test
    public void testEditProductCategoryNotFound() {
        // given
        EditProductCategoryDto editDto = getEditProductCategoryDto();

        // when
        when(productCategoryRepository.findById(anyInt())).thenReturn(Optional.empty());

        // then
        assertThrows(CategoryNotFoundException.class, () -> productCategoryService.edit(editDto, anyInt()));
    }

    @Test
    public void testDeleteProductCategory() throws CategoryNotFoundException, CategoryDeleteException {
        // when
        when(productCategoryRepository.existsById(anyInt())).thenReturn(true);
        when(productRepository.existsByProductCategoryId(anyInt())).thenReturn(false);
        productCategoryService.delete(anyInt());

        // then
        verify(productCategoryRepository).deleteById(anyInt());
    }

    @Test
    public void testDeleteThrowsCategoryDeleteException() {
        // when
        when(productCategoryRepository.existsById(anyInt())).thenReturn(true);
        when(productRepository.existsByProductCategoryId(anyInt())).thenReturn(true);

        //then
        assertThrows(CategoryDeleteException.class, () -> productCategoryService.delete(anyInt()));
    }

    @Test
    public void testDeleteThrowsCategoryNotFoundException() {
        // when
        when(productCategoryRepository.existsById(anyInt())).thenReturn(false);

        // then
        assertThrows(CategoryNotFoundException.class, () -> productCategoryService.delete(anyInt()));
    }
}
