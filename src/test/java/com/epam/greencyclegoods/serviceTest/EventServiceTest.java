package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.event.CreateEventDto;
import com.epam.greencyclegoods.dto.event.EditEventDto;
import com.epam.greencyclegoods.dto.event.EventOverview;
import com.epam.greencyclegoods.entity.Company;
import com.epam.greencyclegoods.entity.Event;
import com.epam.greencyclegoods.entity.Registration;
import com.epam.greencyclegoods.exception.EmailSendingException;
import com.epam.greencyclegoods.exception.EventDeleteException;
import com.epam.greencyclegoods.exception.EventNotFoundException;
import com.epam.greencyclegoods.exception.ImageUploadException;
import com.epam.greencyclegoods.mapper.EventMapper;
import com.epam.greencyclegoods.repository.CompanyRepository;
import com.epam.greencyclegoods.repository.EventRepository;
import com.epam.greencyclegoods.repository.RegistrationRepository;
import com.epam.greencyclegoods.service.impl.EventServiceImpl;
import com.epam.greencyclegoods.service.impl.MailServiceImpl;
import com.epam.greencyclegoods.utility.ImageUtility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
class EventServiceTest {

    @InjectMocks
    EventServiceImpl eventService;

    @Mock
    EventMapper eventMapper;

    @Mock
    EventRepository eventRepository;

    @Mock
    CompanyRepository companyRepository;

    @Mock
    RegistrationRepository registrationRepository;

    @Mock
    ImageUtility imageUtility;

    @Mock
    MailServiceImpl mailService;

    @Test
    void testEvents() {
        // given
        PageRequest pageable = PageRequest.of(1, 1, Sort.Direction.fromString("DESC"), "name");
        List<Event> eventsList = List.of(getEvent(), getEvent(), getEvent());
        Page<Event> eventsPage = new PageImpl<>(eventsList);

        // when
        when(eventRepository.findAll(pageable)).thenReturn(eventsPage);
        Page<EventOverview> result = eventService.events(pageable);

        // then
        verify(eventRepository, times(1)).findAll(pageable);
        verify(eventMapper, times(eventsList.size())).mapToDto(any(Event.class));
        assertFalse(result.isEmpty());
    }

    @Test
    void testSortByCompany() {
        // given
        List<Company> companies = List.of(getCompany(), getCompany(), getCompany());
        List<Event> events = List.of(getEvent(), getEvent(), getEvent());

        // when
        when(companyRepository.findAll()).thenReturn(companies);
        when(eventRepository.findEventsByCompany_Id(anyInt())).thenReturn(events);
        eventService.sortByCompany();

        // then
        verify(companyRepository, times(1)).findAll();
    }

    @Test
    void eventsByUserEmptyPage() {
        // given
        PageRequest pageable = PageRequest.of(1, 1, Sort.Direction.fromString("DESC"), "name");

        //when
        when(eventRepository.findAll(any(Pageable.class))).thenReturn(Page.empty());
        Page<EventOverview> result = eventService.eventsByUser(getUser(), pageable);

        // then
        assertEquals(Page.empty(), result);
        verifyNoInteractions(eventMapper);
    }

    @Test
    void eventById_shouldReturnEventOverview_whenEventExists() throws EventNotFoundException {
        // given
        Event mockEvent = getEvent();
        EventOverview mockEventOverview = getEventOverview();

        //when
        when(eventRepository.findById(anyInt())).thenReturn(Optional.of(mockEvent));
        when(eventMapper.mapToDto(mockEvent)).thenReturn(mockEventOverview);
        EventOverview result = eventService.eventById(anyInt());

        // then
        assertEquals(mockEventOverview, result);
        verify(eventRepository, times(1)).findById(anyInt());
        verify(eventMapper, times(1)).mapToDto(mockEvent);
    }

    @Test
    void eventByIdThrowsEventNotFoundException() {
        // when
        when(eventRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(EventNotFoundException.class, () -> eventService.eventById(anyInt()));

        // then
        verify(eventRepository, times(1)).findById(anyInt());
        verifyNoInteractions(eventMapper);
    }

    @Test
    void companyEvents() {
        // given
        List<Event> mockEvents = List.of(getEvent(), getEvent(), getEvent());
        List<EventOverview> mockEventOverviews = List.of(getEventOverview(), getEventOverview(), getEventOverview());

        // when
        when(eventRepository.findEventsByCompany_Id(anyInt())).thenReturn(mockEvents);
        when(eventMapper.mapToDtoList(mockEvents)).thenReturn(mockEventOverviews);
        List<EventOverview> result = eventService.companyEvents(anyInt());

        // then
        assertEquals(mockEventOverviews, result);
        verify(eventRepository, times(1)).findEventsByCompany_Id(anyInt());
        verify(eventMapper, times(1)).mapToDtoList(mockEvents);
    }

    @Test
    void companyEventsEmptyList() {
        // when
        when(eventRepository.findEventsByCompany_Id(anyInt())).thenReturn(Collections.emptyList());
        List<EventOverview> result = eventService.companyEvents(anyInt());

        // then
        assertEquals(Collections.emptyList(), result);
        verify(eventRepository, times(1)).findEventsByCompany_Id(anyInt());
        verifyNoInteractions(eventMapper);
    }

    @Test
    void save_shouldSaveEventWithUploadedImages() throws ImageUploadException, IOException {
        // given
        CreateEventDto mockDto = mock(CreateEventDto.class);
        MultipartFile[] mockFiles = {mock(MultipartFile.class), mock(MultipartFile.class)};
        List<String> mockImages = List.of("image.jpg", "image.jpg");

        // when
        when(imageUtility.uploadImages(mockFiles)).thenReturn(mockImages);
        when(eventMapper.mapToEntity(mockDto)).thenReturn(mock(Event.class));
        eventService.save(mockDto, mockFiles);

        // then
        verify(imageUtility, times(1)).uploadImages(mockFiles);
        verify(mockDto, times(1)).setPictures(mockImages);
        verify(eventMapper, times(1)).mapToEntity(mockDto);
        verify(eventRepository, times(1)).save(any(Event.class));
    }

    @Test
    void saveThrowsImageUploadException() throws IOException {
        // given
        CreateEventDto createEventDto = getCreateEventDto();
        MultipartFile[] mockFiles = {mock(MultipartFile.class), mock(MultipartFile.class)};

        // when
        when(imageUtility.uploadImages(mockFiles)).thenThrow(IOException.class);
        assertThrows(ImageUploadException.class, () -> eventService.save(createEventDto, mockFiles));

        // then
        verifyNoInteractions(eventRepository);
    }

    @Test
    public void testEditEvent() throws EventNotFoundException, ImageUploadException, IOException {
        // given
        EditEventDto editEventDto = getEditEventDto();
        Company newCompany = getCompany();
        Event existingEvent = getEvent();
        MultipartFile[] files = new MultipartFile[1];
        MultipartFile file = mock(MultipartFile.class);

        // when
        when(file.isEmpty()).thenReturn(false);
        files[0] = file;
        when(eventRepository.findById(anyInt())).thenReturn(Optional.of(existingEvent));
        when(companyRepository.findById(anyInt())).thenReturn(Optional.of(newCompany));
        when(imageUtility.uploadImages(files)).thenReturn(Arrays.asList("image1.jpg", "image2.jpg"));
        eventService.edit(editEventDto, anyInt(), files);

        // then
        verify(eventRepository, times(1)).findById(anyInt());
        verify(eventRepository, times(1)).save(existingEvent);
        assertEquals("Hrazdan River Cleaning", existingEvent.getName());
        assertEquals("Join Us in Restoring the Beauty of Hrazdan River.", existingEvent.getDescription());
        assertEquals(0.0, existingEvent.getPrice(), 0.001);
        assertEquals(1, existingEvent.getCompany().getId());
        assertEquals(Arrays.asList("image1.jpg", "image2.jpg"), existingEvent.getPictures());
    }

    @Test()
    public void testEditThrowsEventNotFoundException() {
        // give
        EditEventDto editEventDto = getEditEventDto();
        MultipartFile[] files = new MultipartFile[1];

        // when
        when(eventRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(EventNotFoundException.class, () -> eventService.edit(editEventDto, anyInt(), files));

        // then
        verifyNoMoreInteractions(eventRepository);
    }

    @Test()
    public void testEditImageUploadException() throws IOException {
        // given
        EditEventDto editEventDto = getEditEventDto();
        MultipartFile[] files = new MultipartFile[1];
        MultipartFile file = mock(MultipartFile.class);
        Event existingEvent = getEvent();

        // when
        when(file.isEmpty()).thenReturn(false);
        files[0] = file;
        when(eventRepository.findById(anyInt())).thenReturn(Optional.of(existingEvent));
        when(imageUtility.uploadImages(files)).thenThrow(new IOException());
        assertThrows(ImageUploadException.class, () -> eventService.edit(editEventDto, anyInt(), files));

        // then
        verifyNoMoreInteractions(eventRepository);
    }

    @Test
    public void testDeleteEvent() throws EventNotFoundException, EventDeleteException, EmailSendingException {
        // given
        Event event = getEvent();

        //when
        when(eventRepository.findById(anyInt())).thenReturn(Optional.of(event));
        when(registrationRepository.findByEventId(anyInt())).thenReturn(Collections.emptyList());
        eventService.delete(anyInt());

        // then
        verify(eventRepository, times(1)).findById(anyInt());
        verify(registrationRepository, times(1)).findByEventId(anyInt());
        verify(eventRepository, times(1)).delete(event);
    }

    @Test
    public void testDeleteThrowsEventNotFoundException() throws EventNotFoundException, EventDeleteException {
        // when
        when(eventRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(EventNotFoundException.class, () -> eventService.delete(anyInt()));

        // then
        verifyNoMoreInteractions(eventRepository);
    }

    @Test
    public void testDeleteEventWithRegistrations() throws EventNotFoundException, EventDeleteException, EmailSendingException {
        // given
        Event event = getEvent();
        event.setEventDateTime(LocalDateTime.now());
        Registration registration = getRegistration();
        registration.setEvent(event);
        List<Registration> registrations = List.of(registration, registration, registration);

        // when
        when(eventRepository.findById(ArgumentMatchers.eq(event.getId()))).thenReturn(Optional.of(event));
        when(registrationRepository.findByEventId(ArgumentMatchers.eq(event.getId()))).thenReturn(registrations);
        mailService.sendEmail(anyString(), anyString(), anyString());
        eventService.delete(event.getId());

        // then
        verify(eventRepository, times(1)).delete(event);
    }

    @Test
    public void testGetEvent() throws EventNotFoundException {
        // given
        Event event = getEvent();
        EventOverview eventOverview = getEventOverview();

        // when
        when(eventRepository.findById(anyInt())).thenReturn(Optional.of(event));
        when(eventMapper.mapToDto(event)).thenReturn(eventOverview);
        EventOverview actual = eventService.getEvent(anyInt());

        // then
        assertNotNull(actual);
        assertEquals(event.getName(), actual.getName());
    }

    @Test
    public void testGetEventThrowsEventNotFoundException() {
        // given
        int nonExistentEventId = -1;

        // when
        when(eventRepository.findById(nonExistentEventId)).thenReturn(Optional.empty());
        assertThrows(EventNotFoundException.class, () -> eventService.getEvent(nonExistentEventId));

        // then
        verifyNoMoreInteractions(eventMapper);
    }

    @Test
    public void testGetAllEvents() {
        // given
        List<Event> events = List.of(getEvent(), getEvent(), getEvent());
        List<EventOverview> eventOverviews = List.of(getEventOverview(), getEventOverview(), getEventOverview());

        // when
        when(eventRepository.findAll()).thenReturn(events);
        when(eventMapper.mapToDtoList(events)).thenReturn(eventOverviews);
        List<EventOverview> actual = eventService.events();

        // then
        assertNotNull(actual);
        assertEquals(eventOverviews, actual);
    }

    @Test
    public void testGetAllEventsEmptyList() {
        // given
        when(eventRepository.findAll()).thenReturn(Collections.emptyList());
        List<EventOverview> actual = eventService.events();

        // then
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
    }
}