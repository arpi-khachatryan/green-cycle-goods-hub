package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.basket.BasketOverview;
import com.epam.greencyclegoods.entity.Basket;
import com.epam.greencyclegoods.entity.Product;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.BasketNotFoundException;
import com.epam.greencyclegoods.exception.ProductNotFoundException;
import com.epam.greencyclegoods.exception.UserNotFoundException;
import com.epam.greencyclegoods.mapper.BasketMapper;
import com.epam.greencyclegoods.repository.BasketRepository;
import com.epam.greencyclegoods.repository.ProductRepository;
import com.epam.greencyclegoods.service.impl.BasketServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
public class BasketServiceTest {

    @InjectMocks
    BasketServiceImpl basketService;

    @Mock
    BasketRepository basketRepository;

    @Mock
    BasketMapper basketMapper;

    @Mock
    ProductRepository productRepository;

    @Test
    void testBaskets() throws BasketNotFoundException {
        // given
        User user = getUser();
        Pageable pageable = PageRequest.of(0, 10);
        List<Basket> baskets = List.of(getBasket(), getBasket(), getBasket());

        // when
        when(basketRepository.findBasketByUser(user)).thenReturn(baskets);
        Page<BasketOverview> actual = basketService.baskets(pageable, user);

        // then
        assertFalse(actual.isEmpty());
        verify(basketRepository, times(1)).findBasketByUser(user);
    }

    @Test
    void testOwnerBaskets() throws BasketNotFoundException {
        // given
        User user = getUser();
        List<Basket> baskets = List.of(getBasket(), getBasket(), getBasket());
        Pageable pageable = PageRequest.of(0, 10);

        /// when
        when(basketRepository.findBasketsByCompanyOwner(user.getId())).thenReturn(baskets);
        Page<BasketOverview> result = basketService.ownerBaskets(pageable, user);

        // then
        assertFalse(result.isEmpty());
        verify(basketRepository, times(1)).findBasketsByCompanyOwner(user.getId());
    }

    @Test
    void testAllBaskets() throws BasketNotFoundException {
        // given
        List<Basket> baskets = List.of(getBasket(), getBasket(), getBasket());
        Pageable pageable = PageRequest.of(0, 10);

        // when
        when(basketRepository.findAll()).thenReturn(baskets);
        Page<BasketOverview> result = basketService.allBaskets(pageable);

        // then
        assertFalse(result.isEmpty());
        verify(basketRepository, times(1)).findAll();
    }

    @Test
    void testBasketsThrowsBasketNotFoundException() {
        // when
        when(basketRepository.findBasketByUser(getUser())).thenThrow(new RuntimeException());

        // then
        assertThrows(BasketNotFoundException.class, () -> basketService.baskets(getUser()));
    }

    @Test
    void testCalculateTotalBasketPrice() throws BasketNotFoundException {
        // given
        User user = getUser();
        List<Basket> baskets = List.of(getBasket(), getBasket(), getBasket());

        // when
        when(basketRepository.findBasketByUser(user)).thenReturn(baskets);
        double totalPrice = basketService.calculateTotalBasketPrice(user);

        // then
        assertEquals(600.0, totalPrice);
        verify(basketRepository, times(1)).findBasketByUser(user);
    }

    @Test
    void testCalculateTotalBasketPriceThrowsBasketNotFoundException() {
        // given
        User user = getUser();
        List<Basket> emptyBaskets = List.of();

        // when
        when(basketRepository.findBasketByUser(user)).thenReturn(emptyBaskets);
        assertThrows(BasketNotFoundException.class, () -> basketService.calculateTotalBasketPrice(user));

        // then
        verify(basketRepository, times(1)).findBasketByUser(user);
    }

    @Test
    void testAddToBasket() throws UserNotFoundException, ProductNotFoundException {
        // given
        User user = getUser();
        Product product = getProduct();
        Basket existingBasket = getBasket();

        // when
        when(productRepository.findById(1)).thenReturn(Optional.of(product));
        when(basketRepository.findBasketByProductAndUser(product, user)).thenReturn(Optional.of(existingBasket));
        basketService.addToBasket(1, user);

        // then
        verify(productRepository, times(1)).findById(1);
        verify(basketRepository, times(1)).findBasketByProductAndUser(product, user);
        verify(basketRepository, times(1)).save(existingBasket);
    }

    @Test
    void testAddToBasketProductNotFound() {
        // when
        when(productRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(ProductNotFoundException.class, () -> basketService.addToBasket(anyInt(), getUser()));

        // then
        verify(productRepository, times(1)).findById(anyInt());
        verify(basketRepository, never()).findBasketByProductAndUser(any(), any());
        verify(basketRepository, never()).save(any());
    }

    @Test
    void testAddThrowsProductNotFoundException() {
        // given
        User user = getUser();
        Product product = getProduct();
        product.setProductCount(0);

        // when
        when(productRepository.findById(1)).thenReturn(Optional.of(product));
        assertThrows(ProductNotFoundException.class, () -> basketService.addToBasket(1, user));

        // then
        verify(productRepository, times(1)).findById(1);
        verify(basketRepository, never()).findBasketByProductAndUser(any(), any());
        verify(basketRepository, never()).save(any());
    }

    @Test
    void testDelete() throws ProductNotFoundException, BasketNotFoundException {
        // given
        User user = getUser();
        Product product = getProduct();
        Basket basket = getBasket();

        // when
        when(productRepository.findById(1)).thenReturn(Optional.of(product));
        when(basketRepository.findBasketByProductAndUser(product, user)).thenReturn(Optional.of(basket));
        basketRepository.save(basket);
        basketService.delete(1, user);

        // then
        verify(productRepository, times(1)).findById(1);
        verify(basketRepository, times(1)).findBasketByProductAndUser(product, user);
        verify(basketRepository, times(1)).delete(basket);
        verify(basketRepository, times(1)).save(basket);
    }

    @Test
    void testDeleteLastItem() throws ProductNotFoundException, BasketNotFoundException {
        // given
        User user = getUser();
        Product product = getProduct();
        Basket basket = getBasket();

        // when
        when(productRepository.findById(1)).thenReturn(Optional.of(product));
        when(basketRepository.findBasketByProductAndUser(product, user)).thenReturn(Optional.of(basket));
        basketService.delete(1, user);

        // then
        verify(productRepository, times(1)).findById(1);
        verify(basketRepository, times(1)).findBasketByProductAndUser(product, user);
        verify(basketRepository, times(1)).delete(basket);
        verify(basketRepository, never()).save(any(Basket.class));
    }

    @Test
    void testDeleteProductNotFound() {
        // given
        User user = getUser();

        // when
        when(productRepository.findById(1)).thenReturn(Optional.empty());
        assertThrows(ProductNotFoundException.class, () -> basketService.delete(1, user));

        // then
        verify(productRepository, times(1)).findById(1);
        verify(basketRepository, never()).findBasketByProductAndUser(any(), any());
        verify(basketRepository, never()).save(any());
        verify(basketRepository, never()).delete(any(Basket.class));
    }

    @Test
    void testDeleteBasketNotFound() {
        // given
        User user = getUser();
        Product product = getProduct();

        // when
        when(productRepository.findById(1)).thenReturn(Optional.of(product));
        when(basketRepository.findBasketByProductAndUser(product, user)).thenReturn(Optional.empty());
        assertThrows(BasketNotFoundException.class, () -> basketService.delete(1, user));

        // then
        verify(productRepository, times(1)).findById(1);
        verify(basketRepository, times(1)).findBasketByProductAndUser(product, user);
        verify(basketRepository, never()).save(any());
        verify(basketRepository, never()).delete(any(Basket.class));
    }

    @Test
    void testProductsInStockAllProductsOutOfStock() {
        // given
        User user = getUser();
        List<Basket> baskets = List.of(getBasketWithQuantity(), getBasketWithQuantity());

        // when
        when(basketRepository.findBasketByUser(user)).thenReturn(baskets);
        boolean result = basketService.productsInStock(user);

        // then
        assertTrue(result);
    }

    @Test
    void testProductsInStockEmptyBasketList() {
        // given
        User user = getUser();

        // when
        when(basketRepository.findBasketByUser(user)).thenReturn(Collections.emptyList());
        boolean result = basketService.productsInStock(user);

        // then
        assertTrue(result);
    }

    @Test
    void testProductsInStockSomeProductsOutOfStock() {
        // given
        User user = getUser();
        List<Basket> baskets = List.of(getBasket(), getBasketWithQuantity());

        // when
        when(basketRepository.findBasketByUser(user)).thenReturn(baskets);
        boolean result = basketService.productsInStock(user);

        // then
        assertFalse(result);
    }
}
