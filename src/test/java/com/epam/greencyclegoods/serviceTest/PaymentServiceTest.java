package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.dto.payment.EditPaymentDto;
import com.epam.greencyclegoods.dto.payment.PaymentOverview;
import com.epam.greencyclegoods.entity.Order;
import com.epam.greencyclegoods.entity.Payment;
import com.epam.greencyclegoods.entity.PaymentStatus;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.mapper.PaymentMapper;
import com.epam.greencyclegoods.repository.CreditCardRepository;
import com.epam.greencyclegoods.repository.OrderRepository;
import com.epam.greencyclegoods.repository.PaymentRepository;
import com.epam.greencyclegoods.service.impl.CreditCardServiceImpl;
import com.epam.greencyclegoods.service.impl.OrderServiceImpl;
import com.epam.greencyclegoods.service.impl.PaymentServiceImpl;
import com.epam.greencyclegoods.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
public class PaymentServiceTest {

    @InjectMocks
    PaymentServiceImpl paymentService;

    @Mock
    PaymentRepository paymentRepository;

    @Mock
    PaymentMapper paymentMapper;

    @Mock
    CreditCardRepository creditCardRepository;

    @Mock
    OrderServiceImpl orderService;

    @Mock
    UserServiceImpl userService;

    @Mock
    CreditCardServiceImpl creditCardService;

    @Mock
    OrderRepository orderRepository;

    @Test
    void testPayments() {
        // given
        Pageable pageable = PageRequest.of(0, 10);
        Page<Payment> paymentPage = new PageImpl<>(List.of(getPayment(), getPayment(), getPayment()));

        // when
        when(paymentRepository.findAll(pageable)).thenReturn(paymentPage);
        Page<PaymentOverview> result = paymentService.payments(pageable);

        // then
        assertFalse(result.isEmpty());
        verify(paymentRepository, times(1)).findAll(pageable);
    }

    @Test
    void testUserPayments() {
        // given
        int userId = 1;
        Pageable pageable = PageRequest.of(0, 10);
        Page<Payment> paymentPage = new PageImpl<>(List.of(getPayment(), getPayment(), getPayment()));

        // when
        when(paymentRepository.findByUserId(userId, pageable)).thenReturn(paymentPage);
        when(paymentMapper.mapToDto(any(Payment.class))).thenReturn(getPaymentOverview());
        Page<PaymentOverview> result = paymentService.userPayments(userId, pageable);

        // then
        assertFalse(result.isEmpty());
        verify(paymentRepository, times(1)).findByUserId(userId, pageable);
    }

    @Test
    void testOwnerPayments() {
        // given
        int ownerId = 1;
        Pageable pageable = PageRequest.of(0, 10);
        Page<Payment> paymentPage = new PageImpl<>(List.of(getPayment(), getPayment(), getPayment()));

        // when
        when(paymentRepository.findPaymentsByCompanyOwner(ownerId, pageable)).thenReturn(paymentPage);
        when(paymentMapper.mapToDto(any(Payment.class))).thenReturn(getPaymentOverview());
        Page<PaymentOverview> result = paymentService.ownerPayments(ownerId, pageable);

        // then
        assertFalse(result.isEmpty());
        verify(paymentRepository, times(1)).findPaymentsByCompanyOwner(ownerId, pageable);
    }

    @Test
    void testPayment() throws PaymentNotFoundException {
        // given
        Payment payment = getPayment();

        // when
        when(paymentRepository.findById(anyInt())).thenReturn(java.util.Optional.of(payment));
        when(paymentMapper.mapToDto(payment)).thenReturn(new PaymentOverview());
        PaymentOverview result = paymentService.payment(anyInt());

        // then
        assertNotNull(result);
        verify(paymentRepository, times(1)).findById(anyInt());
        verify(paymentMapper, times(1)).mapToDto(payment);
    }

    @Test
    void testPaymentThrowsPaymentNotFoundException() {
        // when
        when(paymentRepository.findById(anyInt())).thenReturn(java.util.Optional.empty());
        assertThrows(PaymentNotFoundException.class, () -> paymentService.payment(anyInt()));

        // then
        verify(paymentRepository, times(1)).findById(anyInt());
        verify(paymentMapper, never()).mapToDto(any(Payment.class));
    }

    @Test
    void testAddPaymentWithCreditCard() throws OrderNotFoundException, UserNotFoundException, CreditCardNotFoundException {
        // given
        Order order = getOrder();
        CreateCreditCardDto createCreditCardDto = getCreateCreditCardDto();
        User user = getUser();

        // when
        when(orderService.order(anyInt())).thenReturn(getOrderOverview());
        when(userService.user(anyInt())).thenReturn(getUserOverview());
        userService.user(user.getId());
        orderService.order(order.getId());
        paymentService.addPayment(order, createCreditCardDto, user);

        // then
        verify(orderService, times(1)).order(anyInt());
        verify(userService, times(1)).user(anyInt());
        verify(paymentRepository, times(1)).save(any(Payment.class));
    }

    @Test
    void testAddPaymentUserNotFound() throws OrderNotFoundException, UserNotFoundException, CreditCardNotFoundException {
        // given
        CreateCreditCardDto cardDto = new CreateCreditCardDto();

        // when
        when(orderService.order(anyInt())).thenReturn(getOrderOverview());
        when(userService.user(anyInt())).thenThrow(UserNotFoundException.class);
        assertThrows(UserNotFoundException.class, () -> {
            orderService.order(1);
            userService.user(1);
            paymentService.addPayment(getOrder(), cardDto, null);
        });

        // then
        verify(orderService, times(1)).order(1);
        verify(userService, times(1)).user(anyInt());
        verify(creditCardRepository, never()).existsByCardNumber(anyString());
        verify(creditCardService, never()).addCreditCard(any(CreateCreditCardDto.class), any(User.class));
        verify(paymentRepository, never()).save(any(Payment.class));
    }

    @Test
    void testEditPaymentStatus() throws InvalidDateFormatException, PaymentNotFoundException {
        // given
        EditPaymentDto dto = getEditPaymentDto();
        Payment payment = getPayment();
        Order order = getOrder();

        // when
        when(paymentRepository.findById(anyInt())).thenReturn(Optional.of(payment));
        when(orderRepository.save(any(Order.class))).thenReturn(order);
        orderRepository.save(order);
        paymentService.edit(dto, anyInt());

        // then
        assertEquals(PaymentStatus.PROCESSING, payment.getStatus());
        assertTrue(order.isPaymentCompleted());
        verify(paymentRepository, times(1)).save(payment);
        verify(orderRepository, times(1)).save(order);
    }

    @Test
    void testEditPaymentDate() throws PaymentNotFoundException, InvalidDateFormatException {
        // given
        EditPaymentDto dto = getEditPaymentDto();
        Payment payment = getPayment();

        // when
        when(paymentRepository.findById(anyInt())).thenReturn(Optional.of(payment));
        paymentService.edit(dto, anyInt());

        // Assert
        assertEquals(LocalDateTime.parse("2023-11-10T12:00:00"), payment.getPaymentDate());
        verify(paymentRepository, times(1)).save(payment);
    }

    @Test
    void testEditInvalidDateFormat() {
        // given
        EditPaymentDto dto = getEditPaymentDto();
        dto.setPaymentDate("invalid_date");
        Payment payment = getPayment();

        // when
        when(paymentRepository.findById(anyInt())).thenReturn(Optional.of(payment));

        // then
        assertThrows(InvalidDateFormatException.class, () -> paymentService.edit(dto, anyInt()));
        verify(paymentRepository, never()).save(payment);
    }

    @Test
    void testDelete() throws PaymentNotFoundException, PaymentDeleteException {
        // given
        Payment payment = getPayment();
        payment.setStatus(PaymentStatus.PAID);

        // when
        when(paymentRepository.findById(anyInt())).thenReturn(Optional.of(payment));
        paymentService.delete(anyInt());

        // then
        verify(paymentRepository, times(1)).deleteById(anyInt());
    }

    @Test
    void testDeleteThrowsPaymentDeleteException() {
        // given
        Payment payment = getPayment();
        payment.setStatus(PaymentStatus.UNPAID);

        // when
        when(paymentRepository.findById(anyInt())).thenReturn(Optional.of(payment));
        assertThrows(PaymentDeleteException.class, () -> paymentService.delete(anyInt()));

        // then
        verify(paymentRepository, never()).deleteById(anyInt());
    }

    @Test
    void testDeleteThrowsPaymentNotFoundException() {
        // when
        when(paymentRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(PaymentNotFoundException.class, () -> paymentService.delete(anyInt()));

        // then
        verify(paymentRepository, never()).deleteById(anyInt());
    }
}
