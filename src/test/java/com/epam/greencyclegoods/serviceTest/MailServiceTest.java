package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.service.impl.MailServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
public class MailServiceTest {

    @InjectMocks
    MailServiceImpl mailService;

    @Mock
    private JavaMailSender javaMailSender;

    @Test
    void testSendEmailSuccessfully() throws MessagingException {
        // given
        String to = "email@example.com";
        String subject = "Subject";
        String text = "Text";
        MimeMessage mimeMessage = mock(MimeMessage.class);

        // when
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);
        doNothing().when(javaMailSender).send(any(MimeMessage.class));
        assertDoesNotThrow(() -> mailService.sendEmail(to, subject, text));

        // then
        verify(javaMailSender, times(1)).createMimeMessage();
        verify(javaMailSender, times(1)).send(eq(mimeMessage));
        verify(mimeMessage, times(1)).setRecipient(any(Message.RecipientType.class), any(Address.class));
    }
}
