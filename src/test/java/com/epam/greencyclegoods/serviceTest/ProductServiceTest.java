package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.product.CreateProductDto;
import com.epam.greencyclegoods.dto.product.EditProductDto;
import com.epam.greencyclegoods.dto.product.ProductOverview;
import com.epam.greencyclegoods.entity.Product;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.ImageUploadException;
import com.epam.greencyclegoods.exception.PermissionDeniedException;
import com.epam.greencyclegoods.exception.ProductCreationException;
import com.epam.greencyclegoods.exception.ProductNotFoundException;
import com.epam.greencyclegoods.mapper.ProductMapper;
import com.epam.greencyclegoods.repository.*;
import com.epam.greencyclegoods.service.impl.ProductServiceImpl;
import com.epam.greencyclegoods.utility.ImageUtility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @InjectMocks
    ProductServiceImpl productService;

    @Mock
    ProductMapper productMapper;

    @Mock
    ProductRepository productRepository;

    @Mock
    CompanyRepository companyRepository;

    @Mock
    ProductCategoryRepository productCategoryRepository;

    @Mock
    ImageUtility imageUtility;

    @Mock
    BasketRepository basketRepository;

    @Mock
    OrderRepository orderRepository;

    @Test
    public void testSortProducts() {
        // given
        Pageable pageable = PageRequest.of(0, 10);
        Integer categoryId = 1;
        List<Product> mockProducts = List.of(getProduct(), getProduct(), getProduct());
        List<ProductOverview> mockProductOverviews = List.of(getProductOverview(), getProductOverview(), getProductOverview());

        // when
        when(productRepository.findProductsByProductCategory_Id(eq(categoryId), eq(pageable))).thenReturn(new PageImpl<>(mockProducts));
        when(productRepository.findByOrderByPriceAsc(eq(pageable))).thenReturn(new PageImpl<>(mockProducts));
        when(productRepository.findByOrderByPriceDesc(eq(pageable))).thenReturn(new PageImpl<>(mockProducts));
        when(productRepository.findAll(eq(pageable))).thenReturn(new PageImpl<>(mockProducts));
        when(productMapper.mapToDto(any(Product.class)))
                .thenAnswer(invocation -> {
                    Product product = invocation.getArgument(0);
                    return getProductOverview();
                });

        Page<ProductOverview> resultWithCategoryId = productService.sort(pageable, null, categoryId);
        Page<ProductOverview> resultPriceAsc = productService.sort(pageable, "price_asc", null);
        Page<ProductOverview> resultPriceDesc = productService.sort(pageable, "price_desc", null);
        Page<ProductOverview> resultDefault = productService.sort(pageable, "invalid_sort", null);

        // then
        assertEquals(mockProductOverviews, resultWithCategoryId.getContent());
        verify(productRepository, times(1)).findProductsByProductCategory_Id(eq(categoryId), eq(pageable));
        assertEquals(mockProductOverviews, resultPriceAsc.getContent());
        verify(productRepository, times(1)).findByOrderByPriceAsc(eq(pageable));
        assertEquals(mockProductOverviews, resultPriceDesc.getContent());
        verify(productRepository, times(1)).findByOrderByPriceDesc(eq(pageable));
        assertEquals(mockProductOverviews, resultDefault.getContent());
        verify(productRepository, times(1)).findAll(eq(pageable));
    }

    @Test
    void testSortWithInvalidSortParameter() {
        // given
        Pageable pageable = PageRequest.of(0, 10);

        // when
        when(productRepository.findAll(pageable)).thenReturn(Page.empty());
        Page<ProductOverview> actual = productService.sort(pageable, "invalid_sort", null);

        // then
        assertTrue(actual.isEmpty());
    }

    @Test
    void testProducts() {
        // given
        List<Product> mockProducts = List.of(getProduct(), getProduct(), getProduct());
        List<ProductOverview> productOverviews = List.of(getProductOverview(), getProductOverview(), getProductOverview());

        // when
        when(productRepository.findAll()).thenReturn(mockProducts);
        when(productMapper.mapToDtoList(mockProducts)).thenReturn(productOverviews);
        List<ProductOverview> result = productService.products();

        // then
        assertEquals(productOverviews, result);
    }

    @Test
    void testProductsWhenEmpty() {
        // when
        when(productRepository.findAll()).thenReturn(Collections.emptyList());
        List<ProductOverview> actual = productService.products();

        // then
        assertEquals(Collections.emptyList(), actual);
    }

    @Test
    void testUserProducts() {
        // given
        User mockUser = getUser();
        List<Product> products = List.of(getProduct(), getProduct(), getProduct());
        List<ProductOverview> productOverviews = List.of(getProductOverview(), getProductOverview(), getProductOverview());

        // when
        when(productRepository.findProductByUser(mockUser)).thenReturn(products);
        when(productMapper.mapToDtoList(products)).thenReturn(productOverviews);
        List<ProductOverview> actual = productService.userProducts(mockUser);

        // then
        assertEquals(productOverviews, actual);
    }

    @Test
    void testUserProductsWhenEmpty() {
        // given
        User mockUser = getUser();

        // when
        when(productRepository.findProductByUser(mockUser)).thenReturn(Collections.emptyList());
        List<ProductOverview> actual = productService.userProducts(mockUser);

        // then
        assertEquals(Collections.emptyList(), actual);
    }

    @Test
    public void testSave() throws IOException, ProductCreationException, ImageUploadException {
        // given
        CreateProductDto createProductDto = getCreateProductDto();
        MultipartFile[] mockFiles = {mock(MultipartFile.class), mock(MultipartFile.class)};
        List<String> mockImages = List.of("image.jpg", "image.jpg");
        User user = getUser();

        // when
        when(productMapper.mapToEntity(createProductDto)).thenReturn(getProduct());
        when(imageUtility.uploadImages(mockFiles)).thenReturn(mockImages);
        productService.save(createProductDto, mockFiles, user);

        // then
        verify(productMapper, times(1)).mapToEntity(createProductDto);
        verify(imageUtility, times(1)).uploadImages(mockFiles);
        verify(productRepository, times(1)).save(any(Product.class));
    }

    @Test
    public void testSaveProductWithException() throws IOException {
        // given
        CreateProductDto createProductDto = getCreateProductDto();
        MultipartFile[] mockFiles = {mock(MultipartFile.class), mock(MultipartFile.class)};
        User user = getUser();

        // when
        when(productMapper.mapToEntity(createProductDto)).thenReturn(getProduct());
        when(imageUtility.uploadImages(mockFiles)).thenThrow(IOException.class);
        assertThrows(ProductCreationException.class, () -> productService.save(createProductDto, mockFiles, user));

        // then
        verify(productMapper, times(1)).mapToEntity(createProductDto);
        verify(imageUtility, times(1)).uploadImages(mockFiles);
        verifyNoInteractions(productRepository);
    }

    @Test
    void testEditProduct() throws ProductNotFoundException, ImageUploadException, PermissionDeniedException {
        // given
        int productId = 1;
        EditProductDto editProductDto = getEditProductDto();
        MultipartFile[] mockFiles = {mock(MockMultipartFile.class)};
        Optional<Product> productOptional = Optional.of(getProduct());

        // when
        when(productRepository.findById(productId)).thenReturn(productOptional);
        when(companyRepository.findById(editProductDto.getCompanyId())).thenReturn(Optional.of(getCompany()));
        when(productCategoryRepository.findById(editProductDto.getProductCategoryId())).thenReturn(Optional.of(getProductCategory()));
        when(basketRepository.minProductCountInAllBaskets(productId)).thenReturn(0);
        when(orderRepository.existsByProduct(productId)).thenReturn(false);
        when(basketRepository.existsByProduct(productId)).thenReturn(false);
        productService.edit(editProductDto, productId, mockFiles);

        // then
        verify(productRepository, times(1)).findById(productId);
        verify(productRepository, times(2)).save(any());
        verify(companyRepository, times(1)).findById(editProductDto.getCompanyId());
        verify(productCategoryRepository, times(1)).findById(editProductDto.getProductCategoryId());
        verify(basketRepository, times(1)).minProductCountInAllBaskets(productId);
        verify(orderRepository, times(1)).existsByProduct(productId);
        verify(basketRepository, times(1)).existsByProduct(productId);
    }

    @Test
    void testProductById() throws ProductNotFoundException {
        // given
        Product product = getProduct();

        //when
        when(productRepository.findById(anyInt())).thenReturn(Optional.of(product));
        when(productMapper.mapToDto(product)).thenReturn(getProductOverview());
        ProductOverview actual = productService.productById(anyInt());

        // then
        assertNotNull(actual);
        assertEquals(product.getName(), actual.getName());
        verify(productRepository, times(1)).findById(anyInt());
    }

    @Test
    void testProductThrowsProductNotFoundException() {
        // when
        when(productRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(ProductNotFoundException.class, () -> productService.productById(anyInt()));

        // then
        verify(productRepository, times(1)).findById(anyInt());
    }

    @Test
    void testCompanyProducts() {
        // given
        List<Product> products = Collections.singletonList(getProduct());

        // when
        when(productRepository.findProductsByCompany_Id(anyInt())).thenReturn(products);
        when(productMapper.mapToDtoList(products)).thenReturn(Collections.singletonList(getProductOverview()));
        List<ProductOverview> result = productService.companyProducts(anyInt());

        // then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        verify(productRepository, times(1)).findProductsByCompany_Id(anyInt());
    }

    @Test
    void testCompanyProductsEmpty() {
        // when
        when(productRepository.findProductsByCompany_Id(anyInt())).thenReturn(Collections.emptyList());
        List<ProductOverview> result = productService.companyProducts(anyInt());

        // then
        assertNotNull(result);
        assertTrue(result.isEmpty());
        verify(productRepository, times(1)).findProductsByCompany_Id(anyInt());
    }

    @Test
    void testDeleteProductAsAdmin() {
        // given
        User adminUser = mock(User.class);
        Product product = getProductForAdmin();

        // when
        when(productRepository.findById(anyInt())).thenReturn(Optional.of(product));
        when(adminUser.getRole()).thenReturn(Role.ADMIN);
        assertDoesNotThrow(() -> productService.delete(anyInt(), adminUser));

        // then
        verify(productRepository, times(1)).findById(anyInt());
        verify(productRepository, times(1)).deleteById(anyInt());
        verify(orderRepository, times(1)).existsByProduct(anyInt());
        verify(basketRepository, times(1)).existsByProduct(anyInt());
    }

    @Test
    void testDeleteProductAsProductOwner() {
        // given
        User productOwner = mock(User.class);
        Product product = getProductForOwner();

        // when
        when(productRepository.findById(anyInt())).thenReturn(Optional.of(product));
        when(productOwner.getRole()).thenReturn(Role.OWNER);
        assertThrows(PermissionDeniedException.class, () -> productService.delete(anyInt(), productOwner));

        // then
        verify(productRepository, times(1)).findById(anyInt());
        verify(productRepository, never()).deleteById(anyInt());
        verify(orderRepository, never()).existsByProduct(anyInt());
        verify(basketRepository, never()).existsByProduct(anyInt());
    }

    @Test
    void testDeleteProductNotFound() {
        // when
        when(productRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(ProductNotFoundException.class, () -> productService.delete(anyInt(), getUser()));

        // then
        verify(productRepository, times(1)).findById(anyInt());
        verify(productRepository, never()).deleteById(anyInt());
        verify(orderRepository, never()).existsByProduct(anyInt());
        verify(basketRepository, never()).existsByProduct(anyInt());
    }
}
