package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.user.ChangePasswordDto;
import com.epam.greencyclegoods.dto.user.CreateUserDto;
import com.epam.greencyclegoods.dto.user.EditUserDto;
import com.epam.greencyclegoods.dto.user.UserOverview;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.entity.VerificationToken;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.mapper.UserMapper;
import com.epam.greencyclegoods.repository.UserRepository;
import com.epam.greencyclegoods.repository.VerificationTokenRepository;
import com.epam.greencyclegoods.service.VerificationTokenService;
import com.epam.greencyclegoods.service.impl.MailServiceImpl;
import com.epam.greencyclegoods.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    VerificationTokenRepository tokenRepository;

    @Mock
    private VerificationTokenService tokenService;

    @Mock
    private MailServiceImpl mailService;

    @Test
    void testUsersWhenUsersExist() {
        // given
        var listOfUsers = List.of(getUser(), getUser(), getUser());
        var listOfUserOverviews = List.of(getUserOverview(), getUserOverview(), getUserOverview());

        // when
        doReturn(listOfUsers).when(userRepository).findAll();
        doReturn(listOfUserOverviews).when(userMapper).mapToDtoList(listOfUsers);
        Page<UserOverview> actual = userService.users(Pageable.unpaged());

        // then
        assertFalse(actual.isEmpty());
        assertNotNull(actual);
        verify(userRepository).findAll();
        verify(userMapper).mapToDtoList(listOfUsers);
    }

    @Test
    void testUsersWhenNoUsersExist() {
        // when
        when(userRepository.findAll()).thenReturn(Collections.emptyList());
        Page<UserOverview> result = userService.users(Pageable.unpaged());

        // then
        assertTrue(result.isEmpty());
        verify(userRepository).findAll();
        verify(userMapper, never()).mapToDtoList(anyList());
    }

    @Test
    void testUserWhenUserExists() throws UserNotFoundException {
        // given
        User existingUser = getUser();
        UserOverview expectedUserOverview = getUserOverview();

        // when
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(existingUser));
        when(userMapper.mapToDto(existingUser)).thenReturn(expectedUserOverview);
        UserOverview actual = userService.user(anyInt());

        // then
        assertNotNull(actual);
        assertEquals(expectedUserOverview, actual);
        verify(userRepository).findById(anyInt());
        verify(userMapper).mapToDto(existingUser);
    }

    @Test
    void testUserWhenUserNotExists() {
        // when
        when(userRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () -> userService.user(anyInt()));

        // then
        verify(userRepository).findById(anyInt());
        verify(userMapper, never()).mapToDto(any());
    }

    @Test
    void testSave() throws AuthenticationException, EmailSendingException {
        // given
        String encodedPassword = "encodedPassword";
        CreateUserDto dto = getCreateUserDto();
        User mappedUser = getUser();
        VerificationToken mockToken = getVerificationToken();

        // when
        when(userRepository.existsByEmailIgnoreCase(dto.getEmail())).thenReturn(false);
        when(userMapper.mapToEntity(dto)).thenReturn(mappedUser);
        when(passwordEncoder.encode(mappedUser.getPassword())).thenReturn(encodedPassword);
        when(tokenService.create(mappedUser)).thenReturn(mockToken);
        userService.save(dto);

        // then
        verify(userRepository).save(mappedUser);
        verify(mailService).sendEmail(eq(dto.getEmail()), anyString(), anyString());
        verify(tokenService).create(mappedUser);
    }

    @Test
    void testVerify() throws VerificationException {
        // given
        String plainToken = "sampleToken";
        User mockUser = getUserForTokenWithEnabled();
        VerificationToken mockToken = getVerificationTokenWithExpiry();

        // when
        when(tokenService.find(anyString())).thenReturn(mockToken);
        tokenRepository.delete(mockToken);
        userRepository.save(mockUser);
        userService.verify(plainToken);

        //then
        verify(tokenRepository, times(1)).delete(mockToken);
        verify(userRepository).save(mockUser);
    }

    @Test
    void verifyTokenThrowExceptionWhenUserIsNull() throws VerificationException {
        // given
        VerificationToken token = getTokenWithoutUser();

        // when
        doReturn(token).when(tokenService).find(anyString());
        assertNull(token.getUser());
        tokenRepository.delete(token);

        // then
        assertThrows(VerificationException.class, () -> userService.verify(anyString()));
    }

    @Test
    void verifyTokenThrowException() throws VerificationException {
        // given
        User user = getUser();
        VerificationToken token = getVerificationToken();

        // when
        doReturn(token).when(tokenService).find(anyString());
        tokenService.delete(token);
        assertThat(String.valueOf(user.isEnabled()), true);

        // then
        assertThrows(VerificationException.class, () -> userService.verify(anyString()));
    }

    @Test
    void testEditUser() throws UserNotFoundException {
        // given
        User existingUser = getUser();
        EditUserDto editUserDto = getEditUserDto();

        // when
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(existingUser));
        doReturn(existingUser).when(userRepository).save(any(User.class));
        userService.edit(editUserDto, existingUser.getId());

        //then
        verify(userRepository, times(1)).save(existingUser);
    }

    @Test
    void testChangePassword() throws UserNotFoundException, PasswordChangeException {
        // given
        ChangePasswordDto changePasswordDto = getChangePasswordDto();
        User existingUser = getUser();

        // when
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(existingUser));
        when(passwordEncoder.matches(changePasswordDto.getOldPassword(), existingUser.getPassword())).thenReturn(true);
        when(passwordEncoder.matches(changePasswordDto.getNewPassword1(), existingUser.getPassword())).thenReturn(false);
        userService.changePassword(anyInt(), changePasswordDto);

        // then
        verify(passwordEncoder, times(1)).encode(changePasswordDto.getNewPassword1());
        verify(userRepository, times(1)).save(existingUser);
    }

    @Test
    void changePasswordThrowsUserNotFoundException() {
        // given
        ChangePasswordDto changePasswordDto = getChangePasswordDto();

        // when
        when(userRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () -> userService.changePassword(anyInt(), changePasswordDto));

        // then
        verify(userRepository, times(1)).findById(anyInt());
        verifyNoMoreInteractions(passwordEncoder, userRepository);
    }

    @Test
    void changePasswordThrowsPasswordChangeExceptionOldPassword() {
        // given
        ChangePasswordDto changePasswordDto = getChangePasswordDto();
        User user = getUser();

        // when
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);
        assertThrows(PasswordChangeException.class, () -> userService.changePassword(anyInt(), changePasswordDto));

        // then
        verify(userRepository, times(1)).findById(anyInt());
        verify(passwordEncoder, times(1)).matches(changePasswordDto.getOldPassword(), user.getPassword());
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    void changePasswordThrowsPasswordChangeExceptionPasswordMismatch() {
        // given
        ChangePasswordDto changePasswordDto = getChangePasswordDto();
        User user = getUser();

        // when
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        assertThrows(PasswordChangeException.class, () -> userService.changePassword(anyInt(), changePasswordDto));

        // then
        verify(userRepository, times(1)).findById(anyInt());
        verify(passwordEncoder, times(1)).matches(changePasswordDto.getNewPassword1(), user.getPassword());
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    void changePasswordThrowsPasswordChangeExceptionPasswordNotEquals() {
        // given
        ChangePasswordDto changePasswordDto = getNonMatchingChangePasswordDto();
        User user = getUser();

        // when
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        assertNotEquals(changePasswordDto.getNewPassword1(), changePasswordDto.getNewPassword2());
        assertThrows(PasswordChangeException.class, () -> userService.changePassword(anyInt(), changePasswordDto));

        // then
        verify(userRepository, times(1)).findById(anyInt());
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    void deleteUserById() {
        // when
        when(userRepository.existsById(anyInt())).thenReturn(true);
        doNothing().when(userRepository).deleteById(anyInt());
        assertDoesNotThrow(() -> userService.delete(anyInt()));

        // then
        verify(userRepository, times(1)).existsById(anyInt());
        verify(userRepository, times(1)).deleteById(anyInt());
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    void deleteUserByIdThrowsUserNotFoundExceptionWhenUserNotExists() {
        // when
        when(userRepository.existsById(anyInt())).thenReturn(false);
        assertThrows(UserNotFoundException.class, () -> userService.delete(anyInt()));

        // then
        verify(userRepository, times(1)).existsById(anyInt());
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    void deleteUserByIdThrowsUserNotFoundException() {
        // when
        when(userRepository.existsById(anyInt())).thenReturn(true);
        doThrow(new RuntimeException()).when(userRepository).deleteById(anyInt());
        assertThrows(UserNotFoundException.class, () -> userService.delete(anyInt()));

        // then
        verify(userRepository, times(1)).existsById(anyInt());
        verify(userRepository, times(1)).deleteById(anyInt());
        verifyNoMoreInteractions(userRepository);
    }
}