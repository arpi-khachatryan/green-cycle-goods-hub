package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.companyCategory.CompanyCategoryOverview;
import com.epam.greencyclegoods.dto.companyCategory.CreateCompanyCategoryDto;
import com.epam.greencyclegoods.entity.CompanyCategory;
import com.epam.greencyclegoods.exception.CategoryCreationException;
import com.epam.greencyclegoods.exception.CategoryDeleteException;
import com.epam.greencyclegoods.exception.CategoryNotFoundException;
import com.epam.greencyclegoods.mapper.CompanyCategoryMapper;
import com.epam.greencyclegoods.repository.CompanyCategoryRepository;
import com.epam.greencyclegoods.repository.CompanyRepository;
import com.epam.greencyclegoods.service.impl.CompanyCategoryServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.List;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.getCompanyCategory;
import static com.epam.greencyclegoods.utility.MockDataTestUtil.getCreateCompanyCategoryDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
public class CompanyCategoryServiceTest {

    @InjectMocks
    CompanyCategoryServiceImpl companyCategoryService;

    @Mock
    CompanyCategoryMapper categoryMapper;

    @Mock
    CompanyCategoryRepository companyCategoryRepository;

    @Mock
    CompanyRepository companyRepository;

    @Test
    void testCategoriesPageable() {
        // given
        PageRequest pageable = PageRequest.of(1, 1, Sort.Direction.fromString("DESC"), "name");
        List<CompanyCategory> companyCategories = List.of(getCompanyCategory(), getCompanyCategory(), getCompanyCategory());
        Page<CompanyCategoryOverview> expected = new PageImpl<>(categoryMapper.mapToDtoList(companyCategories));

        // when
        when(companyCategoryRepository.findAll()).thenReturn(companyCategories);
        Page<CompanyCategoryOverview> actual = companyCategoryService.categories(pageable);

        // then
        assertEquals(expected, actual);
        verify(companyCategoryRepository, times(1)).findAll();
    }

    @Test
    void testCategories() throws CategoryNotFoundException {
        // given
        List<CompanyCategory> companyCategories = List.of(getCompanyCategory(), getCompanyCategory(), getCompanyCategory());

        // when
        when(companyCategoryRepository.findAll()).thenReturn(companyCategories);
        List<CompanyCategoryOverview> resultCategories = companyCategoryService.categories();

        // then
        assertEquals(categoryMapper.mapToDtoList(companyCategories), resultCategories);
        verify(companyCategoryRepository, times(1)).findAll();
    }

    @Test
    void testAddCategory() throws CategoryCreationException {
        // given
        CompanyCategory companyCategory = getCompanyCategory();
        CreateCompanyCategoryDto createCategoryDto = getCreateCompanyCategoryDto();

        // when
        when(companyCategoryRepository.existsByName(createCategoryDto.getName())).thenReturn(false);
        doReturn(companyCategory).when(categoryMapper).mapToEntity(createCategoryDto);
        doReturn(companyCategory).when(companyCategoryRepository).save(any(CompanyCategory.class));
        companyCategoryService.add(createCategoryDto);

        // then
        verify(companyCategoryRepository, times(1)).existsByName(createCategoryDto.getName());
        verify(companyCategoryRepository, times(1)).save(any());
    }

    @Test
    void addCategoryThrowsCategoryCreationException() {
        //given
        CreateCompanyCategoryDto createCompanyCategory = getCreateCompanyCategoryDto();

        //when
        doReturn(true).when(companyCategoryRepository).existsByName(anyString());

        //then
        assertThrows(CategoryCreationException.class, () -> companyCategoryService.add(createCompanyCategory));
    }

    @Test
    void testDeleteCategory() throws CategoryNotFoundException, CategoryDeleteException {
        // when
        when(companyCategoryRepository.existsById(anyInt())).thenReturn(true);
        when(companyRepository.existsByCompanyCategoryId(anyInt())).thenReturn(false);
        companyCategoryService.delete(anyInt());

        // then
        verify(companyCategoryRepository, times(1)).existsById(anyInt());
        verify(companyRepository, times(1)).existsByCompanyCategoryId(anyInt());
        verify(companyCategoryRepository, times(1)).deleteById(anyInt());
    }
}
