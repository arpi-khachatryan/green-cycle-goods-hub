package com.epam.greencyclegoods.serviceTest;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.entity.CreditCard;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.CreditCardNotFoundException;
import com.epam.greencyclegoods.exception.UserNotFoundException;
import com.epam.greencyclegoods.mapper.CreditCardMapper;
import com.epam.greencyclegoods.repository.CreditCardRepository;
import com.epam.greencyclegoods.service.impl.CreditCardServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.epam.greencyclegoods.utility.MockDataTestUtil.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * Date: 26.11.2023
 */

@ExtendWith(MockitoExtension.class)
public class CreditCardServiceTest {

    @InjectMocks
    CreditCardServiceImpl creditCardService;

    @Mock
    CreditCardRepository creditCardRepository;

    @Mock
    CreditCardMapper creditCardMapper;

    @Test
    void testAddCreditCard() throws CreditCardNotFoundException, UserNotFoundException {
        // given
        CreateCreditCardDto cardDto = getCreateCreditCardDto();
        User user = getUser();

        // when
        when(creditCardMapper.mapToEntity(cardDto)).thenReturn(getCreditCard());
        creditCardService.addCreditCard(cardDto, user);

        // then
        verify(creditCardMapper, times(1)).mapToEntity(cardDto);
        verify(creditCardRepository, times(1)).save(any(CreditCard.class));
    }

    @Test
    void testAddCreditCardThrowsUserNotFoundException() {
        // given
        CreateCreditCardDto cardDto = getCreateCreditCardDto();

        // when
        assertThrows(UserNotFoundException.class, () -> creditCardService.addCreditCard(cardDto, null));

        // then
        verify(creditCardMapper, never()).mapToEntity(cardDto);
        verify(creditCardRepository, never()).save(any());
    }
}
