package com.epam.greencyclegoods.authn;

import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Loading user by username: {}", username);
        Optional<User> userByEmail = userRepository.findByEmail(username);
        if (userByEmail.isEmpty()) {
            log.error("User with username '{}' not found", username);
            throw new UsernameNotFoundException("User with username '" + username + "' does not exist");
        }
        log.info("User loaded: {}", userByEmail.get().getEmail());
        return new CurrentUser(userByEmail.get());
    }
}