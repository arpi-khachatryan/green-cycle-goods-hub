package com.epam.greencyclegoods.authn;

import com.epam.greencyclegoods.entity.User;
import org.springframework.security.core.authority.AuthorityUtils;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private final User user;

    public CurrentUser(User user) {
        super(user.getEmail(), user.getPassword(), user.isEnabled(), true, true, true, AuthorityUtils.createAuthorityList(user.getRole().name()));
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}