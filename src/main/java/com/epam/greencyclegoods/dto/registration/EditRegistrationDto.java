package com.epam.greencyclegoods.dto.registration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EditRegistrationDto {
    @NotNull(message = "People count is mandatory.")
    @Min(value = 1, message = "People count should be greater than or equal to 1.")
    private int peopleCount;

    @NotNull(message = "Status is mandatory.")
    private String status;
}
