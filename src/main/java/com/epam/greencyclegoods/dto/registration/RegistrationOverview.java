package com.epam.greencyclegoods.dto.registration;

import com.epam.greencyclegoods.dto.event.EventOverview;
import com.epam.greencyclegoods.dto.user.UserOverview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationOverview {
    private Integer id;
    private int peopleCount;
    private String status;
    private EventOverview eventOverview;
    private UserOverview userOverview;
}
