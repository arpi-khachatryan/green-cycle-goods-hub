package com.epam.greencyclegoods.dto.basket;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateBasketDto {
    private double itemQuantity;

    @NotNull(message = "Product is mandatory.")
    private Integer productId;
}
