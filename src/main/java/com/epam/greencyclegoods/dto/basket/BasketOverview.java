package com.epam.greencyclegoods.dto.basket;

import com.epam.greencyclegoods.dto.product.ProductOverview;
import com.epam.greencyclegoods.dto.user.UserOverview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasketOverview {
    private Integer id;
    private double itemQuantity;
    private ProductOverview productOverview;
    private UserOverview userOverview;
}
