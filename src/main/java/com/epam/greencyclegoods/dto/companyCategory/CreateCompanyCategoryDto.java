package com.epam.greencyclegoods.dto.companyCategory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCompanyCategoryDto {
    @NotBlank(message = "Name is mandatory.")
    @Size(min = 2, max = 30, message = "Name must be between 2 and 30 characters.")
    private String name;
}
