package com.epam.greencyclegoods.dto.companyCategory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompanyCategoryOverview {
    private Integer id;
    private String name;
}
