package com.epam.greencyclegoods.dto.payment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EditPaymentDto {
    @NotBlank(message = "Date is mandatory.")
    private String paymentDate;

    @NotBlank(message = "Status is mandatory.")
    private String status;
}
