package com.epam.greencyclegoods.dto.payment;

import com.epam.greencyclegoods.dto.order.OrderOverview;
import com.epam.greencyclegoods.dto.user.UserOverview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentOverview {
    private int id;
    private double totalAmount;
    private LocalDateTime paymentDate;
    private LocalDateTime paymentCreateDate;
    private String status;
    private OrderOverview orderOverview;
    private UserOverview userOverview;
}