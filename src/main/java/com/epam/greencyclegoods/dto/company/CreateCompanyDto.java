package com.epam.greencyclegoods.dto.company;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCompanyDto {
    @NotBlank(message = "Name is mandatory.")
    @Size(min = 2, max = 70, message = "Name must be between 2 and 70 characters.")
    private String name;

    private String address;

    @Email
    @NotBlank(message = "Email is mandatory.")
    private String email;

    @NotBlank(message = "Phone is mandatory.")
    @Pattern(regexp = "^[+][0-9]{11}$", message = "Invalid phone number format.")
    @Size(min = 12, max = 12, message = "Phone number should start with '+' and have 11 digits.")
    private String phone;

    @NotNull(message = "Category is mandatory.")
    private Integer companyCategoryId;

    @NotNull(message = "Price is mandatory.")
    @PositiveOrZero(message = "Price must be greater than or equal to zero.")
    private Double deliveryPrice;

    private List<String> pictures;
}
