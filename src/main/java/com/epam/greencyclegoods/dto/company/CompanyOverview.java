package com.epam.greencyclegoods.dto.company;

import com.epam.greencyclegoods.dto.companyCategory.CompanyCategoryOverview;
import com.epam.greencyclegoods.dto.user.UserOverview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompanyOverview {
    private Integer id;
    private String name;
    private String address;
    private String email;
    private String phone;
    private CompanyCategoryOverview companyCategoryOverview;
    private Double deliveryPrice;
    private List<String> pictures;
    private UserOverview userOverview;
}
