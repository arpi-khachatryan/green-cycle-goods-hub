package com.epam.greencyclegoods.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductDto {
    @NotBlank(message = "Name is mandatory.")
    @Size(min = 2, max = 70, message = "The name should be between 2 and 70 characters.")
    private String name;

    @NotBlank(message = "Description is mandatory.")
    @Size(min = 10, max = 300, message = "Description length must be between 10 and 300 characters.")
    private String description;

    @NotNull(message = "Price is mandatory.")
    @Positive(message = "Price must be a positive value.")
    private Double price;

    @NotNull(message = "Product Category is mandatory.")
    private Integer productCategoryId;

    private List<String> pictures;

    @NotNull(message = "Company is mandatory.")
    private Integer companyId;

    @NotNull(message = "Product count is mandatory.")
    @Positive(message = "Product count must be a positive value.")
    private int productCount;
}

