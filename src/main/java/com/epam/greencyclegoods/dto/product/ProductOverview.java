package com.epam.greencyclegoods.dto.product;

import com.epam.greencyclegoods.dto.company.CompanyOverview;
import com.epam.greencyclegoods.dto.productCategory.ProductCategoryOverview;
import com.epam.greencyclegoods.dto.user.UserOverview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductOverview {
    private int id;
    private String name;
    private String description;
    private Double price;
    private ProductCategoryOverview productCategoryOverview;
    private List<String> pictures;
    private UserOverview userOverview;
    private CompanyOverview companyOverview;
    private int productCount;
}