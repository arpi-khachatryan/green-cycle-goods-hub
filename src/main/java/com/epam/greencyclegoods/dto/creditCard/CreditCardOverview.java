package com.epam.greencyclegoods.dto.creditCard;

import com.epam.greencyclegoods.dto.user.UserOverview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreditCardOverview {
    private Integer id;
    private String cardNumber;
    private String cardHolder;
    private LocalDate cardExpiresAt;
    private String cvv;
    private UserOverview userOverview;
}
