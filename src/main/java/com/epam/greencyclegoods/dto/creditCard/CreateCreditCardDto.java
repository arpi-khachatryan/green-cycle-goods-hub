package com.epam.greencyclegoods.dto.creditCard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.CreditCardNumber;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCreditCardDto {
    @CreditCardNumber(message = "Invalid card number.")
    private String cardNumber;

    @NotBlank(message = "Name is mandatory.")
    private String cardHolder;

    @NotNull(message = "Expiration date is mandatory.")
    @FutureOrPresent(message = "Credit card has expired.")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate cardExpiresAt;

    @NotBlank(message = "CVV is mandatory.")
    @Digits(integer = 3, fraction = 0, message = "Invalid CVV.")
    private String cvv;
}
