package com.epam.greencyclegoods.dto.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EditEventDto {
    @NotBlank(message = "Name is mandatory.")
    @Size(min = 3, max = 70, message = "Name must be between 3 and 70 characters.")
    private String name;

    private String description;

    @NotNull(message = "Price is mandatory.")
    @PositiveOrZero(message = "Price must be greater than or equal to zero.")
    private Double price;

    @NotNull(message = "Event date and time are mandatory.")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime eventDateTime;

    private List<String> pictures;

    private Integer companyId;
}
