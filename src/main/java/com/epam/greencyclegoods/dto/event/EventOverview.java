package com.epam.greencyclegoods.dto.event;

import com.epam.greencyclegoods.dto.company.CompanyOverview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventOverview {
    private int id;
    private String name;
    private String description;
    private double price;
    private LocalDateTime eventDateTime;
    private List<String> pictures;
    private CompanyOverview companyOverview;
}
