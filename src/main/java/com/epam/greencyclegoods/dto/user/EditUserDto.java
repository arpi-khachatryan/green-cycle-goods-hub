package com.epam.greencyclegoods.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EditUserDto {
    @NotBlank(message = "First Name is mandatory.")
    @Size(min = 3, max = 15, message = "The length should be between 3 and 15 characters.")
    private String firstName;

    @NotBlank(message = "Last Name is mandatory.")
    @Size(min = 3, max = 15, message = "The length should be between 3 and 15 characters.")
    private String lastName;
}