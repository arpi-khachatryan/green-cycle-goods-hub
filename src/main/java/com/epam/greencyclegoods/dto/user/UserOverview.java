package com.epam.greencyclegoods.dto.user;

import com.epam.greencyclegoods.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserOverview {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
}
