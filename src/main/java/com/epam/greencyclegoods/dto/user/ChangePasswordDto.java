package com.epam.greencyclegoods.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChangePasswordDto {
    @NotBlank(message = "Password is mandatory.")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,20}$",
            message = "Password must be between 8 and 20 characters, and include at least one uppercase letter, one lowercase letter, one digit, and one special character (@ $ ! % * ? &).")
    private String oldPassword;

    @NotBlank(message = "Password is mandatory.")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,20}$",
            message = "Password must be between 8 and 20 characters, and include at least one uppercase letter, one lowercase letter, one digit, and one special character (@ $ ! % * ? &).")
    private String newPassword1;

    @NotBlank(message = "Password is mandatory.")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,20}$",
            message = "Password must be between 8 and 20 characters, and include at least one uppercase letter, one lowercase letter, one digit, and one special character (@ $ ! % * ? &).")
    private String newPassword2;
}
