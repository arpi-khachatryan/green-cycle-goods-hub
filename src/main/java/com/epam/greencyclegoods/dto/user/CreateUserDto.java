package com.epam.greencyclegoods.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserDto {
    @NotBlank(message = "First Name is mandatory.")
    @Size(min = 3, max = 15, message = "The length should be between 3 and 15 characters.")
    private String firstName;

    @NotBlank(message = "Last Name is mandatory.")
    @Size(min = 3, max = 15, message = "The length should be between 3 and 15 characters.")
    private String lastName;

    @Email
    @NotBlank(message = "Email is mandatory.")
    private String email;

    @NotBlank(message = "Password is mandatory.")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,20}$",
            message = "Password must be between 8 and 20 characters, and include at least one uppercase letter, one lowercase letter, one digit, and one special character (@ $ ! % * ? &).")
    private String password;

    private String verifyToken;

    private boolean enabled;
}