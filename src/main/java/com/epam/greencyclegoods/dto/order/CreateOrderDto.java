package com.epam.greencyclegoods.dto.order;

import com.epam.greencyclegoods.dto.product.ProductOverview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrderDto {
    @NotBlank(message = "Address is mandatory.")
    private String additionalAddress;

    @NotBlank(message = "Phone number is mandatory.")
    @Pattern(regexp = "^[+][0-9]{11}$", message = "Invalid phone number format.")
    @Size(min = 12, max = 12, message = "Phone number should start with '+' and have 11 digits.")
    private String additionalPhone;

    private double totalPrice;

//    private String paymentOption;

    private List<ProductOverview> productOverviews;
}
