package com.epam.greencyclegoods.dto.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EditOrderDto {
    @NotBlank(message = "Address is mandatory.")
    private String additionalAddress;

    @NotBlank(message = "Phone number is mandatory.")
    @Pattern(regexp = "^[+][0-9]{11}$", message = "Invalid phone number format.")
    @Size(min = 12, max = 12, message = "Phone number should start with '+' and have 11 digits.")
    private String additionalPhone;

    @NotBlank(message = "Current status is mandatory.")
    private String status;

//    private String paymentOption;

    @NotNull(message = "This is a mandatory field.")
    private String paymentCompleted;
}
