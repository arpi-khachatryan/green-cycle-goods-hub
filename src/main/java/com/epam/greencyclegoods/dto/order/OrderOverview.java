package com.epam.greencyclegoods.dto.order;

import com.epam.greencyclegoods.dto.product.ProductOverview;
import com.epam.greencyclegoods.dto.user.UserOverview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderOverview {
    private Integer id;
    private String additionalAddress;
    private String additionalPhone;
    private LocalDateTime orderAt;
    private double totalPrice;
    private String status;
//    private String paymentOption;
    private List<ProductOverview> productOverviews;
    private UserOverview userOverview;
    private boolean paymentCompleted;
}
