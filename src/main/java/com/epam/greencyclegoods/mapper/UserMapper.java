package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.user.CreateUserDto;
import com.epam.greencyclegoods.dto.user.UserOverview;
import com.epam.greencyclegoods.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    User mapToEntity(CreateUserDto createUserDto);

    UserOverview mapToDto(User user);

    List<UserOverview> mapToDtoList(List<User> users);
}