package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.registration.CreateRegistrationDto;
import com.epam.greencyclegoods.dto.registration.RegistrationOverview;
import com.epam.greencyclegoods.entity.Registration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RegistrationMapper {

    @Mapping(source = "dto.eventId", target = "event.id")
    Registration mapToEntity(CreateRegistrationDto dto);

    @Mapping(source = "registration.event", target = "eventOverview")
    @Mapping(source = "registration.user", target = "userOverview")
    RegistrationOverview mapToDto(Registration registration);

    List<RegistrationOverview> mapToDtoList(List<Registration> registrations);
}