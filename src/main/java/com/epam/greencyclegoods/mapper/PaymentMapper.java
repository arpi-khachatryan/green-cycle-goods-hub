package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.payment.PaymentOverview;
import com.epam.greencyclegoods.entity.Payment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PaymentMapper {

    @Mapping(source = "payment.order", target = "orderOverview")
    @Mapping(source = "payment.user", target = "userOverview")
    PaymentOverview mapToDto(Payment payment);

    List<PaymentOverview> mapToDto(List<Payment> payments);
}