package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.event.CreateEventDto;
import com.epam.greencyclegoods.dto.event.EventOverview;
import com.epam.greencyclegoods.entity.Event;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Component
@Mapper(componentModel = "spring")
public interface EventMapper {

    @Mapping(source = "dto.companyId", target = "company.id")
    Event mapToEntity(CreateEventDto dto);

    @Mapping(source = "event.company", target = "companyOverview")
    EventOverview mapToDto(Event event);

    List<EventOverview> mapToDtoList(List<Event> events);
}