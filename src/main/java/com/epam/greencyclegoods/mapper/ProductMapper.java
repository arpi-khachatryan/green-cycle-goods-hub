package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.product.CreateProductDto;
import com.epam.greencyclegoods.dto.product.ProductOverview;
import com.epam.greencyclegoods.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Component
@Mapper(componentModel = "spring")
public interface ProductMapper {

    @Mapping(source = "dto.companyId", target = "company.id")
    @Mapping(source = "dto.productCategoryId", target = "productCategory.id")
    Product mapToEntity(CreateProductDto dto);

    @Mapping(source = "product.company", target = "companyOverview")
    @Mapping(source = "product.productCategory", target = "productCategoryOverview")
    @Mapping(source = "product.user", target = "userOverview")
    ProductOverview mapToDto(Product product);

    List<ProductOverview> mapToDtoList(List<Product> products);
}