package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.company.CompanyOverview;
import com.epam.greencyclegoods.dto.company.CreateCompanyDto;
import com.epam.greencyclegoods.entity.Company;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Mapper(componentModel = "spring")
public interface CompanyMapper {

    @Mapping(source = "dto.companyCategoryId", target = "companyCategory.id")
    Company mapToEntity(CreateCompanyDto dto);

    @Mapping(source = "company.companyCategory", target = "companyCategoryOverview")
    @Mapping(source = "company.user", target = "userOverview")
    CompanyOverview mapToDto(Company company);

    List<CompanyOverview> mapToDtoList(List<Company> companies);
}
