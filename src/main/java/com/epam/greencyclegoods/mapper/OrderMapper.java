package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.order.CreateOrderDto;
import com.epam.greencyclegoods.dto.order.OrderOverview;
import com.epam.greencyclegoods.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderMapper {

    @Mapping(source = "dto.productOverviews", target = "products")
    Order mapToEntity(CreateOrderDto dto);

    @Mapping(source = "order.products", target = "productOverviews")
    @Mapping(source = "order.user", target = "userOverview")
    OrderOverview mapToDto(Order order);

    List<OrderOverview> mapToDto(List<Order> orders);
}