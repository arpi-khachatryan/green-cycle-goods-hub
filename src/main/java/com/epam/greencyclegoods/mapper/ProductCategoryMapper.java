package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.productCategory.CreateProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.ProductCategoryOverview;
import com.epam.greencyclegoods.entity.ProductCategory;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Component
@Mapper(componentModel = "spring")
public interface ProductCategoryMapper {

    ProductCategory mapToEntity(CreateProductCategoryDto productCategoryDto);

    ProductCategoryOverview mapToDto(ProductCategory productCategory);

    List<ProductCategoryOverview> mapToDtoList(List<ProductCategory> productCategories);
}