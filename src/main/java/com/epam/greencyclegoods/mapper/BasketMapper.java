package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.basket.BasketOverview;
import com.epam.greencyclegoods.entity.Basket;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BasketMapper {

    @Mapping(source = "basket.product", target = "productOverview")
    @Mapping(source = "basket.user", target = "userOverview")
    BasketOverview mapToDto(Basket basket);

    List<BasketOverview> mapToDtoList(List<Basket> baskets);
}

