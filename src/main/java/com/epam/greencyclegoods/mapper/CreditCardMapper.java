package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.dto.creditCard.CreditCardOverview;
import com.epam.greencyclegoods.entity.CreditCard;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CreditCardMapper {

    CreditCard mapToEntity(CreateCreditCardDto dto);

    @Mapping(source = "creditCard.user", target = "userOverview")
    CreditCardOverview mapToDto(CreditCard creditCard);

    List<CreditCardOverview> mapToDto(List<CreditCard> creditCard);
}