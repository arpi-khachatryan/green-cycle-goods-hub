package com.epam.greencyclegoods.mapper;

import com.epam.greencyclegoods.dto.companyCategory.CompanyCategoryOverview;
import com.epam.greencyclegoods.dto.companyCategory.CreateCompanyCategoryDto;
import com.epam.greencyclegoods.entity.CompanyCategory;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Mapper(componentModel = "spring")
public interface CompanyCategoryMapper {

    CompanyCategory mapToEntity(CreateCompanyCategoryDto createCompanyCategoryDto);

    CompanyCategoryOverview mapToDto(CompanyCategory companyCategory);

    List<CompanyCategoryOverview> mapToDtoList(List<CompanyCategory> companyCategory);
}
