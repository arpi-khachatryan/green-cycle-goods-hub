package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.CompanyCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface CompanyCategoryRepository extends JpaRepository<CompanyCategory, Integer>, PagingAndSortingRepository<CompanyCategory, Integer> {

    boolean existsByName(String name);
}