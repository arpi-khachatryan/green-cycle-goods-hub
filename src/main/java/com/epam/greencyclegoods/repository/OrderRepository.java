package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>, PagingAndSortingRepository<Order, Integer> {

    Page<Order> findByUserId(int id, Pageable pageable);

    Page<Order> findAll(Pageable pageable);

    @Query("SELECT DISTINCT o FROM Order o JOIN o.products p JOIN p.user u WHERE u.id = :companyOwnerId")
    Page<Order> findOrdersByCompanyOwner(@Param("companyOwnerId") int companyOwnerId, Pageable pageable);

    @Query("SELECT COUNT(o) > 0 FROM Order o JOIN o.products p WHERE p.id = :productId")
    boolean existsByProduct(@Param("productId") int productId);
}
