package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.Order;
import com.epam.greencyclegoods.entity.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer>, PagingAndSortingRepository<Payment, Integer> {

    Page<Payment> findByUserId(int id, Pageable pageable);

    Payment findByOrder(Order order);

    @Query("SELECT DISTINCT p FROM Payment p " +
            "JOIN p.order o " +
            "JOIN o.products pr " +
            "JOIN pr.user u " +
            "WHERE u.id = :companyOwnerId")
    Page<Payment> findPaymentsByCompanyOwner(@Param("companyOwnerId") int companyOwnerId, Pageable pageable);
}