package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface UserRepository extends JpaRepository<User, Integer>, PagingAndSortingRepository<User, Integer> {

    Optional<User> findByEmail(String email);

    boolean existsByEmailIgnoreCase(String email);
}
