package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer>, PagingAndSortingRepository<ProductCategory, Integer> {

    boolean existsByNameIgnoreCase(String name);
}