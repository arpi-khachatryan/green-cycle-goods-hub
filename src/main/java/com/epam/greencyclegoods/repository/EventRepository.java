package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface EventRepository extends JpaRepository<Event, Integer>, PagingAndSortingRepository<Event, Integer> {

    List<Event> findEventsByCompany_Id(int companyId);

    @Query("SELECT COUNT(e) > 0 FROM Event e WHERE e.company.id = :companyId")
    boolean existsByCompanyId(@Param("companyId") int companyId);
}