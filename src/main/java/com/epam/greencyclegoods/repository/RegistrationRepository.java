package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.Registration;
import com.epam.greencyclegoods.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Integer>, PagingAndSortingRepository<Registration, Integer> {

    List<Registration> findByEventId(int id);

    Page<Registration> findByUserId(int id, Pageable pageable);

    @Query("SELECT DISTINCT r FROM Registration r " +
            "JOIN r.event e " +
            "JOIN e.company c " +
            "JOIN c.user u " +
            "WHERE u.id = :companyOwnerId")
    Page<Registration> findRegistrationsByCompanyOwner(@Param("companyOwnerId") int companyOwnerId, Pageable pageable);

    @Query("SELECT r FROM Registration r WHERE r.user = :user AND r.event.id = :eventId")
    Optional<Registration> findRegistrationByUserAndEvent(@Param("user") User user, @Param("eventId") Integer eventId);

    void deleteAllByIdIn(List<Integer> registrationIds);
}