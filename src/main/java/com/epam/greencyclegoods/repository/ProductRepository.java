package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.Product;
import com.epam.greencyclegoods.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>, PagingAndSortingRepository<Product, Integer> {

    List<Product> findProductByUser(User user);

    List<Product> findProductsByCompany_Id(int id);

    Page<Product> findByOrderByPriceAsc(Pageable pageable);

    Page<Product> findByOrderByPriceDesc(Pageable pageable);

    Page<Product> findProductsByProductCategory_Id(int id, Pageable pageable);

    @Query("SELECT COUNT(p) > 0 FROM Product p WHERE p.productCategory.id = :productCategoryId")
    boolean existsByProductCategoryId(@Param("productCategoryId") int productCategoryId);

    @Query("SELECT COUNT(p) > 0 FROM Product p WHERE p.company.id = :companyId")
    boolean existsByCompanyId(@Param("companyId") int companyId);
}

