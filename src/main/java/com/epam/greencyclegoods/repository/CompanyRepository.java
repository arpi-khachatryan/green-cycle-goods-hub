package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.Company;
import com.epam.greencyclegoods.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer>, PagingAndSortingRepository<Company, Integer> {

    boolean existsByEmailIgnoreCase(String email);

    Page<Company> findCompanyByUserId(int id, Pageable pageable);

    List<Company> findCompaniesByUser(User user);

    @Query("SELECT COUNT(c) > 0 FROM Company c WHERE c.companyCategory.id = :companyCategoryId")
    boolean existsByCompanyCategoryId(@Param("companyCategoryId") int companyCategoryId);
}