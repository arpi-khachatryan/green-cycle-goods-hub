package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.CreditCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface CreditCardRepository extends JpaRepository<CreditCard, Integer> {

    boolean existsByCardNumber(String cardNumber);
}