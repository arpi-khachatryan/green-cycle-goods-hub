package com.epam.greencyclegoods.repository;

import com.epam.greencyclegoods.entity.Basket;
import com.epam.greencyclegoods.entity.Product;
import com.epam.greencyclegoods.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Repository
public interface BasketRepository extends JpaRepository<Basket, Integer>, PagingAndSortingRepository<Basket, Integer> {

    Page<Basket> findAll(Pageable pageable);

    List<Basket> findBasketByUser(User user);

    Optional<Basket> findBasketByProductAndUser(Product product, User user);

    @Query("SELECT DISTINCT b FROM Basket b JOIN b.product p JOIN p.user u WHERE u.id=:companyOwnerId")
    List<Basket> findBasketsByCompanyOwner(@org.springframework.data.repository.query.Param("companyOwnerId") int id);

    @Query("SELECT COUNT(b) > 0 FROM Basket b WHERE b.product.id = :productId")
    boolean existsByProduct(@Param("productId") int productId);

    @Query("SELECT MIN(b.itemQuantity) FROM Basket b WHERE b.product.id = :productId")
    Integer minProductCountInAllBaskets(@Param("productId") int productId);

    @Query("SELECT SUM(b.itemQuantity) FROM Basket b WHERE b.user = :user")
    Integer getTotalProductCountInBasketForUser(@Param("user") User user);
}