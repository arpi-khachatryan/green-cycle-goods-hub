package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.company.CompanyOverview;
import com.epam.greencyclegoods.dto.company.CreateCompanyDto;
import com.epam.greencyclegoods.dto.company.EditCompanyDto;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface CompanyService {

    Page<CompanyOverview> companies(Pageable pageable);

    Page<CompanyOverview> companiesByUser(User user, Pageable pageable) throws CompanyNotFoundException;

    CompanyOverview company(int id) throws CompanyNotFoundException;

    void edit(EditCompanyDto dto, int id, MultipartFile[] files) throws CompanyNotFoundException, ImageUploadException;

    void delete(int id) throws CompanyNotFoundException, CompanyDeleteException;

    List<CompanyOverview> companies();

    List<CompanyOverview> companiesByUser(User user);

    void add(CreateCompanyDto companyDto, MultipartFile[] files, User user) throws UserUpdateException, ImageUploadException, CompanyCreationException;

    byte[] getCompanyImage(String fileName);
}
