package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.registration.CreateRegistrationDto;
import com.epam.greencyclegoods.dto.registration.EditRegistrationDto;
import com.epam.greencyclegoods.dto.registration.RegistrationOverview;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.RegistrationCreationException;
import com.epam.greencyclegoods.exception.RegistrationNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface RegistrationService {

    Page<RegistrationOverview> registrations(Pageable pageable);

    Page<RegistrationOverview> registrationsByOwner(User user, Pageable pageable) throws RegistrationNotFoundException;

    Page<RegistrationOverview> registrationsByUser(int id, Pageable pageable);

    RegistrationOverview registrationById(int id) throws RegistrationNotFoundException;

    void edit(EditRegistrationDto dto, int id) throws RegistrationNotFoundException;

    void delete(int id) throws RegistrationNotFoundException;

    void addRegistration(CreateRegistrationDto dto, User user) throws RegistrationCreationException;
}


