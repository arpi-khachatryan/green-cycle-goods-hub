package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.entity.VerificationToken;
import com.epam.greencyclegoods.exception.VerificationException;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface VerificationTokenService {

    VerificationToken create(User user);

    VerificationToken find(String plainToken) throws VerificationException;

    void delete(VerificationToken token);
}

