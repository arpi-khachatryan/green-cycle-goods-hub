package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.basket.BasketOverview;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.BasketNotFoundException;
import com.epam.greencyclegoods.exception.ProductNotFoundException;
import com.epam.greencyclegoods.exception.UserNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface BasketService {

    double calculateTotalBasketPrice(User user) throws BasketNotFoundException;

    List<BasketOverview> baskets(User user) throws BasketNotFoundException;

    void delete(int id, User user) throws ProductNotFoundException, BasketNotFoundException;

    boolean productsInStock(User user);

    Page<BasketOverview> baskets(Pageable pageable, User user) throws BasketNotFoundException;

    Page<BasketOverview> ownerBaskets(Pageable pageable, User user) throws BasketNotFoundException;

    Page<BasketOverview> allBaskets(Pageable pageable) throws BasketNotFoundException;

    void addToBasket(int id, User user) throws UserNotFoundException, ProductNotFoundException;

    int totalProductCountInBasketForUser(User user);
}


