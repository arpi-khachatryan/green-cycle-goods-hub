package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.dto.order.CreateOrderDto;
import com.epam.greencyclegoods.dto.order.EditOrderDto;
import com.epam.greencyclegoods.dto.order.OrderOverview;
import com.epam.greencyclegoods.entity.Product;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface OrderService {

    Page<OrderOverview> orders(Pageable pageable);

    OrderOverview order(int id) throws OrderNotFoundException;

    Page<OrderOverview> ownerOrders(int id, Pageable pageable);

    Page<OrderOverview> userOrders(int id, Pageable pageable);

    void add(CreateOrderDto orderDto, CreateCreditCardDto cardDto, User user) throws BasketNotFoundException, OrderNotFoundException, UserNotFoundException, CreditCardNotFoundException, ProductNotFoundException;

    Map<Product, Double> productQuantity(int orderId) throws OrderNotFoundException;

    void edit(EditOrderDto dto, int id) throws OrderNotFoundException;

    void delete(int id) throws OrderNotFoundException;
}


