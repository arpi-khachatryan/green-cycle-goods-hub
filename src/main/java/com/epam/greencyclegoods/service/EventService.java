package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.event.CreateEventDto;
import com.epam.greencyclegoods.dto.event.EditEventDto;
import com.epam.greencyclegoods.dto.event.EventOverview;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.EmailSendingException;
import com.epam.greencyclegoods.exception.EventDeleteException;
import com.epam.greencyclegoods.exception.EventNotFoundException;
import com.epam.greencyclegoods.exception.ImageUploadException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface EventService {

    Page<EventOverview> events(Pageable pageable);

    Map<Integer, List<EventOverview>> sortByCompany();

    Page<EventOverview> eventsByUser(User user, Pageable pageable);

    EventOverview eventById(int id) throws EventNotFoundException;

    void save(CreateEventDto eventDto, MultipartFile[] files) throws ImageUploadException;

    void edit(EditEventDto dto, int id, MultipartFile[] files) throws EventNotFoundException, ImageUploadException;

    void delete(int id) throws EventNotFoundException, EventDeleteException, EmailSendingException;

    List<EventOverview> events();

    EventOverview getEvent(int id) throws EventNotFoundException;

    byte[] getEventImage(String fileName);

    List<EventOverview> companyEvents(int id);
}
