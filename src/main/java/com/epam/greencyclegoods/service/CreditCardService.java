package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.CreditCardNotFoundException;
import com.epam.greencyclegoods.exception.UserNotFoundException;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface CreditCardService {

    void addCreditCard(CreateCreditCardDto createCreditCardDto, User user) throws CreditCardNotFoundException, UserNotFoundException;
}


