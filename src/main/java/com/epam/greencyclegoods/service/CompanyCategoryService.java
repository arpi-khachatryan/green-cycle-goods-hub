package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.companyCategory.CompanyCategoryOverview;
import com.epam.greencyclegoods.dto.companyCategory.CreateCompanyCategoryDto;
import com.epam.greencyclegoods.exception.CategoryCreationException;
import com.epam.greencyclegoods.exception.CategoryDeleteException;
import com.epam.greencyclegoods.exception.CategoryNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface CompanyCategoryService {

    Page<CompanyCategoryOverview> categories(Pageable pageable);

    List<CompanyCategoryOverview> categories() throws CategoryNotFoundException;

    void add(CreateCompanyCategoryDto dto) throws CategoryCreationException;

    void delete(int id) throws CategoryNotFoundException, CategoryDeleteException;
}
