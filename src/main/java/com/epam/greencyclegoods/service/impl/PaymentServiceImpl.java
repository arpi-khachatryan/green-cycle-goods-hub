package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.dto.payment.EditPaymentDto;
import com.epam.greencyclegoods.dto.payment.PaymentOverview;
import com.epam.greencyclegoods.entity.Order;
import com.epam.greencyclegoods.entity.Payment;
import com.epam.greencyclegoods.entity.PaymentStatus;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.mapper.PaymentMapper;
import com.epam.greencyclegoods.repository.CreditCardRepository;
import com.epam.greencyclegoods.repository.OrderRepository;
import com.epam.greencyclegoods.repository.PaymentRepository;
import com.epam.greencyclegoods.service.CreditCardService;
import com.epam.greencyclegoods.service.PaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    private final PaymentMapper paymentMapper;
    private final PaymentRepository paymentRepository;
    private final CreditCardService creditCardService;
    private final CreditCardRepository creditCardRepository;
    private final OrderRepository orderRepository;

    @Override
    public Page<PaymentOverview> payments(Pageable pageable) {
        Page<Payment> payments = paymentRepository.findAll(pageable);
        return processPaymentList(payments);
    }

    @Override
    public Page<PaymentOverview> userPayments(int id, Pageable pageable) {
        Page<Payment> payments = paymentRepository.findByUserId(id, pageable);
        return processPaymentList(payments);
    }

    @Override
    public Page<PaymentOverview> ownerPayments(int id, Pageable pageable) {
        Page<Payment> payments = paymentRepository.findPaymentsByCompanyOwner(id, pageable);
        return processPaymentList(payments);
    }

    private Page<PaymentOverview> processPaymentList(Page<Payment> payments) {
        if (payments.isEmpty()) {
            log.info("No payments found.");
            return Page.empty();
        } else {
            log.info("Payments successfully found.");
            return payments.map(paymentMapper::mapToDto);
        }
    }

    @Override
    public PaymentOverview payment(int id) throws PaymentNotFoundException {
        Payment payment = paymentRepository.findById(id).orElseThrow(() -> new PaymentNotFoundException("Payment not found."));
        log.info("Payment successfully found.");
        return paymentMapper.mapToDto(payment);
    }

    @Override
    public void addPayment(Order order, CreateCreditCardDto cardDto, User user) throws OrderNotFoundException, UserNotFoundException, CreditCardNotFoundException {
        if (order == null) {
            throw new OrderNotFoundException("Order not found.");
        }
        if (user == null) {
            throw new UserNotFoundException("User not found for order.");
        }

        Payment payment = Payment.builder()
                .paymentCreateDate(LocalDateTime.now())
                .paymentDate(LocalDateTime.now())
                .user(user)
                .order(order)
                .totalAmount(order.getTotalPrice())
//                .paymentOption(order.getPaymentOption())
                .build();

//        if (order.getPaymentOption() == PaymentOption.CREDIT_CARD) {
        if (cardDto != null) {
            if (!creditCardRepository.existsByCardNumber(cardDto.getCardNumber())) {
                creditCardService.addCreditCard(cardDto, user);
            }
        } else {
            throw new CreditCardNotFoundException("Invalid credit card information.");
        }
        payment.setStatus(PaymentStatus.PAID);
//        }
//
//        else {
//            payment.setStatus(PaymentStatus.UNPAID);
//        }
        paymentRepository.save(payment);
        log.info("The payment was successfully stored in the database. Total Amount: {}", payment.getTotalAmount());
    }

    @Override
    public void edit(EditPaymentDto dto, int id) throws PaymentNotFoundException, InvalidDateFormatException {
        Payment payment = paymentRepository.findById(id).orElseThrow(() -> new PaymentNotFoundException("Payment not found."));
        Order order = payment.getOrder();

        if (dto.getStatus() != null) {
            payment.setStatus(PaymentStatus.valueOf(dto.getStatus()));
            log.info("Payment status updated to: {}", payment.getStatus());
        } else if (payment.getStatus() == null) {
            payment.setStatus(PaymentStatus.UNPAID);
            log.info("Payment status set to UNPAID");
        }

        String paymentDate = dto.getPaymentDate();
        if (paymentDate != null && !paymentDate.isEmpty()) {
            try {
                payment.setPaymentDate(LocalDateTime.parse(paymentDate));
                log.info("Payment date updated to: {}", paymentDate);
            } catch (DateTimeParseException e) {
                log.error("Invalid date format for payment date: {}", paymentDate);
                throw new InvalidDateFormatException("Invalid date format for payment date.");
            }
        }

        log.info("Payment successfully updated in the database. Payment ID: {}", payment.getId());
        paymentRepository.save(payment);

        if (payment.getStatus().equals(PaymentStatus.PAID)) {
            if (!order.isPaymentCompleted()) {
                order.setPaymentCompleted(true);
                orderRepository.save(order);
                log.info("Order payment status updated to PAID.");
            }
        } else if (payment.getStatus().equals(PaymentStatus.UNPAID) || payment.getStatus().equals(PaymentStatus.PROCESSING) || payment.getStatus().equals(PaymentStatus.FAILED) && order.isPaymentCompleted()) {
            order.setPaymentCompleted(false);
            orderRepository.save(order);
            log.info("Order payment status updated to UNPAID.");
        }
    }

    @Override
    public void delete(int id) throws PaymentNotFoundException, PaymentDeleteException {
        Optional<Payment> paymentOptional = paymentRepository.findById(id);
        if (paymentOptional.isPresent()) {
            Payment payment = paymentOptional.get();
            if (!payment.getStatus().equals(PaymentStatus.PAID)) {
                throw new PaymentDeleteException("Payment must be marked as PAID to delete it.");
            }

            paymentRepository.deleteById(id);
            log.info("Payment with ID {} has been successfully deleted", id);
        } else {
            throw new PaymentNotFoundException("Payment not found.");
        }
    }
}

