package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.basket.BasketOverview;
import com.epam.greencyclegoods.entity.Basket;
import com.epam.greencyclegoods.entity.Product;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.BasketNotFoundException;
import com.epam.greencyclegoods.exception.ProductNotFoundException;
import com.epam.greencyclegoods.exception.UserNotFoundException;
import com.epam.greencyclegoods.mapper.BasketMapper;
import com.epam.greencyclegoods.repository.BasketRepository;
import com.epam.greencyclegoods.repository.ProductRepository;
import com.epam.greencyclegoods.service.BasketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {

    private final BasketMapper basketMapper;
    private final BasketRepository basketRepository;
    private final ProductRepository productRepository;

    @Override
    public Page<BasketOverview> baskets(Pageable pageable, User user) throws BasketNotFoundException {
        List<Basket> baskets = basketRepository.findBasketByUser(user);
        return processBasketList(baskets);
    }

    @Override
    public Page<BasketOverview> ownerBaskets(Pageable pageable, User user) throws BasketNotFoundException {
        List<Basket> baskets = basketRepository.findBasketsByCompanyOwner(user.getId());
        return processBasketList(baskets);
    }

    @Override
    public Page<BasketOverview> allBaskets(Pageable pageable) throws BasketNotFoundException {
        List<Basket> baskets = basketRepository.findAll();
        return processBasketList(baskets);
    }

    private Page<BasketOverview> processBasketList(List<Basket> baskets) throws BasketNotFoundException {
        if (baskets.isEmpty()) {
            log.info("No baskets found.");
            return Page.empty();
        }
        Page<Basket> basketPage = new PageImpl<>(baskets);
        log.info("Baskets successfully found.");
        return basketPage.map(basketMapper::mapToDto);
    }

    @Override
    public List<BasketOverview> baskets(User user) throws BasketNotFoundException {
        try {
            log.info("Baskets successfully found.");
            return basketMapper.mapToDtoList(basketRepository.findBasketByUser(user));
        } catch (Exception e) {
            log.info("Failed to retrieve baskets.");
            throw new BasketNotFoundException("Basket not found.");
        }
    }

    public double calculateTotalBasketPrice(User user) throws BasketNotFoundException {
        double totalPrice = 0;
        try {
            List<Basket> baskets = basketRepository.findBasketByUser(user);
            if (baskets.isEmpty()) {
                throw new BasketNotFoundException("Basket not found for user.");
            }
            for (Basket basket : baskets) {
                Product product = basket.getProduct();

                if (product == null || product.getProductCount() <= 0) {
                    continue;
                }

                int availableQuantity = (int) Math.min(product.getProductCount(), basket.getItemQuantity());
                totalPrice += product.getPrice() * availableQuantity;
            }
            log.info("The total price is calculated.");
        } catch (Exception e) {
            log.error("Error while calculating total price", e);
            throw new BasketNotFoundException("Failed to calculate the total price.");
        }
        return totalPrice;
    }

    @Override
    public void addToBasket(int id, User user) throws UserNotFoundException, ProductNotFoundException {
        if (user == null) {
            throw new UserNotFoundException("Product not found.");
        }
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isEmpty()) {
            throw new ProductNotFoundException("Product not found.");
        }
        Product product = productOptional.get();

        int productCount = product.getProductCount();

        if (productCount <= 0) {
            throw new ProductNotFoundException("Sorry, this product is currently out of stock. Cannot add to basket.");
        }

        Optional<Basket> basketOptional = basketRepository.findBasketByProductAndUser(product, user);
        Basket basket;
        if (basketOptional.isEmpty()) {
            basket = new Basket();
            basket.setProduct(product);
            basket.setItemQuantity(1);
            basket.setUser(user);
            log.info("Product successfully added to basket.");
        } else {
            basket = basketOptional.get();
            double newQuantity = basket.getItemQuantity() + 1;

            if (newQuantity > productCount) {
                throw new ProductNotFoundException("Sorry, this product is currently out of stock. Cannot add to basket.");
            }

            basket.setItemQuantity(newQuantity);
            log.info("Product successfully added to basket.");
        }
        basketRepository.save(basket);
    }

    @Override
    public void delete(int productId, User user) throws ProductNotFoundException, BasketNotFoundException {
        Product product = productRepository.findById(productId).orElseThrow(() -> new ProductNotFoundException("Product not found."));
        Optional<Basket> optionalBasket = basketRepository.findBasketByProductAndUser(product, user);

        if (optionalBasket.isPresent()) {
            Basket basket = optionalBasket.get();

            double quantity = basket.getItemQuantity();
            if (quantity > 1) {
                quantity -= 1;
                basket.setItemQuantity(quantity);
                basketRepository.save(basket);
            } else {
                basket.setItemQuantity(0);
                basketRepository.delete(basket);
            }
            log.info("The product with ID {} has been successfully deleted from the basket", productId);
        } else {
            throw new BasketNotFoundException("Basket not found for the product.");
        }
    }

    @Override
    public int totalProductCountInBasketForUser(User user) {
        Integer totalProductCountInBasketForUser = basketRepository.getTotalProductCountInBasketForUser(user);
        return Objects.requireNonNullElse(totalProductCountInBasketForUser, 0);
    }

    @Override
    public boolean productsInStock(User user) {
        List<Basket> baskets = basketRepository.findBasketByUser(user);
        return baskets.stream()
                .allMatch(basket -> {
                    int productCountInStore = basket.getProduct().getProductCount();
                    return productCountInStore <= 0;
                });
    }
}
