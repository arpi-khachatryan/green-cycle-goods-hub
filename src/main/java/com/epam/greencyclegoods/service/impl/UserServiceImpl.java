package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.user.ChangePasswordDto;
import com.epam.greencyclegoods.dto.user.CreateUserDto;
import com.epam.greencyclegoods.dto.user.EditUserDto;
import com.epam.greencyclegoods.dto.user.UserOverview;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.entity.VerificationToken;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.mapper.UserMapper;
import com.epam.greencyclegoods.repository.UserRepository;
import com.epam.greencyclegoods.service.MailService;
import com.epam.greencyclegoods.service.UserService;
import com.epam.greencyclegoods.service.VerificationTokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final MailService mailService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final VerificationTokenService tokenService;

    @Override
    public Page<UserOverview> users(Pageable pageable) {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            log.info("No users found.");
            return Page.empty();
        } else {
            log.info("Users successfully found.");
            List<UserOverview> userOverviews = userMapper.mapToDtoList(users);
            return new PageImpl<>(userOverviews);
        }
    }

    @Override
    public UserOverview user(int id) throws UserNotFoundException {
        Optional<User> userOptional = userRepository.findById(id);
        User user = userOptional.orElseThrow(() -> new UserNotFoundException("User not found."));
        log.info("User successfully found with ID: {}", id);
        return userMapper.mapToDto(user);
    }

    @Override
    public void save(CreateUserDto dto) throws AuthenticationException, EmailSendingException {
        log.info("New request to get registered. Email {}", dto.getEmail());
        if (userRepository.existsByEmailIgnoreCase(dto.getEmail())) {
            log.info("There is already a user with email {}", dto.getEmail());
            throw new AuthenticationException("Registration failed for email " + dto.getEmail() + ". Please check your information and try again.");
        }
        User user = userMapper.mapToEntity(dto);
        user.setRole(Role.CUSTOMER);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        log.info("User {} has successfully registered", user.getEmail());
        VerificationToken token = tokenService.create(user);

        String verificationUrl = "http://localhost:8081/users/verify?token=" + token.getPlainToken();
        String emailSubject = "Welcome to Green Cycle Goods!";
        String emailMessage = "Hi " + dto.getFirstName() + " " + dto.getLastName() + ",\n\n"
                + "Thank you for registering with our application. To get started, please verify your account by clicking the link below:\n\n"
                + verificationUrl + "\n\n"
                + "This verification link will expire in 12 hours.\n\n"
                + "If you didn't request this verification, please disregard this email.\n\n"
                + "If you have any questions or need assistance, please contact our support team at info@greenCycleGoods.com.\n\n"
                + "Thank you and welcome to our community!";
        mailService.sendEmail(dto.getEmail(), emailSubject, emailMessage);
        log.info("Verification token was sent to email {}", user.getEmail());
    }

    @Override
    public void verify(String plainToken) throws VerificationException {
        log.info("Verification request received for token: {}", plainToken);
        VerificationToken token = tokenService.find(plainToken);
        if (token == null) {
            log.error("Token not found: {}", plainToken);
            throw new VerificationException("Verification failed: Token not found.");
        }
        User user = token.getUser();
        if (user == null) {
            log.error("User not found for token: {}", plainToken);
            throw new VerificationException("Verification failed: User not found.");
        }
        tokenService.delete(token);
        if (user.isEnabled()) {
            log.warn("User already enabled: {}", user.getEmail());
            throw new VerificationException("Verification failed: User is already enabled.");
        }
        user.setEnabled(true);
        userRepository.save(user);
        log.info("User {} has been successfully verified and enabled.", user.getEmail());
    }

    @Override
    public void edit(EditUserDto dto, int id) throws UserNotFoundException {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isEmpty()) {
            log.error("User not found with ID {}", id);
            throw new UserNotFoundException("Failed to edit users' data. Please try again later.");
        }
        User user = optionalUser.get();
        String firstName = dto.getFirstName();
        if (StringUtils.hasText(firstName)) {
            user.setFirstName(firstName);
        }
        String lastName = dto.getLastName();
        if (StringUtils.hasText(lastName)) {
            user.setLastName(lastName);
        }
        userRepository.save(user);
        log.info("User data edited successfully for user with ID {}", id);
    }

    @Override
    public void changePassword(Integer id, ChangePasswordDto dto) throws UserNotFoundException, PasswordChangeException {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isEmpty()) {
            log.error("User not found with ID {}", id);
            throw new UserNotFoundException("User not found.");
        }
        User user = optionalUser.get();
        String oldPassword = dto.getOldPassword();
        String newPassword1 = dto.getNewPassword1();
        String newPassword2 = dto.getNewPassword2();

        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            log.error("Provided wrong password for user with ID {}", id);
            throw new PasswordChangeException("Provided wrong password.");
        }
        if (passwordEncoder.matches(newPassword1, user.getPassword())) {
            log.warn("Provided old password in change password request for user with ID {}", id);
            throw new PasswordChangeException("Provided old password in change password request.");
        }
        if (!newPassword1.equals(newPassword2)) {
            log.error("New passwords don't match for user with ID {}", id);
            throw new PasswordChangeException("New passwords don't match.");
        }
        user.setPassword(passwordEncoder.encode(newPassword1));
        userRepository.save(user);
        log.info("Password changed successfully for user with ID {}", id);
    }

    @Override
    public void delete(int id) throws UserNotFoundException {
        if (!userRepository.existsById(id)) {
            log.info("User not found with ID {}", id);
            throw new UserNotFoundException("User deletion failed: User not found.");
        }
        try {
            log.info("Deleting user with ID {}", id);
            userRepository.deleteById(id);
            log.info("The user with ID {} has been successfully deleted", id);
        } catch (Exception e) {
            log.error("Error deleting user with ID {}", id, e);
            throw new UserNotFoundException("User deletion failed.");
        }
    }
}
