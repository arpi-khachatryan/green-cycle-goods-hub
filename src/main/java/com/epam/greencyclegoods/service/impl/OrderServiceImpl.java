package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.dto.order.CreateOrderDto;
import com.epam.greencyclegoods.dto.order.EditOrderDto;
import com.epam.greencyclegoods.dto.order.OrderOverview;
import com.epam.greencyclegoods.dto.product.ProductOverview;
import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.mapper.OrderMapper;
import com.epam.greencyclegoods.mapper.ProductMapper;
import com.epam.greencyclegoods.repository.BasketRepository;
import com.epam.greencyclegoods.repository.OrderRepository;
import com.epam.greencyclegoods.repository.PaymentRepository;
import com.epam.greencyclegoods.repository.ProductRepository;
import com.epam.greencyclegoods.service.BasketService;
import com.epam.greencyclegoods.service.OrderService;
import com.epam.greencyclegoods.service.PaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderMapper orderMapper;
    private final BasketService basketService;
    private final ProductMapper productMapper;
    private final PaymentService paymentService;
    private final OrderRepository orderRepository;
    private final BasketRepository basketRepository;
    private final PaymentRepository paymentRepository;
    private final ProductRepository productRepository;

    @Override
    public Page<OrderOverview> orders(Pageable pageable) {
        Page<Order> orders = orderRepository.findAll(pageable);
        return processOrderList(orders);
    }

    @Override
    public Page<OrderOverview> ownerOrders(int id, Pageable pageable) {
        Page<Order> orders = orderRepository.findOrdersByCompanyOwner(id, pageable);
        return processOrderList(orders);
    }

    @Override
    public Page<OrderOverview> userOrders(int id, Pageable pageable) {
        Page<Order> orders = orderRepository.findByUserId(id, pageable);
        return processOrderList(orders);
    }

    private Page<OrderOverview> processOrderList(Page<Order> orders) {
        if (orders.isEmpty()) {
            log.info("No orders found.");
            return Page.empty();
        }
        log.info("Orders successfully found.");
        return orders.map(orderMapper::mapToDto);
    }

    @Override
    public OrderOverview order(int id) throws OrderNotFoundException {
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new OrderNotFoundException("Order not found."));
        log.info("Order successfully found.");
        return orderMapper.mapToDto(order);
    }

    @Override
    public Map<Product, Double> productQuantity(int orderId) throws OrderNotFoundException {
        Map<Product, Double> productsQuantity = new LinkedHashMap<>();
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new OrderNotFoundException("Order not found."));
        List<Product> products = order.getProducts();
        for (Product product : products) {
            productsQuantity.merge(product, 1.0, Double::sum);
        }
        return productsQuantity;
    }

    @Override
    public void add(CreateOrderDto orderDto, CreateCreditCardDto cardDto, User user) throws BasketNotFoundException, OrderNotFoundException, UserNotFoundException, CreditCardNotFoundException, ProductNotFoundException {
        log.info("Calculating total basket price for user: {}", user.getId());
        double totalPrice = basketService.calculateTotalBasketPrice(user);
        orderDto.setTotalPrice(totalPrice);

        List<Basket> basketByUser = basketRepository.findBasketByUser(user);

        if (basketByUser.isEmpty()) {
            log.warn("No basket found for user: {}", user.getId());
            throw new BasketNotFoundException("Basket not found for the user.");
        }

        List<ProductOverview> orderProducts = new ArrayList<>();

        for (Basket basket : basketByUser) {
            Product product = basket.getProduct();
            int productCount = product.getProductCount();
            double itemQuantity = basket.getItemQuantity();

            if (itemQuantity <= productCount) {
                handleSufficientStock(basket, product, productCount, itemQuantity, orderDto, orderProducts, user);
            } else if (itemQuantity > productCount) {
                handleInsufficientStockError(product, productCount, orderDto, orderProducts, user);
            }
        }

        Order order = orderMapper.mapToEntity(orderDto);
        order.setUser(user);
        order.setStatus(OrderStatus.NEW);
//        order.setPaymentCompleted(order.getPaymentOption().equals(PaymentOption.CREDIT_CARD));
        order.setPaymentCompleted(true);
        orderRepository.save(order);
        log.info("The order was successfully stored in the database.");
        paymentService.addPayment(order, cardDto, user);
    }

    // 1
    private void handleSufficientStock(Basket basket, Product product, int productCount, double itemQuantity, CreateOrderDto orderDto, List<ProductOverview> orderProducts, User user) throws BasketNotFoundException, ProductNotFoundException {
        product.setProductCount(productCount - (int) itemQuantity);
        productRepository.save(product);

        while (basket.getItemQuantity() != 0) {
            orderProducts.add(productMapper.mapToDto(product));
            basketService.delete(product.getId(), user);
        }
        orderDto.setProductOverviews(orderProducts);
    }

    // 2
    private void handleInsufficientStockError(Product product, int productCount, CreateOrderDto orderDto, List<ProductOverview> orderProducts, User user) throws BasketNotFoundException, ProductNotFoundException {
        product.setProductCount(0);
        productRepository.save(product);

        for (int i = productCount; i > 0; i--) {
            orderProducts.add(productMapper.mapToDto(product));
            basketService.delete(product.getId(), user);
        }
        orderDto.setProductOverviews(orderProducts);
    }

    @Override
    public void edit(EditOrderDto orderDto, int id) throws OrderNotFoundException {
        Order order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException("Order not found."));
        if (orderDto == null) {
            throw new OrderNotFoundException("Order not found.");
        }
        updateOrderDetails(order, orderDto);
        orderRepository.save(order);
        handlePayment(order);
        log.info("The order was successfully updated in the database. Order ID: {}", order.getId());
    }

    private void updateOrderDetails(Order order, EditOrderDto orderDto) {
        order.setPaymentCompleted("true".equals(orderDto.getPaymentCompleted()));

        String address = orderDto.getAdditionalAddress();
        if (address != null) {
            order.setAdditionalAddress(address);
        }
        String phoneNumber = orderDto.getAdditionalPhone();
        if (phoneNumber != null) {
            order.setAdditionalPhone(phoneNumber);
        }
        String status = orderDto.getStatus();
        if (status != null) {
            order.setStatus(OrderStatus.valueOf(status));
        }
//        String paymentOption = orderDto.getPaymentOption();
//        if (paymentOption != null) {
//            order.setPaymentOption(PaymentOption.valueOf(paymentOption));
//        }
    }

    private void handlePayment(Order order) throws OrderNotFoundException {
        Payment payment = paymentRepository.findByOrder(order);

        if (order.isPaymentCompleted() && order.getStatus() == OrderStatus.DELIVERED) {
            order.getProducts().clear();
            paymentRepository.delete(payment);
        }

        if (payment != null) {
            if (order.isPaymentCompleted()) {
                payment.setStatus(PaymentStatus.PAID);
            }
            if (order.getStatus() == OrderStatus.CANCELLED && order.isPaymentCompleted() && payment.getStatus() == PaymentStatus.PAID) {
                throw new OrderNotFoundException("Sorry, but you've already paid for the order, and it cannot be canceled.");
            } else {
                payment.setStatus(PaymentStatus.UNPAID);
            }
            paymentRepository.save(payment);
        }
    }

    @Override
    public void delete(int id) throws OrderNotFoundException {
        if (!orderRepository.existsById(id)) {
            log.debug("Order with ID {} not found.", id);
            throw new OrderNotFoundException("Order not found.");
        }
        orderRepository.deleteById(id);
        log.info("The order with ID {} has been successfully deleted.", id);
    }
}
