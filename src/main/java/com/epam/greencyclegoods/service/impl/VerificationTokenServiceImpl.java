package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.entity.VerificationToken;
import com.epam.greencyclegoods.exception.VerificationException;
import com.epam.greencyclegoods.repository.VerificationTokenRepository;
import com.epam.greencyclegoods.service.VerificationTokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class VerificationTokenServiceImpl implements VerificationTokenService {

    private final VerificationTokenRepository tokenRepository;

    @Override
    public VerificationToken create(User user) {
        log.info("Token successfully created");
        return tokenRepository.save(VerificationToken.builder()
                .plainToken(UUID.randomUUID().toString())
                .expiresAt(LocalDateTime.now().plus(12, ChronoUnit.HOURS))
                .user(user)
                .build());
    }

    @Override
    public VerificationToken find(String plainToken) throws VerificationException {
        log.info("Finding token for plainToken: {}", plainToken);
        Optional<VerificationToken> tokenOptional = tokenRepository.findByPlainToken(plainToken);
        if (tokenOptional.isEmpty()) {
            log.error("Token not found: {}", plainToken);
            throw new VerificationException("Verification failed: Token not found.");
        }
        VerificationToken token = tokenOptional.get();
        if (token.getExpiresAt().isBefore(LocalDateTime.now())) {
            log.error("Token expired: {}", plainToken);
            delete(token);
            throw new VerificationException("Verification failed: Token expired.");
        }
        log.info("Token successfully found: {}", token);
        return token;
    }

    @Override
    public void delete(VerificationToken token) {
        log.info("Deleting token: {}", token);
        tokenRepository.delete(token);
        log.info("The token has been successfully deleted");
    }
}
