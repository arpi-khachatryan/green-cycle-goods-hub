package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.exception.EmailSendingException;
import com.epam.greencyclegoods.service.MailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private final JavaMailSender javaMailSender;

    @Async
    @Override
    public void sendEmail(String to, String subject, String text) throws EmailSendingException {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, false);
            messageHelper.setTo(to);
            messageHelper.setSubject(subject);
            messageHelper.setText(text);
            javaMailSender.send(mimeMessage);
            log.info("Email sent successfully to {} with subject: '{}'", to, subject);
        } catch (MessagingException e) {
            log.error("Failed to send email.", e);
            throw new EmailSendingException("Failed to send email.");
        }
    }
}
