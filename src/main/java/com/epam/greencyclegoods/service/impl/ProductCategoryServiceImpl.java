package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.productCategory.CreateProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.EditProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.ProductCategoryOverview;
import com.epam.greencyclegoods.entity.ProductCategory;
import com.epam.greencyclegoods.exception.CategoryDeleteException;
import com.epam.greencyclegoods.exception.CategoryNotFoundException;
import com.epam.greencyclegoods.exception.CategoryUpdateException;
import com.epam.greencyclegoods.exception.ProductCategorySaveException;
import com.epam.greencyclegoods.mapper.ProductCategoryMapper;
import com.epam.greencyclegoods.repository.ProductCategoryRepository;
import com.epam.greencyclegoods.repository.ProductRepository;
import com.epam.greencyclegoods.service.ProductCategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductCategoryServiceImpl implements ProductCategoryService {

    private final ProductCategoryMapper productCategoryMapper;
    private final ProductCategoryRepository productCategoryRepository;
    private final ProductRepository productRepository;

    @Override
    public Page<ProductCategoryOverview> categories(Pageable pageable) {
        Page<ProductCategory> categories = productCategoryRepository.findAll(pageable);
        if (categories.isEmpty()) {
            log.info("No categories found.");
            return Page.empty();
        }
        log.info("Categories successfully found.");
        return categories.map(productCategoryMapper::mapToDto);
    }

    @Override
    public List<ProductCategoryOverview> categories() {
        List<ProductCategory> categories = productCategoryRepository.findAll();
        if (categories.isEmpty()) {
            log.info("Category not found");
            return Collections.emptyList();
        }
        log.info("Category successfully found");
        return productCategoryMapper.mapToDtoList(categories);
    }

    @Override
    public ProductCategoryOverview categoryById(int id) throws CategoryNotFoundException {
        ProductCategory productCategory = productCategoryRepository.findById(id).orElseThrow(() ->
                new CategoryNotFoundException("No category found."));
        log.info("Category successfully found");
        return productCategoryMapper.mapToDto(productCategory);
    }

    @Override
    public void save(CreateProductCategoryDto dto) throws ProductCategorySaveException {
        try {
            if (dto != null && StringUtils.hasText(dto.getName())) {
                log.info("Attempting to save the category: {}", dto.getName());
                ProductCategory productCategory = productCategoryMapper.mapToEntity(dto);
                productCategoryRepository.save(productCategory);
                log.info("The category was successfully saved in the database: {}", dto.getName());
            } else {
                log.warn("Failed to save category - missing or empty name.");
            }
        } catch (DataAccessException e) {
            log.error("Error while saving category: " + e.getMessage());
            throw new ProductCategorySaveException("Failed to save the product category.");
        }
    }

    @Override
    public void edit(EditProductCategoryDto dto, int id) throws CategoryUpdateException, CategoryNotFoundException {
        Optional<ProductCategory> productCategoryOptional = productCategoryRepository.findById(id);
        if (productCategoryOptional.isEmpty()) {
            log.info("No category found.");
            throw new CategoryNotFoundException("No category found.");
        }

        String newName = dto.getName();
        if (productCategoryRepository.existsByNameIgnoreCase(newName)) {
            throw new CategoryUpdateException("Category with the name " + newName + " already exists.");
        }

        ProductCategory productCategory = productCategoryOptional.get();

        if (StringUtils.hasText(newName) && !productCategory.getName().equals(newName)) {
            productCategory.setName(newName);
            log.info("Updating category name to: {}", newName);
            productCategoryRepository.save(productCategory);
        } else {
            log.warn("Failed to update category - new name is missing, empty, or already exists.");
            throw new CategoryUpdateException("Invalid new name or category with the same name already exists.");
        }
    }

    @Override
    public void delete(int id) throws CategoryNotFoundException, CategoryDeleteException {
        if (productCategoryRepository.existsById(id)) {
            boolean productExists = productRepository.existsByProductCategoryId(id);
            if (!productExists) {
                productCategoryRepository.deleteById(id);
                log.info("Category with ID {} has been successfully deleted", id);
            } else {
                log.error("Category cannot be deleted because there are associated products");
                throw new CategoryDeleteException("Category cannot be deleted because there are associated products.");
            }
        } else {
            log.info("Category with ID {} not found", id);
            throw new CategoryNotFoundException("Category not found.");
        }
    }
}