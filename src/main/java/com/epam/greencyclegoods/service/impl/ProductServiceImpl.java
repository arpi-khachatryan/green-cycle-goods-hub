package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.product.CreateProductDto;
import com.epam.greencyclegoods.dto.product.EditProductDto;
import com.epam.greencyclegoods.dto.product.ProductOverview;
import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.exception.ImageUploadException;
import com.epam.greencyclegoods.exception.PermissionDeniedException;
import com.epam.greencyclegoods.exception.ProductCreationException;
import com.epam.greencyclegoods.exception.ProductNotFoundException;
import com.epam.greencyclegoods.mapper.ProductMapper;
import com.epam.greencyclegoods.repository.*;
import com.epam.greencyclegoods.service.ProductService;
import com.epam.greencyclegoods.utility.ImageUtility;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ImageUtility imageUtility;
    private final ProductMapper productMapper;
    private final ProductRepository productRepository;
    private final CompanyRepository companyRepository;
    private final ProductCategoryRepository productCategoryRepository;
    private final OrderRepository orderRepository;
    private final BasketRepository basketRepository;

    @Override
    public Page<ProductOverview> sort(Pageable pageable, String sort, Integer id) {
        Page<Product> products;
        if (id != null) {
            products = productRepository.findProductsByProductCategory_Id(id, pageable);
        } else {
            products = switch (sort) {
                case "price_asc" -> productRepository.findByOrderByPriceAsc(pageable);
                case "price_desc" -> productRepository.findByOrderByPriceDesc(pageable);
                default -> productRepository.findAll(pageable);
            };
        }
        if (products.isEmpty()) {
            log.info("No products found.");
        } else {
            log.info("Products successfully found.");
        }
        return products.map(productMapper::mapToDto);
    }

    @Override
    public List<ProductOverview> products() {
        List<Product> products = productRepository.findAll();
        if (products.isEmpty()) {
            log.info("Products not found");
            return Collections.emptyList();
        }
        log.info("Products successfully found");
        return productMapper.mapToDtoList(products);
    }

    @Override
    public List<ProductOverview> userProducts(User user) {
        List<Product> products = productRepository.findProductByUser(user);
        if (products.isEmpty()) {
            log.info("Products not found");
            return Collections.emptyList();
        } else {
            log.info("Products successfully found");
        }
        return productMapper.mapToDtoList(products);
    }

    @Override
    public void save(CreateProductDto dto, MultipartFile[] files, User user) throws ProductCreationException, ImageUploadException {
        try {
            if (StringUtils.hasText(dto.getName()) && dto.getPrice() >= 0) {
                Product product = productMapper.mapToEntity(dto);
                product.setUser(user);

                if (files != null && files.length > 0 && !files[0].isEmpty()) {
                    try {
                        product.setPictures(imageUtility.uploadImages(files));
                    } catch (IOException e) {
                        log.error("IOException during image upload: {}", e.getMessage());
                        throw new ImageUploadException("Failed to upload product images.");
                    }
                }

                productRepository.save(product);
                log.info("The product was successfully stored in the database {}", dto.getName());
            }
        } catch (Exception e) {
            log.error("Error during product creation: {}", e.getMessage());
            throw new ProductCreationException("Failed to save the product. Please try again.");
        }
    }

    @Override
    public void edit(EditProductDto dto, int id, MultipartFile[] files) throws ProductNotFoundException, ImageUploadException, PermissionDeniedException {
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isEmpty()) {
            log.info("No product found with ID: {}", id);
            throw new ProductNotFoundException("No product found.");
        }

        Product product = productOptional.get();

        Integer minProductCount = basketRepository.minProductCountInAllBaskets(product.getId());

        if (dto.getProductCount() >= 1) {
            if (minProductCount != null && minProductCount >= 1) {
                if (dto.getProductCount() < minProductCount) {
                    throw new PermissionDeniedException("This product is in a basket. You can only update the product count to a lower value.");
                } else {
                    product.setProductCount(dto.getProductCount());
                }
            } else {
                product.setProductCount(dto.getProductCount());
            }
        }

        productRepository.save(product);

        checkProductAssociations(product.getId());

        if (StringUtils.hasText(dto.getName())) {
            product.setName(dto.getName());
        }
        if (StringUtils.hasText(dto.getDescription())) {
            product.setDescription(dto.getDescription());
        } else {
            product.setDescription(null);
        }
        if (dto.getPrice() >= 0) {
            product.setPrice(dto.getPrice());
        }
        if (dto.getCompanyId() != null) {
            Optional<Company> companyOptional = companyRepository.findById(dto.getCompanyId());
            companyOptional.ifPresent(product::setCompany);
        }
        if (dto.getProductCategoryId() != null) {
            Optional<ProductCategory> categoryOptional = productCategoryRepository.findById(dto.getProductCategoryId());
            categoryOptional.ifPresent(product::setProductCategory);
        }
        if (files != null && files.length > 0 && !files[0].isEmpty()) {
            try {
                product.setPictures(imageUtility.uploadImages(files));
            } catch (IOException e) {
                log.error("IOException during image upload: {}", e.getMessage());
                throw new ImageUploadException("Failed to upload product images.");
            }
        }
        log.info("Product with ID {} was successfully updated.", id);
        productRepository.save(product);
    }

    @Override
    public byte[] getProductImage(String fileName) {
        try {
            log.info("Image successfully found");
            return imageUtility.getImageContent(fileName);
        } catch (IOException e) {
            log.error("Error retrieving product image: {}", e.getMessage(), e);
            throw new IllegalStateException("Failed to retrieve product image. Please try again.");
        }
    }

    @Override
    public ProductOverview productById(int productId) throws ProductNotFoundException {
        Product product = productRepository.findById(productId).orElseThrow(() ->
                new ProductNotFoundException("No product found."));
        log.info("Product successfully found");
        return productMapper.mapToDto(product);
    }

    @Override
    public void delete(int productId, User user) throws ProductNotFoundException, PermissionDeniedException {
        Product product = productRepository.findById(productId).orElseThrow(() ->
                new ProductNotFoundException("No product found."));
        if (user.getRole() == Role.ADMIN || user.getId().equals(product.getUser().getId())) {

            checkProductAssociations(productId);

            productRepository.deleteById(productId);
            log.info("The product has been successfully deleted");
        } else {
            throw new PermissionDeniedException("You do not have permission to delete this product.");
        }
    }

    @Override
    public List<ProductOverview> companyProducts(int id) {
        List<Product> products = productRepository.findProductsByCompany_Id(id);
        if (products.isEmpty()) {
            log.info("No products found for company with ID: {}", id);
            return Collections.emptyList();
        }
        log.info("Products successfully found for company with ID: {}", id);
        return productMapper.mapToDtoList(products);
    }

    private void checkProductAssociations(int productId) throws PermissionDeniedException {
        boolean orderExists = orderRepository.existsByProduct(productId);
        boolean basketExists = basketRepository.existsByProduct(productId);

        if (orderExists && basketExists) {
            throw new PermissionDeniedException("This product is in both an order and a basket. You can only update the product count.");
        }
        if (orderExists) {
            throw new PermissionDeniedException("This product is in an order. You can only update the product count.");
        }
        if (basketExists) {
            throw new PermissionDeniedException("This product is in a basket.You can only update the product count.");
        }
    }
}
