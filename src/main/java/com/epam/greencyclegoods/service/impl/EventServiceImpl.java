package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.event.CreateEventDto;
import com.epam.greencyclegoods.dto.event.EditEventDto;
import com.epam.greencyclegoods.dto.event.EventOverview;
import com.epam.greencyclegoods.entity.*;
import com.epam.greencyclegoods.exception.EmailSendingException;
import com.epam.greencyclegoods.exception.EventDeleteException;
import com.epam.greencyclegoods.exception.EventNotFoundException;
import com.epam.greencyclegoods.exception.ImageUploadException;
import com.epam.greencyclegoods.mapper.EventMapper;
import com.epam.greencyclegoods.repository.CompanyRepository;
import com.epam.greencyclegoods.repository.EventRepository;
import com.epam.greencyclegoods.repository.RegistrationRepository;
import com.epam.greencyclegoods.service.EventService;
import com.epam.greencyclegoods.service.MailService;
import com.epam.greencyclegoods.utility.ImageUtility;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final ImageUtility imageUtility;
    private final EventMapper eventMapper;
    private final EventRepository eventRepository;
    private final CompanyRepository companyRepository;
    private final RegistrationRepository registrationRepository;
    private final MailService mailService;

    @Override
    public Page<EventOverview> events(Pageable pageable) {
        Page<Event> events = eventRepository.findAll(pageable);
        if (events.isEmpty()) {
            log.info("No Events found.");
            return Page.empty();
        }
        log.info("Events successfully found");
        return events.map(eventMapper::mapToDto);
    }

    @Override
    public Map<Integer, List<EventOverview>> sortByCompany() {
        Map<Integer, List<EventOverview>> events = new HashMap<>();
        List<Company> companies = companyRepository.findAll();
        for (Company company : companies) {
            if (company != null) {
                List<EventOverview> eventsByCompany = eventMapper.mapToDtoList(eventRepository.findEventsByCompany_Id(company.getId()));
                events.put(company.getId(), eventsByCompany);
            }
        }
        if (events.isEmpty()) {
            log.info("No events found for any company.");
            return Collections.emptyMap();
        }
        log.info("Events successfully grouped by company.");
        return events;
    }

    @Override
    public Page<EventOverview> eventsByUser(User user, Pageable pageable) {
        Page<Event> events = eventRepository.findAll(pageable);
        if (events.isEmpty()) {
            log.info("No events found for user.");
            return Page.empty();
        }
        Page<Event> eventsByUser;
        if (user.getRole() == Role.OWNER) {
            List<Event> filteredEvents = events
                    .stream()
                    .filter(event -> Objects.equals(user.getEmail(), event.getCompany().getUser().getEmail()))
                    .collect(Collectors.toList());
            eventsByUser = new PageImpl<>(filteredEvents, pageable, filteredEvents.size());
        } else {
            eventsByUser = events;
        }
        return eventsByUser.map(eventMapper::mapToDto);
    }

    @Override
    public EventOverview eventById(int id) throws EventNotFoundException {
        Optional<Event> optionalEvent = eventRepository.findById(id);
        if (optionalEvent.isEmpty()) {
            log.info("No event found with ID {}", id);
            throw new EventNotFoundException("Event not found.");
        }
        Event event = optionalEvent.get();
        log.info("Event found with ID {}", event.getId());
        return eventMapper.mapToDto(event);
    }

    @Override
    public List<EventOverview> companyEvents(int id) {
        List<Event> events = eventRepository.findEventsByCompany_Id(id);
        if (events.isEmpty()) {
            log.info("No events found for company with ID: {}", id);
            return Collections.emptyList();
        }
        return eventMapper.mapToDtoList(events);
    }

    @Override
    public void save(CreateEventDto dto, MultipartFile[] files) throws ImageUploadException {
        try {
            List<String> images = imageUtility.uploadImages(files);
            dto.setPictures(images);

            Event event = eventMapper.mapToEntity(dto);
            eventRepository.save(event);

            log.info("The event with ID {} was successfully stored in the database", event.getId());
        } catch (IOException e) {
            log.error("Error uploading images for event: {}", e.getMessage());
            throw new ImageUploadException("Failed to upload event images.");
        }
    }

    @Override
    public void edit(EditEventDto dto, int id, MultipartFile[] files) throws EventNotFoundException, ImageUploadException {
        Event event = eventRepository.findById(id).orElseThrow(() ->
                new EventNotFoundException("Event not found."));
        String name = dto.getName();
        if (StringUtils.hasText(name)) {
            event.setName(name);
        }
        String description = dto.getDescription();
        if (StringUtils.hasText(description)) {
            event.setDescription(description);
        } else {
            event.setDescription(null);
        }
        double price = dto.getPrice();
        if (price >= 0) {
            event.setPrice(price);
        }
        LocalDateTime eventDateTime = dto.getEventDateTime();
        if (eventDateTime != null) {
            event.setEventDateTime(eventDateTime);
        }
        Integer companyId = dto.getCompanyId();
        if (companyId != null) {
            Optional<Company> optionalCompany = companyRepository.findById(companyId);
            optionalCompany.ifPresent(event::setCompany);
        }
        List<String> pictures = null;
        try {
            if (!files[0].isEmpty()) {
                pictures = imageUtility.uploadImages(files);
            } else if (dto.getPictures() != null) {
                pictures = dto.getPictures();
            } else if (event.getPictures() != null) {
                pictures = event.getPictures();
            } else {
                pictures = Collections.emptyList();
            }

        } catch (IOException e) {
            throw new ImageUploadException("Failed to upload event images.");
        }
        event.setPictures(pictures);
        eventRepository.save(event);
        log.info("Event with ID {} was successfully updated in the database", id);
    }

    @Override
    public byte[] getEventImage(String fileName) {
        try {
            log.info("Images successfully found");
            return imageUtility.getImageContent(fileName);
        } catch (IOException e) {
            log.error("Error retrieving event image: {}", e.getMessage(), e);
            throw new IllegalStateException("Failed to retrieve event image. Please try again.");
        }
    }

    @Override
    @Transactional
    public void delete(int id) throws EventNotFoundException, EventDeleteException, EmailSendingException {
        Event event = eventRepository.findById(id).orElseThrow(() -> new EventNotFoundException("Event not found."));

        List<Registration> registrations = registrationRepository.findByEventId(id);
        if (registrations != null && !registrations.isEmpty()) {
            try {
                deleteRegistrations(registrations);
                log.info("Deleted {} registrations for event with ID {}", registrations.size(), id);
            } catch (RuntimeException e) {
                log.error("Failed to delete registrations for event with ID {}", id, e);
                throw new EventDeleteException("Failed to delete registrations for event");
            }

            for (Registration registration : registrations) {
                User user = registration.getUser();

                try {
                    sendCancellationEmail(user, registration);
                    log.info("Cancellation email sent to user with ID {} for event with ID {}", user.getId(), registration.getEvent().getId());
                } catch (EmailSendingException e) {
                    log.error("Failed to send cancellation email to user with ID {} for event with ID {}", user.getId(), registration.getEvent().getId(), e);
                    throw new EmailSendingException("Failed to send cancellation email.");
                }
            }
        }

        List<String> pictures = event.getPictures();
        if (pictures != null) {
            pictures.clear();
            eventRepository.save(event);
            log.info("Pictures for event with ID {} cleared", id);
        }

        eventRepository.delete(event);
        log.info("Event with ID {} deleted, including {} registrations and associated pictures", id, registrations);
    }

    private void deleteRegistrations(List<Registration> registrations) {
        List<Integer> registrationIdsToDelete = registrations.stream()
                .map(Registration::getId)
                .collect(Collectors.toList());
        registrationRepository.deleteAllByIdIn(registrationIdsToDelete);
        log.info("Deleted {} registrations", registrations.size());
    }

    private void sendCancellationEmail(User user, Registration registration) throws EmailSendingException {
        String eventName = registration.getEvent().getName();
        String eventDate = registration.getEvent().getEventDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        String companyName = registration.getEvent().getCompany().getName();

        String emailSubject = "Event Cancellation: " + eventName;
        String emailMessage = "Hi " + user.getFirstName() + " " + user.getLastName() + ",\n\n"
                + "We regret to inform you that the event \"" + eventName + "\" scheduled for " + eventDate
                + " hosted by " + companyName + " has been canceled. We sincerely apologize for any inconvenience this may cause.\n\n"
                + "If you have any questions or concerns, please contact our support team at info@greenCycleGoods.com.\n\n"
                + "Thank you for your understanding.";
        mailService.sendEmail(user.getEmail(), emailSubject, emailMessage);
    }

    @Override
    public EventOverview getEvent(int id) throws EventNotFoundException {
        Event event = eventRepository.findById(id).orElseThrow(() -> new EventNotFoundException("Event not found."));
        log.info("Event successfully found: {}", event.getName());
        return eventMapper.mapToDto(event);
    }

    @Override
    public List<EventOverview> events() {
        List<Event> events = eventRepository.findAll();
        if (events.isEmpty()) {
            return Collections.emptyList();
        }
        log.info("Event successfully found");
        return eventMapper.mapToDtoList(events);
    }
}

