package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.registration.CreateRegistrationDto;
import com.epam.greencyclegoods.dto.registration.EditRegistrationDto;
import com.epam.greencyclegoods.dto.registration.RegistrationOverview;
import com.epam.greencyclegoods.entity.Registration;
import com.epam.greencyclegoods.entity.RegistrationStatus;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.RegistrationCreationException;
import com.epam.greencyclegoods.exception.RegistrationNotFoundException;
import com.epam.greencyclegoods.mapper.RegistrationMapper;
import com.epam.greencyclegoods.repository.RegistrationRepository;
import com.epam.greencyclegoods.service.RegistrationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

    private final RegistrationMapper registrationMapper;
    private final RegistrationRepository registrationRepository;

    @Override
    public Page<RegistrationOverview> registrations(Pageable pageable) {
        Page<Registration> registrations = registrationRepository.findAll(pageable);
        return processRegistrationList(registrations);
    }

    @Override
    public Page<RegistrationOverview> registrationsByUser(int id, Pageable pageable) {
        Page<Registration> registrations = registrationRepository.findByUserId(id, pageable);
        return processRegistrationList(registrations);
    }

    @Override
    public Page<RegistrationOverview> registrationsByOwner(User user, Pageable pageable) throws RegistrationNotFoundException {
        try {
            Page<Registration> registrations = registrationRepository.findRegistrationsByCompanyOwner(user.getId(), pageable);
            return processRegistrationList(registrations);
        } catch (PersistenceException e) {
            log.error("Error fetching registrations: {}", e.getMessage());
            throw new RegistrationNotFoundException("No registrations found.");
        }
    }

    private Page<RegistrationOverview> processRegistrationList(Page<Registration> registrations) {
        if (registrations.isEmpty()) {
            log.info("No registrations found");
            return Page.empty();
        }
        log.info("Registrations successfully found");
        return registrations.map(registrationMapper::mapToDto);
    }

    @Override
    public RegistrationOverview registrationById(int id) throws RegistrationNotFoundException {
        Registration registration = registrationRepository.findById(id).orElseThrow(() ->
                new RegistrationNotFoundException("No registrations found."));
        log.info("Registration successfully found.");
        return registrationMapper.mapToDto(registration);
    }

    @Override
    public void edit(EditRegistrationDto dto, int id) throws RegistrationNotFoundException {
        Registration registration = registrationRepository.findById(id).orElseThrow(() -> new RegistrationNotFoundException("No registrations found."));
        int peopleCount = dto.getPeopleCount();
        if (peopleCount > 0) {
            registration.setPeopleCount(peopleCount);
        }
        String status = dto.getStatus();
        if (status != null) {
            registration.setStatus(RegistrationStatus.valueOf(status));
        }
        log.info("The registration with ID {} was successfully updated in the database.", registration.getId());
        registrationRepository.save(registration);
    }

    @Override
    public void delete(int id) throws RegistrationNotFoundException {
        if (registrationRepository.existsById(id)) {
            registrationRepository.deleteById(id);
            log.info("The registration with ID {} has been successfully deleted", id);
        } else {
            log.info("Registration with ID {} not found", id);
            throw new RegistrationNotFoundException("No registration found.");
        }
    }

    @Override
    public void addRegistration(CreateRegistrationDto dto, User user) throws RegistrationCreationException {
        if (user == null) {
            log.error("Attempted to add a registration without a valid user.");
            throw new RegistrationCreationException("Please take a moment to register on our website. Once registered, you'll have the opportunity to sign up for the event seamlessly. Thank you for joining us.");
        }

        Optional<Registration> registrationByUserAndEvent = registrationRepository.findRegistrationByUserAndEvent(user, dto.getEventId());
        if (registrationByUserAndEvent.isPresent()) {
            throw new RegistrationCreationException("You are already registered for this event.");
        }

        try {
            Registration registration = registrationMapper.mapToEntity(dto);
            registration.setUser(user);
            registration.setStatus(RegistrationStatus.PENDING);
            registrationRepository.save(registration);
            log.info("Registration successfully added to the database. User ID: {}, Event ID: {}", user.getId(), dto.getEventId());
        } catch (DataAccessException e) {
            log.error("Error adding registration to the database: {}", e.getMessage());
            throw new RegistrationCreationException("Failed to add the registration. Please make sure you have provided valid information and try again.");
        }
    }
}
