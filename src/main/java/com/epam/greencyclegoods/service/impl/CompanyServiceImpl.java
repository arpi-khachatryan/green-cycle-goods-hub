package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.company.CompanyOverview;
import com.epam.greencyclegoods.dto.company.CreateCompanyDto;
import com.epam.greencyclegoods.dto.company.EditCompanyDto;
import com.epam.greencyclegoods.entity.Company;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.mapper.CompanyMapper;
import com.epam.greencyclegoods.repository.*;
import com.epam.greencyclegoods.service.CompanyService;
import com.epam.greencyclegoods.utility.ImageUtility;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final ImageUtility imageUtility;
    private final CompanyMapper companyMapper;
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final CompanyCategoryRepository companyCategoryRepository;
    private final ProductRepository productRepository;
    private final EventRepository eventRepository;

    @Override
    public Page<CompanyOverview> companies(Pageable pageable) {
        Page<Company> companies = companyRepository.findAll(pageable);
        if (companies.isEmpty()) {
            log.info("No companies found.");
            return Page.empty();
        } else {
            log.info("Companies successfully found");
            return companies.map(companyMapper::mapToDto);
        }
    }

    @Override
    public Page<CompanyOverview> companiesByUser(User user, Pageable pageable) throws CompanyNotFoundException {
        try {
            Page<Company> companies = companyRepository.findCompanyByUserId(user.getId(), pageable);
            if (companies.isEmpty()) {
                throw new CompanyNotFoundException("No companies found for the user.");
            }
            log.info("Company successfully found");
            return companies.map(companyMapper::mapToDto);
        } catch (DataAccessException ex) {
            log.error("Error while fetching companies for user: {}", ex.getMessage());
            throw new CompanyNotFoundException("No companies found for the user.");
        }
    }

    @Override
    public CompanyOverview company(int id) throws CompanyNotFoundException {
        Optional<Company> companyOptional = companyRepository.findById(id);
        Company company = companyOptional.orElseThrow(() -> new CompanyNotFoundException("Company not found."));
        log.info("Company successfully found with ID: {}", id);
        return companyMapper.mapToDto(company);
    }

    @Override
    public List<CompanyOverview> companies() {
        List<Company> companies = companyRepository.findAll();
        if (companies.isEmpty()) {
            log.info("Companies not found.");
            return Collections.emptyList();
        }
        log.info("Companies successfully found.");
        return companyMapper.mapToDtoList(companies);
    }

    @Override
    public void edit(EditCompanyDto dto, int id, MultipartFile[] files) throws CompanyNotFoundException, ImageUploadException {
        try {
            Company company = companyRepository.findById(id).orElseThrow(() -> new CompanyNotFoundException("Failed to update the company information."));
            String name = dto.getName();
            if (StringUtils.hasText(name)) {
                company.setName(name);
            }
            String email = dto.getEmail();
            if (StringUtils.hasText(email)) {
                company.setEmail(email);
            }
            String phone = dto.getPhone();
            if (StringUtils.hasText(phone)) {
                company.setPhone(phone);
            }
            String address = dto.getAddress();
            if (StringUtils.hasText(address)) {
                company.setAddress(address);
            } else {
                company.setAddress(null);
            }
            Double deliveryPrice = dto.getDeliveryPrice();
            if (deliveryPrice != null) {
                company.setDeliveryPrice(deliveryPrice);
            }
            Integer companyCategoryId = dto.getCompanyCategoryId();
            if (companyCategoryId != null) {
                company.setCompanyCategory(companyCategoryRepository.getReferenceById(companyCategoryId));
            }
            if (files != null && files.length > 0 && !files[0].isEmpty()) {
                try {
                    company.setPictures(imageUtility.uploadImages(files));
                } catch (IOException e) {
                    throw new ImageUploadException("Failed to upload company images.");
                }
            }
            log.info("The company was successfully stored in the database {}", company.getName());
            companyRepository.save(company);
        } catch (Exception e) {
            log.error("An error occurred while updating the company with ID " + id, e);
            throw new CompanyNotFoundException("Failed to update the company information.");
        }
    }

    @Override
    public void add(CreateCompanyDto dto, MultipartFile[] files, User user) throws UserUpdateException, ImageUploadException, CompanyCreationException {
        if (companyRepository.existsByEmailIgnoreCase(dto.getEmail())) {
            log.info("Company with that email already exists: {}", dto.getName());
            throw new CompanyCreationException("Please use a different email address.");
        }

        try {
            Optional<User> currentUser = userRepository.findById(user.getId());
            if (currentUser.isPresent()) {
                User companyOwner = currentUser.get();
                if (companyOwner.getRole() == Role.CUSTOMER) {
                    companyOwner.setRole(Role.OWNER);
                    userRepository.save(companyOwner);
                }
            } else {
                throw new UserUpdateException("Failed to save the company.");
            }
        } catch (DataAccessException e) {
            throw new UserUpdateException("Failed to save the company.");
        }

        try {
            dto.setPictures(imageUtility.uploadImages(files));
            Company company = companyMapper.mapToEntity(dto);
            company.setUser(user);
            companyRepository.save(company);
            log.info("The company was successfully stored in the database: {}", dto.getName());
        } catch (IOException e) {
            throw new ImageUploadException("Failed to upload company images.");
        } catch (DataAccessException e) {
            throw new CompanyCreationException("Failed to save the company.");
        }
    }

    @Override
    public byte[] getCompanyImage(String fileName) {
        try {
            log.info("Images successfully found");
            return imageUtility.getImageContent(fileName);
        } catch (IOException e) {
            log.error("Error retrieving company image: {}", e.getMessage(), e);
            throw new IllegalStateException("Failed to retrieve company image. Please try again.");
        }
    }

    @Override
    public void delete(int id) throws CompanyNotFoundException, CompanyDeleteException {
        Optional<Company> companyOptional = companyRepository.findById(id);
        if (companyOptional.isPresent()) {

            boolean productExistsByCompany = productRepository.existsByCompanyId(id);
            boolean eventExistsByCompany = eventRepository.existsByCompanyId(id);

            if (productExistsByCompany || eventExistsByCompany) {
                String errorMessage = "Cannot delete company because there are associated ";
                if (productExistsByCompany && eventExistsByCompany) {
                    errorMessage += "products and events.";
                } else if (productExistsByCompany) {
                    errorMessage += "products.";
                } else {
                    errorMessage += "events.";
                }

                log.error("Error deleting company with ID {}.", id);
                throw new CompanyDeleteException(errorMessage);
            } else {
                try {
                    log.info("Deleting company with ID {}", id);
                    companyRepository.deleteById(id);
                    log.info("The company with ID {} has been successfully deleted", id);
                } catch (DataAccessException e) {
                    log.error("Error deleting company with ID {}: {}", id, e.getMessage());
                    throw new CompanyDeleteException("Failed to delete company.");
                }
            }
        } else {
            log.info("Company with ID {} not found", id);
            throw new CompanyNotFoundException("Failed to delete company.");
        }
    }

    @Override
    public List<CompanyOverview> companiesByUser(User user) {
        List<Company> companies;
        if (user.getRole() == Role.ADMIN) {
            companies = companyRepository.findAll();
            log.info("All companies retrieved for user with ADMIN role.");
        } else {
            companies = companyRepository.findCompaniesByUser(user);
            log.info("Companies retrieved for user with non-ADMIN role (User ID: {}).", user.getId());
        }
        return companyMapper.mapToDtoList(companies);
    }
}
