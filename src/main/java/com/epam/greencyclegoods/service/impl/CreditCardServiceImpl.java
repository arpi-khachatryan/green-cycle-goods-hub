package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.entity.CreditCard;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.CreditCardNotFoundException;
import com.epam.greencyclegoods.exception.UserNotFoundException;
import com.epam.greencyclegoods.mapper.CreditCardMapper;
import com.epam.greencyclegoods.repository.CreditCardRepository;
import com.epam.greencyclegoods.service.CreditCardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class CreditCardServiceImpl implements CreditCardService {

    private final CreditCardMapper creditCardMapper;
    private final CreditCardRepository creditCardRepository;

    @Override
    public void addCreditCard(CreateCreditCardDto cardDto, User user) throws CreditCardNotFoundException, UserNotFoundException {
        if (cardDto == null) {
            throw new CreditCardNotFoundException("Credit card data is required.");
        }
        if (user == null) {
            throw new UserNotFoundException("User not found.");
        }

        CreditCard creditCard = creditCardMapper.mapToEntity(cardDto);
        creditCard.setUser(user);
        creditCardRepository.save(creditCard);
        log.info("The credit card was successfully stored in the database {}", creditCard.getCardHolder());
    }
}
