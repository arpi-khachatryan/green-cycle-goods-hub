package com.epam.greencyclegoods.service.impl;

import com.epam.greencyclegoods.dto.companyCategory.CompanyCategoryOverview;
import com.epam.greencyclegoods.dto.companyCategory.CreateCompanyCategoryDto;
import com.epam.greencyclegoods.entity.CompanyCategory;
import com.epam.greencyclegoods.exception.CategoryCreationException;
import com.epam.greencyclegoods.exception.CategoryDeleteException;
import com.epam.greencyclegoods.exception.CategoryNotFoundException;
import com.epam.greencyclegoods.mapper.CompanyCategoryMapper;
import com.epam.greencyclegoods.repository.CompanyCategoryRepository;
import com.epam.greencyclegoods.repository.CompanyRepository;
import com.epam.greencyclegoods.service.CompanyCategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyCategoryServiceImpl implements CompanyCategoryService {

    private final CompanyCategoryMapper categoryMapper;
    private final CompanyCategoryRepository companyCategoryRepository;
    private final CompanyRepository companyRepository;

    @Override
    public Page<CompanyCategoryOverview> categories(Pageable pageable) {
        List<CompanyCategoryOverview> companyCategoryOverviews = categoryMapper.mapToDtoList(companyCategoryRepository.findAll());
        if (companyCategoryOverviews.isEmpty()) {
            log.info("No categories found.");
            return Page.empty();
        } else {
            log.info("Categories successfully found.");
            return new PageImpl<>(companyCategoryOverviews);
        }
    }

    @Override
    public void add(CreateCompanyCategoryDto dto) throws CategoryCreationException {
        boolean categoryExists = companyCategoryRepository.existsByName(dto.getName());

        if (categoryExists) {
            log.error("Failed to add category. Category with the same name already exists: {}", dto.getName());
            throw new CategoryCreationException("Category already exists.");
        } else {
            log.info("The Category was successfully stored in the database: {}", dto.getName());
            companyCategoryRepository.save(categoryMapper.mapToEntity(dto));
        }
    }

    @Override
    public List<CompanyCategoryOverview> categories() throws CategoryNotFoundException {
        try {
            List<CompanyCategory> categories = companyCategoryRepository.findAll();
            if (categories.isEmpty()) {
                log.info("No company categories found.");
                throw new CategoryNotFoundException("No company categories found.");
            }
            log.info("Categories successfully found.");
            return categoryMapper.mapToDtoList(categories);
        } catch (Exception e) {
            log.error("No company categories found.", e);
            throw new CategoryNotFoundException("No company categories found.");
        }
    }

    @Override
    public void delete(int id) throws CategoryNotFoundException, CategoryDeleteException {
        if (companyCategoryRepository.existsById(id)) {
            boolean companyExists = companyRepository.existsByCompanyCategoryId(id);
            if (!companyExists){
                companyCategoryRepository.deleteById(id);
                log.info("The category has been successfully deleted");
            }else{
                log.error("Category cannot be deleted because there are associated companies");
                throw new CategoryDeleteException("Category cannot be deleted because there are associated companies.");
            }
        } else {
            log.error("Category not found");
            throw new CategoryNotFoundException("Category not found.");
        }
    }
}

