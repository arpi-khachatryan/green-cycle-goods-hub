package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.productCategory.CreateProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.EditProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.ProductCategoryOverview;
import com.epam.greencyclegoods.exception.CategoryDeleteException;
import com.epam.greencyclegoods.exception.CategoryNotFoundException;
import com.epam.greencyclegoods.exception.CategoryUpdateException;
import com.epam.greencyclegoods.exception.ProductCategorySaveException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface ProductCategoryService {

    Page<ProductCategoryOverview> categories(Pageable pageable);

    ProductCategoryOverview categoryById(int id) throws CategoryNotFoundException;

    void save(CreateProductCategoryDto dto) throws ProductCategorySaveException;

    void edit(EditProductCategoryDto dto, int id) throws CategoryUpdateException, CategoryNotFoundException;

    void delete(int id) throws CategoryNotFoundException, CategoryDeleteException;

    List<ProductCategoryOverview> categories();
}
