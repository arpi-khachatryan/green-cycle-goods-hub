package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.user.ChangePasswordDto;
import com.epam.greencyclegoods.dto.user.CreateUserDto;
import com.epam.greencyclegoods.dto.user.EditUserDto;
import com.epam.greencyclegoods.dto.user.UserOverview;
import com.epam.greencyclegoods.exception.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface UserService {

    Page<UserOverview> users(Pageable pageable);

    UserOverview user(int id) throws UserNotFoundException;

    void save(CreateUserDto dto) throws AuthenticationException, EmailSendingException;

    void verify(String plainToken) throws VerificationException;

    void edit(EditUserDto dto, int userId) throws UserNotFoundException;

    void changePassword(Integer id, ChangePasswordDto dto) throws UserNotFoundException, PasswordChangeException;

    void delete(int id) throws UserNotFoundException;
}