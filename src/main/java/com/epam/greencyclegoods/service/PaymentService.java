package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.dto.payment.EditPaymentDto;
import com.epam.greencyclegoods.dto.payment.PaymentOverview;
import com.epam.greencyclegoods.entity.Order;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface PaymentService {

    Page<PaymentOverview> payments(Pageable pageable);

    Page<PaymentOverview> userPayments(int id, Pageable pageable);

    Page<PaymentOverview> ownerPayments(int id, Pageable pageable);

    PaymentOverview payment(int id) throws PaymentNotFoundException;

    void delete(int id) throws PaymentNotFoundException, PaymentDeleteException;

    void edit(EditPaymentDto dto, int id) throws PaymentNotFoundException, InvalidDateFormatException;

    void addPayment(Order order, CreateCreditCardDto cardDto, User user) throws OrderNotFoundException, UserNotFoundException, CreditCardNotFoundException;
}


