package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.dto.product.CreateProductDto;
import com.epam.greencyclegoods.dto.product.EditProductDto;
import com.epam.greencyclegoods.dto.product.ProductOverview;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.exception.ImageUploadException;
import com.epam.greencyclegoods.exception.PermissionDeniedException;
import com.epam.greencyclegoods.exception.ProductCreationException;
import com.epam.greencyclegoods.exception.ProductNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface ProductService {

    Page<ProductOverview> sort(Pageable pageable, String sortProduct, Integer id);

    List<ProductOverview> products();

    ProductOverview productById(int id) throws ProductNotFoundException;

    void delete(int id, User user) throws ProductNotFoundException, PermissionDeniedException;

    List<ProductOverview> userProducts(User user);

    List<ProductOverview> companyProducts(int id);

    byte[] getProductImage(String fileName);

    void edit(EditProductDto dto, int id, MultipartFile[] files) throws ProductNotFoundException, ImageUploadException, PermissionDeniedException;

    void save(CreateProductDto dto, MultipartFile[] files, User user) throws ProductCreationException, ImageUploadException;
}
