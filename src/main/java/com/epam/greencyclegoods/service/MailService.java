package com.epam.greencyclegoods.service;

import com.epam.greencyclegoods.exception.EmailSendingException;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public interface MailService {

    void sendEmail(String to, String subject, String text) throws EmailSendingException;
}