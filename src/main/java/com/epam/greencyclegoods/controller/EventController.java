package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.authn.CurrentUser;
import com.epam.greencyclegoods.dto.event.CreateEventDto;
import com.epam.greencyclegoods.dto.event.EditEventDto;
import com.epam.greencyclegoods.dto.event.EventOverview;
import com.epam.greencyclegoods.dto.registration.CreateRegistrationDto;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.service.CompanyService;
import com.epam.greencyclegoods.service.EventService;
import com.epam.greencyclegoods.service.RegistrationService;
import com.epam.greencyclegoods.utility.PaginationUtility;
import com.epam.greencyclegoods.validation.ValidationErrorMapping;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/events")
public class EventController {

    private final EventService eventService;
    private final CompanyService companyService;
    private final RegistrationService registrationService;

    @GetMapping
    public String events(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "6") int size,
                         ModelMap modelMap, Model model) {
        Page<EventOverview> events = eventService.events(PageRequest.of(page - 1, size));
        modelMap.addAttribute("events", events);
        modelMap.addAttribute("companies", companyService.companies());
        modelMap.addAttribute("eventsByCompany", eventService.sortByCompany());
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(events));

        if (model.containsAttribute("errorMessageEvent")) {
            modelMap.addAttribute("errorMessageEvent", model.getAttribute("errorMessageEvent"));
        }
        return "event/events";
    }

    @GetMapping("/events")
    public String userEvents(@RequestParam(value = "page", defaultValue = "1") int page,
                             @RequestParam(value = "size", defaultValue = "8") int size,
                             @AuthenticationPrincipal CurrentUser currentUser,
                             Model model, ModelMap modelMap) {
        Page<EventOverview> events = eventService.eventsByUser(currentUser.getUser(), PageRequest.of(page - 1, size));
        modelMap.addAttribute("events", events);
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(events));
        if (model.containsAttribute("errorMessageEdit")) {
            modelMap.addAttribute("errorMessageEdit", model.getAttribute("errorMessageEdit"));
        }
        if (model.containsAttribute("errorMessageDelete")) {
            modelMap.addAttribute("errorMessageDelete", model.getAttribute("errorMessageDelete"));
        }
        if (model.containsAttribute("updateSuccess")) {
            modelMap.addAttribute("updateSuccess", model.getAttribute("updateSuccess"));
        }
        return "admin/events";
    }

    @GetMapping("/{id}")
    public String event(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes, Model model) {
        try {
            modelMap.addAttribute("event", eventService.eventById(id));
        } catch (EventNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEvent", e.getMessage());
            return "redirect:/events";
        }
        if (model.containsAttribute("errorMessageEvent")) {
            modelMap.addAttribute("errorMessageEvent", model.getAttribute("errorMessageEvent"));
        }
        return "event/event";
    }

    @GetMapping(value = "/getImages", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getImage(@RequestParam("fileName") String fileName) {
        return eventService.getEventImage(fileName);
    }

    @GetMapping("/add")
    public String add(@AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap, Model model) {
        modelMap.addAttribute("companies", companyService.companiesByUser(currentUser.getUser()));
        if (model.containsAttribute("errorMessageSave")) {
            modelMap.addAttribute("errorMessageSave", model.getAttribute("errorMessageSave"));
        }
        return "event/addEvent";
    }

    @PostMapping("/add")
    public String add(@RequestParam("eventImage") MultipartFile[] files, @ModelAttribute @Valid CreateEventDto dto,
                      BindingResult bindingResult, @AuthenticationPrincipal CurrentUser currentUser,
                      ModelMap modelMap, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            modelMap.addAttribute("eventErrors", errors);
            modelMap.addAttribute("companies", companyService.companiesByUser(currentUser.getUser()));
            return "event/addEvent";
        }
        try {
            eventService.save(dto, files);
        } catch (ImageUploadException e) {
            redirectAttributes.addFlashAttribute("errorMessageSave", e.getMessage());
            return "redirect:/events/add";
        }
        return "redirect:/events/events";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap modelMap, @AuthenticationPrincipal CurrentUser currentUser,
                       RedirectAttributes redirectAttributes, Model model) {
        try {
            modelMap.addAttribute("event", eventService.eventById(id));
            modelMap.addAttribute("companies", companyService.companiesByUser(currentUser.getUser()));
        } catch (EventNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEdit", e.getMessage());
            return "redirect:/events/events";
        }
        if (model.containsAttribute("eventErrors")) {
            modelMap.addAttribute("eventErrors", model.getAttribute("eventErrors"));
        }
        if (model.containsAttribute("errorMessageEdit")) {
            modelMap.addAttribute("errorMessageEdit", model.getAttribute("errorMessageEdit"));
        }
        return "event/editEvent";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, @ModelAttribute @Valid EditEventDto dto, BindingResult bindingResult,
                       @RequestParam("eventImage") MultipartFile[] files, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            redirectAttributes.addFlashAttribute("eventErrors", errors);
            return "redirect:/events/edit/" + id;
        }

        try {
            eventService.edit(dto, id, files);
            redirectAttributes.addFlashAttribute("updateSuccess", "Event edited successfully");
        } catch (EventNotFoundException | ImageUploadException e) {
            redirectAttributes.addFlashAttribute("errorMessageEdit", e.getMessage());
            return "redirect:/events/edit/" + id;
        }
        return "redirect:/events/events";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        try {
            eventService.delete(id);
        } catch (EventNotFoundException | EventDeleteException | EmailSendingException e) {
            redirectAttributes.addFlashAttribute("errorMessageDelete", e.getMessage());
        }
        return "redirect:/events/events";
    }

    @GetMapping("/{id}/registration")
    public String eventRegistration(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        try {
            modelMap.addAttribute("event", eventService.getEvent(id));
        } catch (EventNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEvent", e.getMessage());
            return "redirect:/events/" + id;
        }
        return "registration/addRegistration";
    }

    @PostMapping("/{id}/registration")
    public String eventRegistration(@PathVariable("id") int id, @ModelAttribute @Valid CreateRegistrationDto dto, BindingResult bindingResult,
                                    @AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        EventOverview event;
        try {
            event = eventService.getEvent(id);
            modelMap.addAttribute("event", event);
        } catch (EventNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEvent", e.getMessage());
            return "redirect:/events/" + id;
        }

        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            modelMap.addAttribute("registrationErrors", errors);
            modelMap.addAttribute("event", event);
            return "registration/addRegistration";
        }

        if (currentUser == null) {
            modelMap.addAttribute("errorMessageRegistration", "Please take a moment to register on our website." +
                    " Once registered, you'll have the opportunity to sign up for the event seamlessly. " +
                    "Thank you for joining us.");
            return "registration/addRegistration";
        }

        try {
            registrationService.addRegistration(dto, currentUser.getUser());
        } catch (RegistrationCreationException e) {
            modelMap.addAttribute("errorMessageRegistration", e.getMessage());
            return "registration/addRegistration";
        }
        return "registration/successfulRegistration";
    }
}