package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.dto.companyCategory.CompanyCategoryOverview;
import com.epam.greencyclegoods.dto.companyCategory.CreateCompanyCategoryDto;
import com.epam.greencyclegoods.exception.CategoryCreationException;
import com.epam.greencyclegoods.exception.CategoryDeleteException;
import com.epam.greencyclegoods.exception.CategoryNotFoundException;
import com.epam.greencyclegoods.service.CompanyCategoryService;
import com.epam.greencyclegoods.utility.PaginationUtility;
import com.epam.greencyclegoods.validation.ValidationErrorMapping;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/companyCategories")
public class CompanyCategoryController {

    private final CompanyCategoryService companyCategoryService;

    @GetMapping
    public String companies(@RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "5") int size,
                            ModelMap modelMap, Model model) {
        Page<CompanyCategoryOverview> categories = companyCategoryService.categories(PageRequest.of(page - 1, size));
        modelMap.addAttribute("categories", categories);
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(categories));

        if (model.containsAttribute("errorMessageDeleteCategory")) {
            modelMap.addAttribute("errorMessageDeleteCategory", model.getAttribute("errorMessageDeleteCategory"));
        }
        return "companyCategory/companyCategory";
    }

    @GetMapping("/add")
    public String add() {
        return "companyCategory/addCompanyCategory";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute @Valid CreateCompanyCategoryDto dto, BindingResult bindingResult, ModelMap modelMap) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            modelMap.addAttribute("categoryErrors", errors);
            return "companyCategory/addCompanyCategory";
        }
        try {
            companyCategoryService.add(dto);
        } catch (CategoryCreationException e) {
            modelMap.addAttribute("errorMessageAddCategory", e.getMessage());
            return "companyCategory/addCompanyCategory";
        }
        return "redirect:/companyCategories";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        try {
            companyCategoryService.delete(id);
        } catch (CategoryNotFoundException | CategoryDeleteException e) {
            redirectAttributes.addFlashAttribute("errorMessageDeleteCategory", e.getMessage());
        }
        return "redirect:/companyCategories";
    }
}