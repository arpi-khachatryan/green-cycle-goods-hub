package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.authn.CurrentUser;
import com.epam.greencyclegoods.dto.company.CompanyOverview;
import com.epam.greencyclegoods.dto.company.CreateCompanyDto;
import com.epam.greencyclegoods.dto.company.EditCompanyDto;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.service.*;
import com.epam.greencyclegoods.utility.PaginationUtility;
import com.epam.greencyclegoods.validation.ValidationErrorMapping;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/companies")
public class CompanyController {

    private final EventService eventService;
    private final CompanyService companyService;
    private final ProductService productService;
    private final CompanyCategoryService companyCategoryService;
    private final ProductCategoryService productCategoryService;

    @GetMapping
    public String companies(@RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "6") int size,
                            ModelMap modelMap, Model model) {
        Page<CompanyOverview> companies = companyService.companies(PageRequest.of(page - 1, size));
        modelMap.addAttribute("companies", companies);
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(companies));

        if (model.containsAttribute("errorMessageNoCompany")) {
            modelMap.addAttribute("errorMessageNoCompany", model.getAttribute("errorMessageNoCompany"));
        }
        return "company/companies";
    }

    @GetMapping("/user-companies")
    public String userCompanies(@RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "6") int size,
                                @AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap) {
        try {
            Page<CompanyOverview> companies = companyService.companiesByUser(currentUser.getUser(), PageRequest.of(page - 1, size));
            modelMap.addAttribute("companies", companies);
            modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(companies));
            return "company/companies";
        } catch (CompanyNotFoundException e) {
            modelMap.addAttribute("errorMessageNoCompany", e.getMessage());
            return "redirect:/companies";
        }
    }

    @GetMapping("/{id}")
    public String company(@PathVariable int id, Model model, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        try {
            modelMap.addAttribute("company", companyService.company(id));
            modelMap.addAttribute("products", productService.companyProducts(id));
        } catch (CompanyNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageNoCompany", e.getMessage());
            return "redirect:/companies";
        }
        modelMap.addAttribute("categories", productCategoryService.categories());
        modelMap.addAttribute("eventsByCompany", eventService.companyEvents(id));

        if (model.containsAttribute("errorMessageEdit")) {
            modelMap.addAttribute("errorMessageEdit", model.getAttribute("errorMessageEdit"));
        }

        if (model.containsAttribute("errorMessageDelete")) {
            modelMap.addAttribute("errorMessageDelete", model.getAttribute("errorMessageDelete"));
        }
        return "company/company";
    }

    @GetMapping("/add")
    public String add(ModelMap modelMap, RedirectAttributes redirectAttributes, Model model) {
        try {
            modelMap.addAttribute("categories", companyCategoryService.categories());
        } catch (CategoryNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageCompanyCreation", true);
        }

        if (model.containsAttribute("errorMessageNoUser")) {
            modelMap.addAttribute("errorMessageNoUser", model.getAttribute("errorMessageNoUser"));
        }
        if (model.containsAttribute("errorMessageCreate")) {
            modelMap.addAttribute("errorMessageCreate", model.getAttribute("errorMessageCreate"));
        }
        if (model.containsAttribute("errorMessageFile")) {
            modelMap.addAttribute("errorMessageFile", model.getAttribute("errorMessageFile"));
        }
        if (model.containsAttribute("companyErrors")) {
            modelMap.addAttribute("companyErrors", model.getAttribute("companyErrors"));
        }
        modelMap.addAttribute("company", model.containsAttribute("company") ? model.getAttribute("company") : new CreateCompanyDto());

        return "company/addCompany";
    }

    @PostMapping("/add")
    public String add(@RequestParam("companyImage") MultipartFile[] files,
                      @ModelAttribute @Valid CreateCompanyDto dto, BindingResult bindingResult,
                      @AuthenticationPrincipal CurrentUser currentUser,
                      ModelMap modelMap, RedirectAttributes redirectAttributes) throws IOException {
        if (currentUser == null) {
            redirectAttributes.addFlashAttribute("errorMessageNoUser", true);
            redirectAttributes.addFlashAttribute("company", dto);
            return "redirect:/companies/add";
        }
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            redirectAttributes.addFlashAttribute("companyErrors", errors);
            redirectAttributes.addFlashAttribute("company", dto);
            return "redirect:/companies/add";
        }
        for (MultipartFile file : files) {
            if (!file.isEmpty() && file.getSize() > 0) {
                if (file.getContentType() != null && !file.getContentType().contains("image")) {
                    modelMap.addAttribute("errorMessageFile", true);
                    redirectAttributes.addFlashAttribute("company", dto);
                    return "redirect:/companies/add";
                }
            }
        }
        try {
            companyService.add(dto, files, currentUser.getUser());
            if (currentUser.getUser().getRole() == Role.OWNER) {
                return "redirect:/companies/user-companies";
            } else {
                return "redirect:/companies";
            }
        } catch (ImageUploadException | UserUpdateException | CompanyCreationException e) {
            redirectAttributes.addFlashAttribute("company", dto);
            redirectAttributes.addFlashAttribute("errorMessageCreate", e.getMessage());
            return "redirect:/companies/add";
        }
    }

    @GetMapping("/registration-terms")
    public String registrationTerms(ModelMap modelMap, RedirectAttributes redirectAttributes) {
        try {
            modelMap.addAttribute("categories", companyCategoryService.categories());
        } catch (CategoryNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageCategoryNotFound", true);
        }
        return "company/companyTerms";
    }

    @GetMapping(value = "/getImages", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getImage(@RequestParam("fileName") String fileName) {
        return companyService.getCompanyImage(fileName);
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes, Model model) {
        try {
            modelMap.addAttribute("company", companyService.company(id));
            modelMap.addAttribute("categories", companyCategoryService.categories());
        } catch (CompanyNotFoundException | CategoryNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEdit", e.getMessage());
            return "redirect:/companies/" + id;
        }

        if (model.containsAttribute("errorMessageEdit")) {
            modelMap.addAttribute("errorMessageEdit", model.getAttribute("errorMessageEdit"));
        }
        if (model.containsAttribute("updateSuccess")) {
            modelMap.addAttribute("updateSuccess", model.getAttribute("updateSuccess"));
        }
        if (model.containsAttribute("companyErrors")) {
            modelMap.addAttribute("companyErrors", model.getAttribute("companyErrors"));
        }
        return "company/editCompanies";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, @ModelAttribute @Valid EditCompanyDto dto, @RequestParam("companyImage") MultipartFile[] files, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            redirectAttributes.addFlashAttribute("companyErrors", errors);
            return "redirect:/companies/edit/" + id;
        }
        try {
            companyService.edit(dto, id, files);
            redirectAttributes.addFlashAttribute("updateSuccess", "Company updated successfully.");
        } catch (ImageUploadException | CompanyNotFoundException e) {
            redirectAttributes.addAttribute("errorMessageEdit", e.getMessage());
        }
        return "redirect:/companies/edit/" + id;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        try {
            companyService.delete(id);
        } catch (CompanyDeleteException | CompanyNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageDelete", e.getMessage());
        }
        return "redirect:/companies/" + id;
    }

    @GetMapping("/{id}/events")
    public String companyEvents(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        try {
            modelMap.addAttribute("company", companyService.company(id));
            modelMap.addAttribute("products", productService.companyProducts(id));
        } catch (CompanyNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEvent", "This company has not organized any events.");
            return "redirect:/events";
        }
        modelMap.addAttribute("eventsByCompany", eventService.companyEvents(id));
        modelMap.addAttribute("categories", productCategoryService.categories());
        return "event/eventCatalog";
    }
}