package com.epam.greencyclegoods.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Controller
@RequestMapping
public class MainController {

    @GetMapping("/")
    public String mainPage() {
        return "main/index";
    }

    @GetMapping("/loginPage")
    public String loginPage(@RequestParam(value = "error", required = false) String error, ModelMap modelMap, Model model) {
        if (error != null && error.equals("true")) {
            modelMap.addAttribute("loginError", "Invalid username or password. Please check your credentials and try again.");
        }
        if (model.containsAttribute("registrationEmail")) {
            modelMap.addAttribute("registrationEmail", model.getAttribute("registrationEmail"));
        }
        return "main/loginPage";
    }

    @GetMapping("/accessDenied")
    public String accessDenied() {
        return "main/accessDenied";
    }
}