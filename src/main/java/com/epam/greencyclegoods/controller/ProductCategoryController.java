package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.dto.productCategory.CreateProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.EditProductCategoryDto;
import com.epam.greencyclegoods.dto.productCategory.ProductCategoryOverview;
import com.epam.greencyclegoods.exception.CategoryDeleteException;
import com.epam.greencyclegoods.exception.CategoryNotFoundException;
import com.epam.greencyclegoods.exception.CategoryUpdateException;
import com.epam.greencyclegoods.exception.ProductCategorySaveException;
import com.epam.greencyclegoods.service.ProductCategoryService;
import com.epam.greencyclegoods.utility.PaginationUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Objects;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/productCategories")
public class ProductCategoryController {

    private final ProductCategoryService productCategoryService;

    @GetMapping
    public String categories(@RequestParam(value = "page", defaultValue = "1") int page,
                             @RequestParam(value = "size", defaultValue = "5") int size,
                             ModelMap modelMap, Model model) {
        Page<ProductCategoryOverview> categories = productCategoryService.categories(PageRequest.of(page - 1, size));
        modelMap.addAttribute("categories", categories);
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(categories));
        if (model.containsAttribute("errorMessageEdit")) {
            modelMap.addAttribute("errorMessageEdit", model.getAttribute("errorMessageEdit"));
        }
        if (model.containsAttribute("errorMessageDelete")) {
            modelMap.addAttribute("errorMessageDelete", model.getAttribute("errorMessageDelete"));
        }
        if (model.containsAttribute("updateSuccess")) {
            modelMap.addAttribute("updateSuccess", model.getAttribute("updateSuccess"));
        }
        return "productCategory/productCategories";
    }

    @GetMapping("/add")
    public String add() {
        return "productCategory/addProductCategory";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute @Valid CreateProductCategoryDto dto, BindingResult bindingResult, ModelMap modelMap) {
        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("nonNullCategoryMessage", Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
            return "productCategory/addProductCategory";
        }
        try {
            productCategoryService.save(dto);
        } catch (ProductCategorySaveException e) {
            modelMap.addAttribute("errorMessageSave", e.getMessage());
            return "productCategory/addProductCategory";
        }
        return "redirect:/productCategories";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes, Model model) {
        try {
            modelMap.addAttribute("category", productCategoryService.categoryById(id));
        } catch (CategoryNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEdit", e.getMessage());
            return "redirect:/productCategories";
        }
        if (model.containsAttribute("errorMessageEdit")) {
            modelMap.addAttribute("errorMessageEdit", model.getAttribute("errorMessageEdit"));
        }
        if (model.containsAttribute("nonNullCategoryMessage")) {
            modelMap.addAttribute("nonNullCategoryMessage", model.getAttribute("nonNullCategoryMessage"));
        }
        return "productCategory/editProductCategory";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, @ModelAttribute @Valid EditProductCategoryDto dto, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("nonNullCategoryMessage", Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
            return "redirect:/productCategories/edit/" + id;
        }

        try {
            productCategoryService.edit(dto, id);
            redirectAttributes.addFlashAttribute("updateSuccess", "Edited successfully");
        } catch (CategoryNotFoundException | CategoryUpdateException e) {
            redirectAttributes.addFlashAttribute("errorMessageEdit", e.getMessage());
            return "redirect:/productCategories/edit/" + id;
        }
        return "redirect:/productCategories";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        try {
            productCategoryService.delete(id);
        } catch (CategoryNotFoundException | CategoryDeleteException e) {
            redirectAttributes.addFlashAttribute("errorMessageDelete", e.getMessage());
        }
        return "redirect:/productCategories";
    }
}