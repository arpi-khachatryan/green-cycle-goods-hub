package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.authn.CurrentUser;
import com.epam.greencyclegoods.dto.registration.EditRegistrationDto;
import com.epam.greencyclegoods.dto.registration.RegistrationOverview;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.exception.RegistrationNotFoundException;
import com.epam.greencyclegoods.service.EventService;
import com.epam.greencyclegoods.service.RegistrationService;
import com.epam.greencyclegoods.utility.PaginationUtility;
import com.epam.greencyclegoods.validation.ValidationErrorMapping;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/registrations")
public class RegistrationController {

    private final RegistrationService registrationService;
    private final EventService eventService;

    @GetMapping
    public String registrations(@RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "6") int size,
                                @AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap, Model model) {
        Page<RegistrationOverview> registrations = null;
        if (currentUser.getUser().getRole() == Role.ADMIN) {
            registrations = registrationService.registrations(PageRequest.of(page - 1, size));
        } else if (currentUser.getUser().getRole() == Role.OWNER) {
            try {
                registrations = registrationService.registrationsByOwner(currentUser.getUser(), PageRequest.of(page - 1, size));
                modelMap.addAttribute("ownerRegistrations", true);
            } catch (RegistrationNotFoundException e) {
                modelMap.addAttribute("errorMessageRegistration", e.getMessage());
            }
        } else if (currentUser.getUser().getRole() == Role.CUSTOMER) {
            registrations = registrationService.registrationsByUser(currentUser.getUser().getId(), PageRequest.of(page - 1, size));
        }

        modelMap.addAttribute("registrations", registrations);
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(registrations));
        if (model.containsAttribute("errorMessageEdit")) {
            modelMap.addAttribute("errorMessageEdit", model.getAttribute("errorMessageEdit"));
        }
        if (model.containsAttribute("errorMessageDelete")) {
            modelMap.addAttribute("errorMessageDelete", model.getAttribute("errorMessageDelete"));
        }
        return "registration/registrations";
    }

    @GetMapping("/user-registrations")
    public String userRegistrations(@RequestParam(value = "page", defaultValue = "1") int page,
                                    @RequestParam(value = "size", defaultValue = "6") int size,
                                    @AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap) {
        Page<RegistrationOverview> registrations = registrationService.registrationsByUser(currentUser.getUser().getId(), PageRequest.of(page - 1, size));
        modelMap.addAttribute("registrations", registrations);
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(registrations));
        modelMap.addAttribute("adminRegistrations", true);
        return "registration/registrations";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes, Model model) {
        try {
            modelMap.addAttribute("registration", registrationService.registrationById(id));
        } catch (RegistrationNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEdit", e.getMessage());
            return "redirect:/registrations";
        }
        modelMap.addAttribute("events", eventService.events());

        if (model.containsAttribute("errorMessageEdit")) {
            modelMap.addAttribute("errorMessageEdit", model.getAttribute("errorMessageEdit"));
        }
        if (model.containsAttribute("registrationErrors")) {
            modelMap.addAttribute("registrationErrors", model.getAttribute("registrationErrors"));
        }
        if (model.containsAttribute("updateSuccess")) {
            modelMap.addAttribute("updateSuccess", model.getAttribute("updateSuccess"));
        }
        return "registration/editRegistration";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, @ModelAttribute @Valid EditRegistrationDto dto, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            redirectAttributes.addFlashAttribute("registrationErrors", errors);
        }

        try {
            registrationService.edit(dto, id);
            redirectAttributes.addFlashAttribute("updateSuccess", "Registration edited successfully");
        } catch (RegistrationNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEdit", e.getMessage());
        }
        return "redirect:/registrations/edit/" + id;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        try {
            registrationService.delete(id);
        } catch (RegistrationNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageDelete", e.getMessage());
        }
        return "redirect:/registrations";
    }
}