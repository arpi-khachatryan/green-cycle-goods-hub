package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.authn.CurrentUser;
import com.epam.greencyclegoods.dto.payment.EditPaymentDto;
import com.epam.greencyclegoods.dto.payment.PaymentOverview;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.exception.InvalidDateFormatException;
import com.epam.greencyclegoods.exception.PaymentDeleteException;
import com.epam.greencyclegoods.exception.PaymentNotFoundException;
import com.epam.greencyclegoods.service.PaymentService;
import com.epam.greencyclegoods.validation.ValidationErrorMapping;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

import static com.epam.greencyclegoods.utility.PaginationUtility.generatePageNumbers;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/payments")
public class PaymentController {

    private final PaymentService paymentService;

    @GetMapping
    public String payments(@RequestParam(value = "page", defaultValue = "1") int page,
                           @RequestParam(value = "size", defaultValue = "10") int size,
                           @AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap, Model model) {
        if (currentUser.getUser().getRole() == Role.ADMIN) {
            Page<PaymentOverview> payments = paymentService.payments(PageRequest.of(page - 1, size));
            modelMap.addAttribute("payments", payments);
            modelMap.addAttribute("totalPages", generatePageNumbers(payments));
        } else if (currentUser.getUser().getRole() == Role.OWNER) {
            Page<PaymentOverview> payments = paymentService.ownerPayments(currentUser.getUser().getId(), PageRequest.of(page - 1, size));
            modelMap.addAttribute("payments", payments);
            modelMap.addAttribute("totalPages", generatePageNumbers(payments));
            modelMap.addAttribute("ownerPayment", true);
        } else {
            Page<PaymentOverview> payments = paymentService.userPayments(currentUser.getUser().getId(), PageRequest.of(page - 1, size));
            modelMap.addAttribute("payments", payments);
            modelMap.addAttribute("totalPages", generatePageNumbers(payments));
        }

        if (model.containsAttribute("errorMessagePayment")) {
            modelMap.addAttribute("errorMessagePayment", model.getAttribute("errorMessagePayment"));
        }
        return "payment/payments";
    }

    @GetMapping("/user-payments")
    public String userPayments(@RequestParam(value = "page", defaultValue = "1") int page,
                               @RequestParam(value = "size", defaultValue = "10") int size,
                               ModelMap modelMap, @AuthenticationPrincipal CurrentUser currentUser) {
        Page<PaymentOverview> payments = paymentService.userPayments(currentUser.getUser().getId(), PageRequest.of(page - 1, size));
        modelMap.addAttribute("payments", payments);
        modelMap.addAttribute("totalPages", generatePageNumbers(payments));
        return "payment/payments";
    }

    @GetMapping("/{id}")
    public String payment(@PathVariable("id") int id,
                          @RequestParam(name = "isOwner", defaultValue = "false") boolean isOwner,
                          ModelMap modelMap, RedirectAttributes redirectAttributes, Model model) {
        try {
            modelMap.addAttribute("payment", paymentService.payment(id));
            if (isOwner || model.containsAttribute("isOwner")) {
                modelMap.addAttribute("isOwner", true);
            }
        } catch (PaymentNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessagePayment", e.getMessage());
            return "redirect:/payments";
        }

        if (model.containsAttribute("paymentErrors")) {
            modelMap.addAttribute("paymentErrors", model.getAttribute("paymentErrors"));
        }
        if (model.containsAttribute("errorMessageEditPayment")) {
            modelMap.addAttribute("errorMessageEditPayment", model.getAttribute("errorMessageEditPayment"));
        }
        if (model.containsAttribute("updateSuccess")) {
            modelMap.addAttribute("updateSuccess", model.getAttribute("updateSuccess"));
        }
        return "payment/payment";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        try {
            modelMap.addAttribute("payment", paymentService.payment(id));
            return "payment/payment";
        } catch (PaymentNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessagePayment", e.getMessage());
            return "redirect:/payments";
        }
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, @ModelAttribute EditPaymentDto dto, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            redirectAttributes.addFlashAttribute("paymentErrors", errors);
            return "redirect:/payments/" + id;
        }
        try {
            paymentService.edit(dto, id);
            redirectAttributes.addFlashAttribute("updateSuccess", "Payment updated successfully.");
            redirectAttributes.addFlashAttribute("isOwner", true);
        } catch (PaymentNotFoundException | InvalidDateFormatException e) {
            redirectAttributes.addFlashAttribute("errorMessageEditPayment", e.getMessage());
        }
        return "redirect:/payments/" + id;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        try {
            paymentService.delete(id);
        } catch (PaymentNotFoundException | PaymentDeleteException e) {
            redirectAttributes.addFlashAttribute("errorMessagePayment", e.getMessage());
        }
        return "redirect:/payments";
    }
}