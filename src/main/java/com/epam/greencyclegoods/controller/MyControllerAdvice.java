package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.authn.CurrentUser;
import com.epam.greencyclegoods.entity.User;
import com.epam.greencyclegoods.service.BasketService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@ControllerAdvice
@RequiredArgsConstructor
public class MyControllerAdvice {

    private final UserDetailsService userDetailsService;
    private final BasketService basketService;

    @ModelAttribute(name = "currentUser")
    public User currentUser(@AuthenticationPrincipal CurrentUser currentUser) {
        if (currentUser != null) {
            currentUser = (CurrentUser) userDetailsService.loadUserByUsername(currentUser.getUsername());
            return currentUser.getUser();
        }
        return null;
    }

    @ModelAttribute(name = "basketCount")
    public int basketCount(@AuthenticationPrincipal CurrentUser currentUser) {
        if (currentUser != null) {
            return basketService.totalProductCountInBasketForUser(currentUser.getUser());
        }
        return 0;
    }
}