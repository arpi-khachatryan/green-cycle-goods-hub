package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.authn.CurrentUser;
import com.epam.greencyclegoods.dto.basket.BasketOverview;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.exception.BasketNotFoundException;
import com.epam.greencyclegoods.exception.ProductNotFoundException;
import com.epam.greencyclegoods.exception.UserNotFoundException;
import com.epam.greencyclegoods.service.BasketService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static com.epam.greencyclegoods.utility.PaginationUtility.generatePageNumbers;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/baskets")
public class BasketController {

    private final BasketService basketService;

    @GetMapping
    public String baskets(@RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "size", defaultValue = "5") int size,
                          @AuthenticationPrincipal CurrentUser currentUser,
                          ModelMap modelMap, Model model) {
        try {
            Page<BasketOverview> baskets = basketService.baskets(PageRequest.of(page - 1, size), currentUser.getUser());
            modelMap.addAttribute("baskets", baskets);
            modelMap.addAttribute("totalPages", generatePageNumbers(baskets));

            if (model.containsAttribute("errorMessageAddOrder")) {
                modelMap.addAttribute("errorMessageAddOrder", model.getAttribute("errorMessageAddOrder"));
            }
            if (model.containsAttribute("errorMessageDeleteBasket")) {
                modelMap.addAttribute("errorMessageDeleteBasket", model.getAttribute("errorMessageDeleteBasket"));
            }
            return "basket/basket";
        } catch (BasketNotFoundException e) {
            return "redirect:/users/home";
        }
    }

    @GetMapping("/all-baskets")
    public String baskets(@RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "size", defaultValue = "5") int size,
                          @AuthenticationPrincipal CurrentUser currentUser,
                          ModelMap modelMap) {
        Page<BasketOverview> baskets = null;
        try {
            if (currentUser.getUser().getRole() == Role.ADMIN) {
                baskets = basketService.allBaskets(PageRequest.of(page - 1, size));
                modelMap.addAttribute("baskets", baskets);
                modelMap.addAttribute("totalPages", generatePageNumbers(baskets));
            } else if (currentUser.getUser().getRole() == Role.OWNER) {
                baskets = basketService.ownerBaskets(PageRequest.of(page - 1, size), currentUser.getUser());
                modelMap.addAttribute("baskets", baskets);
                modelMap.addAttribute("totalPages", generatePageNumbers(baskets));
            }
        } catch (BasketNotFoundException e) {
            return "redirect:/users/home";
        }
        return "admin/basket";
    }

    @GetMapping("/add/{id}")
    public String addToBasket(@PathVariable("id") int id, @AuthenticationPrincipal CurrentUser currentUser, RedirectAttributes redirectAttributes) {
        try {
            basketService.addToBasket(id, currentUser.getUser());
            redirectAttributes.addFlashAttribute("basketSuccess", true);
            return "redirect:/products";
        } catch (UserNotFoundException | ProductNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageAddToBasket", e.getMessage());
            return "redirect:/products";
        }
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, @AuthenticationPrincipal CurrentUser currentUser, RedirectAttributes redirectAttributes) {
        try {
            basketService.delete(id, currentUser.getUser());
            return "redirect:/baskets";
        } catch (BasketNotFoundException | ProductNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageDeleteBasket", e.getMessage());
            return "basket/basket";
        }
    }

    @GetMapping("/products-in-stock")
    public ResponseEntity<String> productsInStock(@AuthenticationPrincipal CurrentUser currentUser) {
        try {
            boolean allProductsOutOfStock = basketService.productsInStock(currentUser.getUser());
            if (allProductsOutOfStock) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                return ResponseEntity.ok().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}