package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.authn.CurrentUser;
import com.epam.greencyclegoods.dto.product.CreateProductDto;
import com.epam.greencyclegoods.dto.product.EditProductDto;
import com.epam.greencyclegoods.dto.product.ProductOverview;
import com.epam.greencyclegoods.dto.productCategory.ProductCategoryOverview;
import com.epam.greencyclegoods.exception.ImageUploadException;
import com.epam.greencyclegoods.exception.PermissionDeniedException;
import com.epam.greencyclegoods.exception.ProductCreationException;
import com.epam.greencyclegoods.exception.ProductNotFoundException;
import com.epam.greencyclegoods.service.CompanyService;
import com.epam.greencyclegoods.service.ProductCategoryService;
import com.epam.greencyclegoods.service.ProductService;
import com.epam.greencyclegoods.utility.PaginationUtility;
import com.epam.greencyclegoods.validation.ValidationErrorMapping;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;
    private final CompanyService companyService;
    private final ProductCategoryService productCategoryService;

    @GetMapping
    public String products(@RequestParam(value = "id", required = false) Integer id,
                           @RequestParam(value = "sort", defaultValue = "") String sort,
                           @RequestParam(value = "page", defaultValue = "1") int page,
                           @RequestParam(value = "size", defaultValue = "15") int size,
                           ModelMap modelMap, Model model) {
        Page<ProductOverview> products = productService.sort(PageRequest.of(page - 1, size), sort, id);
        modelMap.addAttribute("categories", productCategoryService.categories());
        modelMap.addAttribute("products", products);
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(products));

        if (model.containsAttribute("errorMessageAddToBasket")) {
            modelMap.addAttribute("errorMessageAddToBasket", model.getAttribute("errorMessageAddToBasket"));
        }
        if (model.containsAttribute("basketSuccess")) {
            modelMap.addAttribute("basketSuccess", model.getAttribute("basketSuccess"));
        }
        return "product/products";
    }

    @GetMapping("/myproducts")
    public String userProducts(@RequestParam(value = "page", defaultValue = "1") int page,
                               @RequestParam(value = "size", defaultValue = "6") int size,
                               @AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap, Model model) {
        Page<ProductCategoryOverview> categories = productCategoryService.categories(PageRequest.of(page - 1, size));
        modelMap.addAttribute("categories", categories);
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(categories));
        List<ProductOverview> products = productService.userProducts(currentUser.getUser());
        modelMap.addAttribute("products", products);
        if (model.containsAttribute("errorMessageProduct")) {
            modelMap.addAttribute("errorMessageProduct", model.getAttribute("errorMessageProduct"));
        }
        return "admin/products";
    }

    @GetMapping("/add")
    public String add(ModelMap modelMap, Model model) {
        modelMap.addAttribute("companies", companyService.companies());
        modelMap.addAttribute("categories", productCategoryService.categories());
        if (model.containsAttribute("errorMessageProduct")) {
            modelMap.addAttribute("errorMessageProduct", model.getAttribute("errorMessageProduct"));
        }
        return "product/addProduct";
    }

    @PostMapping("/add")
    public String add(@RequestParam("productImage") MultipartFile[] files, @ModelAttribute @Valid CreateProductDto dto,
                      BindingResult bindingResult, @AuthenticationPrincipal CurrentUser currentUser,
                      ModelMap modelMap, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            modelMap.addAttribute("productErrors", errors);
            modelMap.addAttribute("companies", companyService.companies());
            modelMap.addAttribute("categories", productCategoryService.categories());
            return "product/addProduct";
        }
        try {
            productService.save(dto, files, currentUser.getUser());
        } catch (ProductCreationException | ImageUploadException e) {
            redirectAttributes.addFlashAttribute("errorMessageProduct", e.getMessage());
            return "redirect:/products/add";
        }
        return "redirect:/products/myproducts";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes, Model model) {
        modelMap.addAttribute("categories", productCategoryService.categories());
        modelMap.addAttribute("companies", companyService.companies());
        try {
            modelMap.addAttribute("product", productService.productById(id));
        } catch (ProductNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageProduct", e.getMessage());
            return "redirect:/products/myproducts";
        }
        if (model.containsAttribute("productErrors")) {
            modelMap.addAttribute("productErrors", model.getAttribute("productErrors"));
        }
        if (model.containsAttribute("errorMessageProduct")) {
            modelMap.addAttribute("errorMessageProduct", model.getAttribute("errorMessageProduct"));
        }
        return "product/editProduct";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, @RequestParam("productImage") MultipartFile[] files,
                       @ModelAttribute @Valid EditProductDto dto, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            redirectAttributes.addFlashAttribute("productErrors", errors);
            return "redirect:/products/edit/" + id;
        }

        try {
            productService.edit(dto, id, files);
        } catch (ImageUploadException | ProductNotFoundException | PermissionDeniedException e) {
            redirectAttributes.addFlashAttribute("errorMessageProduct", e.getMessage());
            return "redirect:/products/edit/" + id;
        }
        return "redirect:/products/myproducts";
    }

    @GetMapping(value = "/getImages", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getImage(@RequestParam("fileName") String fileName) {
        return productService.getProductImage(fileName);
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, @AuthenticationPrincipal CurrentUser currentUser, RedirectAttributes redirectAttributes) {
        try {
            productService.delete(id, currentUser.getUser());
        } catch (ProductNotFoundException | PermissionDeniedException e) {
            redirectAttributes.addFlashAttribute("errorMessageProduct", e.getMessage());
            return "redirect:/products/myproducts";
        }
        return "redirect:/products/myproducts";
    }
}