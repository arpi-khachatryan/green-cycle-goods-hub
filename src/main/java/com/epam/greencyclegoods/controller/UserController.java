package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.authn.CurrentUser;
import com.epam.greencyclegoods.dto.user.ChangePasswordDto;
import com.epam.greencyclegoods.dto.user.CreateUserDto;
import com.epam.greencyclegoods.dto.user.EditUserDto;
import com.epam.greencyclegoods.dto.user.UserOverview;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.service.UserService;
import com.epam.greencyclegoods.utility.PaginationUtility;
import com.epam.greencyclegoods.validation.ValidationErrorMapping;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @GetMapping("/home")
    public String showUserProfile(@AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap, Model model) {
        if (model.containsAttribute("errorMessagePassword")) {
            modelMap.addAttribute("errorMessagePassword", model.getAttribute("errorMessagePassword"));
        }
        if (model.containsAttribute("errorMessageEdit")) {
            modelMap.addAttribute("errorMessageEdit", model.getAttribute("errorMessageEdit"));
        }
        return switch (currentUser.getUser().getRole()) {
            case OWNER, CUSTOMER -> "user/customer";
            case ADMIN -> "admin/admin";
            default -> "redirect:/";
        };
    }

    @GetMapping
    public String users(@RequestParam(value = "page", defaultValue = "1") int page,
                        @RequestParam(value = "size", defaultValue = "6") int size,
                        ModelMap modelMap, Model model) {
        Page<UserOverview> users = userService.users(PageRequest.of(page - 1, size));
        modelMap.addAttribute("users", users);
        modelMap.addAttribute("totalPages", PaginationUtility.generatePageNumbers(users));
        if (model.containsAttribute("errorMessageDelete")) {
            modelMap.addAttribute("errorMessageDelete", model.getAttribute("errorMessageDelete"));
        }
        return "user/users";
    }

    @GetMapping("/add")
    public String showUserRegistrationPage(Model model) {
        model.addAttribute("user", new CreateUserDto());
        return "user/addUser";
    }

    @PostMapping("/add")
    public String registerUser(@ModelAttribute @Valid CreateUserDto dto, BindingResult bindingResult, RedirectAttributes redirectAttributes, ModelMap modelMap) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            modelMap.addAttribute("userErrors", errors);
            modelMap.addAttribute("user", dto);
            return "user/addUser";
        }
        try {
            userService.save(dto);
            redirectAttributes.addFlashAttribute("registrationEmail", "Please check your email.");
            return "redirect:/loginPage";
        } catch (EmailSendingException | AuthenticationException e) {
            modelMap.addAttribute("user", dto);
            modelMap.addAttribute("registrationError", e.getMessage());
            return "user/addUser";
        }
    }

    @GetMapping("/verify")
    public String verifyUserByToken(@RequestParam("token") String token, ModelMap modelMap) {
        try {
            userService.verify(token);
            modelMap.addAttribute("verificationMessage", "Your account has been successfully verified.");
            return "main/loginPage";
        } catch (VerificationException e) {
            modelMap.addAttribute("user", new CreateUserDto());
            modelMap.addAttribute("verificationError", e.getMessage());
            return "user/addUser";
        }
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable int id, ModelMap modelMap, RedirectAttributes redirectAttributes, Model model) {
        try {
            modelMap.addAttribute("user", userService.user(id));
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEdit", e.getMessage());
            return "redirect:/users/home";
        }
        if (model.containsAttribute("userErrors")) {
            modelMap.addAttribute("userErrors", model.getAttribute("userErrors"));
        }

        return "user/customerEdit";
    }

    @PostMapping("/edit/{id}")
    public String edit(@ModelAttribute @Valid EditUserDto dto, BindingResult bindingResult, @PathVariable int id, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            redirectAttributes.addFlashAttribute("userErrors", errors);
            return "redirect:/users/edit/" + id;
        }
        try {
            userService.edit(dto, id);
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEdit", e.getMessage());
        }
        return "redirect:/users/home";
    }

    @PostMapping("/changePassword/{id}")
    public String changePassword(@ModelAttribute @Valid ChangePasswordDto dto, BindingResult bindingResult, @PathVariable int id, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            redirectAttributes.addFlashAttribute("userErrors", errors);
            return "redirect:/users/edit/" + id;
        }
        try {
            userService.changePassword(id, dto);
        } catch (UserNotFoundException | PasswordChangeException e) {
            redirectAttributes.addFlashAttribute("errorMessagePassword", e.getMessage());
        }
        return "redirect:/users/home";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        try {
            userService.delete(id);
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageDelete", e.getMessage());
        }
        return "redirect:/users";
    }

    @GetMapping("/profile")
    public String showUserProfile(@AuthenticationPrincipal CurrentUser currentUser) {
        return switch (currentUser.getUser().getRole()) {
            case ADMIN -> "admin/adminProfile";
            case OWNER -> "user/userProfile";
            default -> "redirect:/";
        };
    }
}