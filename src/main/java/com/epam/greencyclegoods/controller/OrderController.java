package com.epam.greencyclegoods.controller;

import com.epam.greencyclegoods.authn.CurrentUser;
import com.epam.greencyclegoods.dto.basket.BasketOverview;
import com.epam.greencyclegoods.dto.creditCard.CreateCreditCardDto;
import com.epam.greencyclegoods.dto.order.CreateOrderDto;
import com.epam.greencyclegoods.dto.order.EditOrderDto;
import com.epam.greencyclegoods.dto.order.OrderOverview;
import com.epam.greencyclegoods.entity.Role;
import com.epam.greencyclegoods.exception.*;
import com.epam.greencyclegoods.service.BasketService;
import com.epam.greencyclegoods.service.OrderService;
import com.epam.greencyclegoods.validation.ValidationErrorMapping;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static com.epam.greencyclegoods.utility.PaginationUtility.generatePageNumbers;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/orders")
public class OrderController {

    private final OrderService orderService;
    private final BasketService basketService;

    @GetMapping
    public String orders(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "10") int size,
                         @AuthenticationPrincipal CurrentUser currentUser, Model model, ModelMap modelMap) {
        if (currentUser.getUser().getRole() == Role.ADMIN) {
            Page<OrderOverview> orders = orderService.orders(PageRequest.of(page - 1, size));
            modelMap.addAttribute("orders", orders);
            modelMap.addAttribute("totalPages", generatePageNumbers(orders));
        } else if (currentUser.getUser().getRole() == Role.OWNER) {
            Page<OrderOverview> orders = orderService.ownerOrders(currentUser.getUser().getId(), PageRequest.of(page - 1, size));
            modelMap.addAttribute("orders", orders);
            modelMap.addAttribute("totalPages", generatePageNumbers(orders));
            modelMap.addAttribute("ownerOrder", true);
        } else {
            Page<OrderOverview> orders = orderService.userOrders(currentUser.getUser().getId(), PageRequest.of(page - 1, size));
            modelMap.addAttribute("orders", orders);
            modelMap.addAttribute("totalPages", generatePageNumbers(orders));
        }

        if (model.containsAttribute("errorMessageOrder")) {
            modelMap.addAttribute("errorMessageOrder", model.getAttribute("errorMessageOrder"));
        }
        if (model.containsAttribute("errorMessageEditOrder")) {
            modelMap.addAttribute("errorMessageEditOrder", model.getAttribute("errorMessageEditOrder"));
        }
        if (model.containsAttribute("errorMessageDeleteOrder")) {
            modelMap.addAttribute("errorMessageDeleteOrder", model.getAttribute("errorMessageDeleteOrder"));
        }
        return "order/orders";
    }

    @GetMapping("/user-orders")
    public String userOrders(@RequestParam(value = "page", defaultValue = "1") int page,
                             @RequestParam(value = "size", defaultValue = "10") int size,
                             @AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap) {
        Page<OrderOverview> orders = orderService.userOrders(currentUser.getUser().getId(), PageRequest.of(page - 1, size));
        modelMap.addAttribute("orders", orders);
        modelMap.addAttribute("totalPages", generatePageNumbers(orders));
        return "order/orders";
    }

    @GetMapping("/{id}")
    public String order(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes,
                        @RequestParam(name = "isOwner", defaultValue = "false") boolean isOwner) {
        try {
            modelMap.addAttribute("order", orderService.order(id));
            modelMap.addAttribute("productQuantity", orderService.productQuantity(id));
            if (isOwner) {
                modelMap.addAttribute("isOwner", true);
            }
            return "order/order";
        } catch (OrderNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageOrder", e.getMessage());
            return "redirect:/orders";
        }
    }

    @GetMapping("/add")
    public String add(ModelMap modelMap, @AuthenticationPrincipal CurrentUser currentUser, RedirectAttributes redirectAttributes, Model model) {
        if (model.containsAttribute("errorMessageAddOrder")) {
            modelMap.addAttribute("errorMessageAddOrder", model.getAttribute("errorMessageAddOrder"));
        }

        try {
            List<BasketOverview> baskets = basketService.baskets(currentUser.getUser());
            if (baskets.size() != 0) {
                modelMap.addAttribute("baskets", baskets);
                modelMap.addAttribute("totalPrice", basketService.calculateTotalBasketPrice(currentUser.getUser()));
                return "order/addOrder";
            } else
                return "redirect:/baskets";
        } catch (BasketNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageAddOrder", e.getMessage());
            return "redirect:/baskets";
        }
    }

    @PostMapping("/add")
    public String add(@ModelAttribute @Valid CreateOrderDto orderDto, BindingResult orderBindingResult,
                      @ModelAttribute @Valid CreateCreditCardDto cardDto, BindingResult cardBindingResult,
                      @AuthenticationPrincipal CurrentUser currentUser, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        try {
            List<BasketOverview> baskets = basketService.baskets(currentUser.getUser());
            double totalPrice = basketService.calculateTotalBasketPrice(currentUser.getUser());
            modelMap.addAttribute("baskets", baskets);
            modelMap.addAttribute("totalPrice", totalPrice);
        } catch (BasketNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageAddOrder", e.getMessage());
            return "order/addOrder";
        }

//        if (orderBindingResult.hasErrors() || (orderDto.getPaymentOption().equals("CREDIT_CARD") && cardBindingResult.hasErrors())) {
        if (orderBindingResult.hasErrors() || cardBindingResult.hasErrors()) {
            Map<String, Object> orderErrors = ValidationErrorMapping.mapValidationErrors(orderBindingResult);
            modelMap.addAttribute("orderErrors", orderErrors);
//            if (orderDto.getPaymentOption().equals("CREDIT_CARD")) {
            Map<String, Object> cardErrors = ValidationErrorMapping.mapValidationErrors(cardBindingResult);
            modelMap.addAttribute("cardErrors", cardErrors);
//            }
            return "order/addOrder";
        }

        try {
            orderService.add(orderDto, cardDto, currentUser.getUser());
        } catch (BasketNotFoundException | OrderNotFoundException | UserNotFoundException |
                 CreditCardNotFoundException | ProductNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageAddOrder", e.getMessage());
            return "redirect:/orders/add";
        }
        return "order/confirmOrder";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap modelMap, RedirectAttributes redirectAttributes, Model model) {
        try {
            modelMap.addAttribute("order", orderService.order(id));
            modelMap.addAttribute("productQuantity", orderService.productQuantity(id));
        } catch (OrderNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEditOrder", e.getMessage());
            return "redirect:/orders";
        }
        if (model.containsAttribute("orderErrors")) {
            modelMap.addAttribute("orderErrors", model.getAttribute("orderErrors"));
        }
        if (model.containsAttribute("successMessage")) {
            modelMap.addAttribute("successMessage", model.getAttribute("successMessage"));
        }
        if (model.containsAttribute("errorMessageEditOrder")) {
            modelMap.addAttribute("errorMessageEditOrder", model.getAttribute("errorMessageEditOrder"));
        }
        modelMap.addAttribute("isOwner", true);
        return "order/order";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, @ModelAttribute @Valid EditOrderDto dto, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            Map<String, Object> errors = ValidationErrorMapping.mapValidationErrors(bindingResult);
            redirectAttributes.addFlashAttribute("orderErrors", errors);
            return "redirect:/orders/edit/" + id;
        }
        try {
            orderService.edit(dto, id);
            redirectAttributes.addFlashAttribute("successMessage", "Order edited successfully");
        } catch (OrderNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageEditOrder", e.getMessage());
        }
        return "redirect:/orders/edit/" + id;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        try {
            orderService.delete(id);
        } catch (OrderNotFoundException e) {
            redirectAttributes.addFlashAttribute("errorMessageDeleteOrder", e.getMessage());
        }
        return "redirect:/orders";
    }
}