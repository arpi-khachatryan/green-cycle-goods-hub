package com.epam.greencyclegoods.config;

import com.epam.greencyclegoods.entity.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Component
@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .formLogin()
                .loginPage("/loginPage")
                .usernameParameter("username")
                .passwordParameter("password")
                .loginProcessingUrl("/login")
                .failureUrl("/loginPage?error=true")
                .defaultSuccessUrl("/users/home")
                .and()
                .logout()
                .logoutSuccessUrl("/")
                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied")
                .and()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/loginPage").permitAll()
                .antMatchers("/users/add").not().authenticated()
                .antMatchers("/users/verify").not().authenticated()

                .antMatchers("/users", "/users/delete/*").hasAuthority(Role.ADMIN.name())
                .antMatchers("/users/edit/*", "/users/changePassword", "/users/home").authenticated()

                .antMatchers("/companies", "/companies/*/events", "/companies/add").permitAll()
                .antMatchers("/companies/user-companies").authenticated()
                .antMatchers("/companies/edit/*").hasAnyAuthority(Role.ADMIN.name(), Role.OWNER.name())
                .antMatchers("/companies/delete/*").hasAuthority(Role.ADMIN.name())

                .antMatchers("/companyCategories").permitAll()
                .antMatchers("/companyCategories/add").hasAnyAuthority(Role.ADMIN.name(), Role.OWNER.name())
                .antMatchers("/companyCategories/delete/*").hasAuthority(Role.ADMIN.name())

                .antMatchers("/products").permitAll()
                .antMatchers("/products/myproducts").authenticated()
                .antMatchers("/products/add", "/products/edit/*", "/products/delete/*").hasAnyAuthority(Role.ADMIN.name(), Role.OWNER.name())

                .antMatchers("/productCategories", "/productCategories/**").hasAnyAuthority(Role.ADMIN.name(), Role.OWNER.name())

                .antMatchers("/payments", "/payments/*").authenticated()
                .antMatchers("/payments/edit/*").hasAnyAuthority(Role.ADMIN.name(), Role.OWNER.name())
                .antMatchers("/payments/delete/*").hasAuthority(Role.ADMIN.name())

                .antMatchers("/orders", "/orders/*").authenticated()
                .antMatchers("/orders/edit/*", "/orders/delete/*").hasAuthority(Role.ADMIN.name())

                .antMatchers("/events").permitAll()
                .antMatchers("/events/events", "/events/add", "/events/edit/*", "/events/delete/*").hasAnyAuthority(Role.ADMIN.name(), Role.OWNER.name())

                .antMatchers("/baskets", "/baskets/all-baskets", "/baskets/add/*", "/baskets/delete/*").authenticated()

                .antMatchers("/registrations", "/registrations/user-registrations", "/registrations/edit/*").authenticated()
                .antMatchers("/registrations/delete/*").hasAuthority(Role.ADMIN.name())
                .anyRequest().permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }
}