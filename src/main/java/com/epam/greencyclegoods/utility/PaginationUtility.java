package com.epam.greencyclegoods.utility;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Arpi Khachatryan
 * Date: 15.09.2023
 */

@Slf4j
@UtilityClass
public class PaginationUtility {

    public static List<Integer> generatePageNumbers(Page<?> pageable) {
        int totalPages = pageable.getTotalPages();
        if (totalPages > 0) {
            log.info("Generating page numbers for {} total pages", totalPages);
            return IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }
        log.info("No page numbers to generate (total pages: 0)");
        return null;
    }
}