package com.epam.greencyclegoods.utility;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 15.09.2023
 */

@Slf4j
@Component
public class ImageUtility {

    private final S3Client s3Client;

    public ImageUtility(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    @Value("${s3.bucketName}")
    private String s3Bucket;

    @Value("${project.images.folder}")
    private String imageStoragePath;

    public List<String> uploadImages(MultipartFile[] imageFiles) throws IOException {
        List<String> uploadedImagePaths = new ArrayList<>();
        for (MultipartFile imageFile : imageFiles) {
            if (!imageFile.isEmpty() && imageFile.getSize() > 0) {
                String fileName = System.currentTimeMillis() + "_" + imageFile.getOriginalFilename();
                String key = "path/in/s3/" + fileName;
                try (InputStream inputStream = imageFile.getInputStream()) {
                    byte[] fileBytes = inputStream.readAllBytes();
                    uploadFileToS3(key, fileBytes);
                    uploadedImagePaths.add(key);
                    log.info("Uploaded image to S3: {}", key);
                } catch (Exception e) {
                    log.error("Failed to upload image to S3: {}", key, e);
                }
            }
        }
        return uploadedImagePaths;
    }

    public void uploadFileToS3(String key, byte[] file) {
        PutObjectRequest objectRequest = PutObjectRequest.builder()
                .bucket(s3Bucket)
                .key(key)
                .build();
        s3Client.putObject(objectRequest, RequestBody.fromBytes(file));
    }

    public byte[] getImageContent(String fileName) throws IOException {
        GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                .bucket(s3Bucket)
                .key(fileName)
                .build();
        ResponseInputStream<GetObjectResponse> response = s3Client.getObject(getObjectRequest);

        try {
            return response.readAllBytes();
        } catch (SdkClientException e) {
            log.error("Failed to retrieve image: {}", fileName, e);
            throw new RuntimeException(e);
        }
    }

//    public List<String> uploadImages(MultipartFile[] imageFiles) throws IOException {
//        List<String> uploadedImagePaths = new ArrayList<>();
//        for (MultipartFile imageFile : imageFiles) {
//            if (!imageFile.isEmpty() && imageFile.getSize() > 0) {
//                String fileName = System.currentTimeMillis() + "_" + imageFile.getOriginalFilename();
//                File destinationFile = new File(imageStoragePath + File.separator + fileName);
//                imageFile.transferTo(destinationFile);
//                uploadedImagePaths.add(fileName);
//                log.info("Uploaded image: {}", fileName);
//            }
//        }
//        return uploadedImagePaths;
//    }
//
//    public byte[] getImageContent(String fileName) throws IOException {
//        log.info("Getting image content for: {}", fileName);
//        return Files.readAllBytes(Paths.get(imageStoragePath + File.separator + fileName));
//    }
}