package com.epam.greencyclegoods.validation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 15.09.2023
 */

@Slf4j
@Component
public class ValidationErrorMapping {

    public static Map<String, Object> mapValidationErrors(BindingResult bindingResult) {
        Map<String, Object> errorModel = bindingResult.getModel();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        for (FieldError fieldError : fieldErrors) {
            errorModel.put(fieldError.getField(), fieldError.getDefaultMessage());
            log.error("Validation error for field '{}' - Message: '{}'", fieldError.getField(), fieldError.getDefaultMessage());
        }
        return errorModel;
    }
}