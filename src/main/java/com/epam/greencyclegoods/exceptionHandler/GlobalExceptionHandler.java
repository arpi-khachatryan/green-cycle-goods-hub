package com.epam.greencyclegoods.exceptionHandler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * @author Arpi Khachatryan
 * Date: 06.11.2023
 */

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @GetMapping
    @ExceptionHandler(value = {IllegalStateException.class, RuntimeException.class,
            IOException.class, MessagingException.class})
    public String exceptionHandler(Model model, Exception e) {
        if (e instanceof RuntimeException || e instanceof IOException || e instanceof MessagingException) {
            log.error("An exception occurred: {}", e.getMessage(), e);
            model.addAttribute("error", "Oops! It seems like the page you are looking for is not here. Please check the URL and try again.");
        }
        return "main/error";
    }
}