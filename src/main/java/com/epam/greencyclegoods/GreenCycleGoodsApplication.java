package com.epam.greencyclegoods;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@EnableAsync
@SpringBootApplication
@RequiredArgsConstructor
public class GreenCycleGoodsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreenCycleGoodsApplication.class, args);
    }
}