package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class EmailSendingException extends Exception {

    public EmailSendingException(String message) {
        super(message);
    }
}
