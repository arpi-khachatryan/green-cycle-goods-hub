package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class VerificationException extends Exception {

    public VerificationException(String message) {
        super(message);
    }
}
