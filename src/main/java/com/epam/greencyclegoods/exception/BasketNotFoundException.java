package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class BasketNotFoundException extends Exception {

    public BasketNotFoundException(String message) {
        super(message);
    }
}
