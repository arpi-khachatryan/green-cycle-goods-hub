package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class EventNotFoundException extends Exception {

    public EventNotFoundException(String message) {
        super(message);
    }
}
