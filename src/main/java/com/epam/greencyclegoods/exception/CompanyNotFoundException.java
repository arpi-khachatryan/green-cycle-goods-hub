package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class CompanyNotFoundException extends Exception {

    public CompanyNotFoundException(String message) {
        super(message);
    }
}
