package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class PaymentNotFoundException extends Exception {

    public PaymentNotFoundException(String message) {
        super(message);
    }
}
