package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class UserUpdateException extends Exception {

    public UserUpdateException(String message) {
        super(message);
    }
}
