package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class EventDeleteException extends Exception {

    public EventDeleteException(String message) {
        super(message);
    }
}
