package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class CategoryDeleteException extends Exception {

    public CategoryDeleteException(String message) {
        super(message);
    }
}