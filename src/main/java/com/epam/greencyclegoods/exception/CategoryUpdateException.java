package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class CategoryUpdateException extends Exception {

    public CategoryUpdateException(String message) {
        super(message);
    }
}
