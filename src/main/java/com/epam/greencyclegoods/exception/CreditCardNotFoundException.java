package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class CreditCardNotFoundException extends Exception {

    public CreditCardNotFoundException(String message) {
        super(message);
    }
}
