package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class RegistrationNotFoundException extends Exception {

    public RegistrationNotFoundException(String message) {
        super(message);
    }
}
