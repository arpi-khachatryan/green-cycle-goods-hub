package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class PaymentDeleteException extends Exception {

    public PaymentDeleteException(String message) {
        super(message);
    }
}