package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class CategoryCreationException extends Exception {

    public CategoryCreationException(String message) {
        super(message);
    }
}
