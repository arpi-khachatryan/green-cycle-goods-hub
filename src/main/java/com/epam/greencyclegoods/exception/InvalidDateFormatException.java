package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class InvalidDateFormatException extends Exception {

    public InvalidDateFormatException(String message) {
        super(message);
    }
}
