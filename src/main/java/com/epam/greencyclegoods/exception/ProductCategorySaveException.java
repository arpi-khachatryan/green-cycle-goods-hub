package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class ProductCategorySaveException extends Exception {

    public ProductCategorySaveException(String message) {
        super(message);
    }
}
