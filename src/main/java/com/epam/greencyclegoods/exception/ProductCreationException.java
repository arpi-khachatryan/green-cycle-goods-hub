package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class ProductCreationException extends Exception {

    public ProductCreationException(String message) {
        super(message);
    }
}
