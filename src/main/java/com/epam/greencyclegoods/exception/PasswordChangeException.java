package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class PasswordChangeException extends Exception {

    public PasswordChangeException(String message) {
        super(message);
    }
}
