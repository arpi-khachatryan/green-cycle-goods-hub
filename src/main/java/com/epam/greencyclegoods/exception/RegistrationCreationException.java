package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class RegistrationCreationException extends Exception {

    public RegistrationCreationException(String message) {
        super(message);
    }
}
