package com.epam.greencyclegoods.exception;

import java.util.Map;

/**
 * @author Arpi Khachatryan
 * Date: 20.11.2023
 */

public class OrderProcessingException extends Exception {

    private final Map<String, String> errorMessages;

    public OrderProcessingException(Map<String, String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    @Override
    public String getMessage() {
        StringBuilder message = new StringBuilder();
        for (String errorMessage : errorMessages.values()) {
            message.append(errorMessage).append(", ");
        }
        return message.length() > 0 ? message.substring(0, message.length() - 2) : "";
    }
}

