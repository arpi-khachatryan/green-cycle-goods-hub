package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class AuthenticationException extends Exception {

    public AuthenticationException(String message) {
        super(message);
    }
}
