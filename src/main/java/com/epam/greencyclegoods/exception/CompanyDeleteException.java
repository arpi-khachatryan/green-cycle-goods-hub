package com.epam.greencyclegoods.exception;

/**
 * @author Arpi Khachatryan
 * Date: 22.10.2023
 */

public class CompanyDeleteException extends Exception {

    public CompanyDeleteException(String message) {
        super(message);
    }
}
