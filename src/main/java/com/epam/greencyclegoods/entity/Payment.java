package com.epam.greencyclegoods.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private double totalAmount;

    @CreationTimestamp
    private LocalDateTime paymentDate;

    private LocalDateTime paymentCreateDate;

    @Enumerated(value = EnumType.STRING)
    private PaymentStatus status;

//    @Enumerated(EnumType.STRING)
//    private PaymentOption paymentOption;

    @OneToOne
    private Order order;

    @ManyToOne
    private User user;
}
