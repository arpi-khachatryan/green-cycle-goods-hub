package com.epam.greencyclegoods.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private int peopleCount;

    @Enumerated(value = EnumType.STRING)
    private RegistrationStatus status;

    @ManyToOne
    private Event event;

    @ManyToOne
    private User user;
}
