package com.epam.greencyclegoods.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private User user;

    @CreationTimestamp
    private LocalDateTime orderAt;

    private double totalPrice;

    @Enumerated(value = EnumType.STRING)
    private OrderStatus status = OrderStatus.NEW;

//    @Enumerated(value = EnumType.STRING)
//    private PaymentOption paymentOption;

    @ManyToMany
    private List<Product> products;

    private boolean paymentCompleted;

    private String additionalAddress;

    private String additionalPhone;
}
