package com.epam.greencyclegoods.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String address;

    private String email;

    private String phone;

    @ManyToOne
    private CompanyCategory companyCategory;

    private Double deliveryPrice;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> pictures;

    @ManyToOne
    private User user;
}
