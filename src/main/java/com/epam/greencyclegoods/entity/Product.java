package com.epam.greencyclegoods.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String description;

    private Double price;

    @ManyToOne
    private ProductCategory productCategory;

    @ManyToOne
    private Company company;

    @ElementCollection
    private List<String> pictures;

    @ManyToOne
    private User user;

    private int productCount;
}
