package com.epam.greencyclegoods.entity;

import lombok.*;

import javax.persistence.*;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String name;
}
