package com.epam.greencyclegoods.entity;

/**
 * @author Arpi Khachatryan
 * Date: 02.09.2023
 */

public enum Role {
    CUSTOMER,
    ADMIN,
    OWNER
}
