# Green Cycle Goods: Your Eco-friendly Marketplace and Events Hub

The project aims to create a comprehensive online platform, Green Cycle Goods, that serves as an eco-friendly marketplace for selling various items made from recycled waste materials. In addition to being a marketplace, the platform will also act as an events hub, providing users with information about upcoming environmental events. The primary goal of the project is to promote sustainable living, raise awareness about environmental issues, and encourage the use of recycled products.

## Features
- Marketplace: Buy and sell eco-friendly products made from recycled materials.
- Events Hub: Stay informed about upcoming environmental events.
- Event Registration: Users can register for environmental events.

## Technologies Used
- [Spring Boot](https://spring.io/projects/spring-boot): A powerful and flexible framework for building Java-based enterprise applications.
- [Spring Data JPA](https://spring.io/projects/spring-data-jpa): Simplifies data access using the Java Persistence API (JPA).
- [Spring Boot Mail](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html#production-ready-email): Provides email sending capabilities in Spring Boot applications.
- [Spring Security](https://spring.io/projects/spring-security): A powerful and customizable authentication and access control framework.
- [Spring DevTools](https://docs.spring.io/spring-boot/docs/current/reference/html/using.html#using.devtools): Tools for local development, including automatic restarts.
- [Thymeleaf](https://www.thymeleaf.org/): A modern server-side Java template engine for web and standalone environments.
- [MySQL](https://www.mysql.com/): An open-source relational database management system.
- [Lombok](https://projectlombok.org/): A Java library that simplifies the writing of boilerplate code.
- [MapStruct](https://mapstruct.org/): A code generator that simplifies the implementation of mappings between Java bean types.
- [ModelMapper](http://modelmapper.org/): A smart object mapping library for Java.
- [Amazon S3](https://aws.amazon.com/s3/): Amazon Simple Storage Service is an object storage service offering industry-leading scalability, data availability, security, and performance.

## Prerequisites
Before running this project, ensure you have met the following requirements:
- **Java**: You need to have Java Development Kit (JDK) 17 or a higher version installed.
- **MySQL**: Install and configure MySQL as your database server.
- **Amazon S3**: Set up an Amazon S3 bucket to store and manage images. Ensure that your credentials and configurations are correctly integrated into the project.
