FROM openjdk:17-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ./target/green-cycle-goods-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]


#FROM openjdk:17-jdk-alpine
#VOLUME /TMP
#EXPOSE 8081
#ADD ./target/green-cycle-goods-0.0.1-SNAPSHOT.jar app.jar
#RUN mkdir -p /var/images
#ENTRYPOINT ["java","-Dspring.profiles.active=dev","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]